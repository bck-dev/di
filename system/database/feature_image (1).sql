-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Jul 08, 2021 at 06:20 AM
-- Server version: 5.7.28
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bck.dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `feature_image`
--

DROP TABLE IF EXISTS `feature_image`;
CREATE TABLE IF NOT EXISTS `feature_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `buttn_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `buttn_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feature_image`
--

INSERT INTO `feature_image` (`id`, `title`, `buttn_text`, `buttn_link`, `image`) VALUES
(4, 'MEN SPORTS', 'click to mens wears', 'men', 'uploads/07-07-2021/69110ab19db9aa19c993560f374d2103.png'),
(3, 'Womens Wear', 'click to womrns wear', 'women', 'uploads/07-07-2021/e895bc65ad5df103f5db65064f95ca65.png'),
(6, 'Common Wears', 'click to common wears', 'common', 'uploads/07-07-2021/8ffa9470240ab7919f4e82b888ece8db.png'),
(11, 'Trend Product', 'click to trend item', 'trend', 'uploads/07-07-2021/05ee224ef5868cea6808b4abc119d688.png'),
(10, 'KIDS WEAR', 'click to kids wears', 'kids', 'uploads/07-07-2021/4e7b5daedf98c901e0cbc6a31b4bc8cb.png');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
