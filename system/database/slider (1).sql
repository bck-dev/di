-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Jul 08, 2021 at 06:20 AM
-- Server version: 5.7.28
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bck.dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `buttn_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `buttn_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `image1` text COLLATE utf8_unicode_ci NOT NULL,
  `image2` text COLLATE utf8_unicode_ci NOT NULL,
  `image3` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `sub_title`, `buttn_text`, `buttn_link`, `image`, `image1`, `image2`, `image3`) VALUES
(4, 'Main Page DI ', 'DI Leather', 'Shop', 'shop', 'uploads/08-07-2021/61262eac6092c555896d2bed9ea1650c.jpg', 'uploads/08-07-2021/1f0c577c250e26ac02c4e0c173ad5dc1.png', 'uploads/08-07-2021/c0c64b755174943bd307a34336967195.png', 'uploads/08-07-2021/0281cf6b449f3b9f2e9f4e66f547c0c1.png'),
(3, 'DiLeather Second Page', 'Di_leather', 'view', 'shop', 'uploads/08-07-2021/f12e821000350952b80239ad21d8af46.jpg', 'uploads/08-07-2021/6f3ee03275b498169044b45dd08d20d4.png', 'uploads/08-07-2021/514f697261b1b6f4252304ae60d1f177.png', 'uploads/08-07-2021/c1ebd3823765b1e00d5de2f96a6426f5.png');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
