(function ($) {
	'use strict';
	/*----------------------------------------*/
	/*  Check if element exists
/*----------------------------------------*/
	$.fn.elExists = function () {
		return this.length > 0;
	};

	/*----------------------------------------*/
	/*  loadOverlay
/*----------------------------------------*/
	$(window).on('load', function () {
		var wind = $(window);
		$('head').addClass('loadOverlay');
		setTimeout(removeClass, 10);
		function removeClass() {
			$('head').removeClass('loadOverlay');
		}
	});

	/*----------------------------------------*/
	/* Newsletter Popup
/*----------------------------------------*/
	setTimeout(function () {
		$('.popup_wrapper').css({
			opacity: '1',
			visibility: 'visible'
		});
		$('.popup_off').on('click', function () {
			$('.popup_wrapper').fadeOut(500);
		});
	}, 5000);

	/*----------------------------------------*/
	/*  WOW
/*----------------------------------------*/
	new WOW().init();

	/*---------------------------------------
		Header Sticky
---------------------------------*/
	$(window).on('scroll', function () {
		if ($(this).scrollTop() >= 300) {
			$('.header-sticky').addClass('sticky');
		} else {
			$('.header-sticky').removeClass('sticky');
		}
	});

/*------------------------------------
	Scrollax
	------------------------------------- */
	$.Scrollax();

	if ($('.jarallax-bg').elExists()) {
		$('.jarallax-bg').jarallax({
			type: "scroll",
		});
	}
/*------------------------------------
	Simple Parallax
	------------------------------------- */
	if ($('.orientation-left').elExists()) {
		var img = document.querySelectorAll('.orientation-left');
		new simpleParallax(img, {
			delay: 0,
			orientation: 'left',
			scale: 1.3
		});
	}
	if ($('.orientation-right').elExists()) {
		var img = document.querySelectorAll('.orientation-right');
		new simpleParallax(img, {
			delay: 0,
			orientation: 'right',
			scale: 1.3
		});
	}
/*------------------------------------
	Toolbar Button
	------------------------------------- */
	var $overlay = $('.global-overlay');
	$('.toolbar-btn').on('click', function (e) {
		e.preventDefault();
		e.stopPropagation();
		var $this = $(this);
		var target = $this.attr('href');
		var prevTarget = $this.parent().siblings().children('.toolbar-btn').attr('href');
		$(target).toggleClass('open');
		$(prevTarget).removeClass('open');
		$($overlay).addClass('overlay-open');
	});

	/*----------------------------------------*/
	/*  Click on Documnet
/*----------------------------------------*/
	var $body = $('.global-overlay');

	$body.on('click', function (e) {
		var $target = e.target;
		var dom = $('.main-wrapper').children();

		if (!$($target).is('.toolbar-btn') && !$($target).parents().is('.open')) {
			dom.removeClass('open');
			dom.find('.open').removeClass('open');
			$overlay.removeClass('overlay-open');
		}
	});

	/*----------------------------------------*/
	/*  Close Button Actions
/*----------------------------------------*/
	$('.btn-close').on('click', function (e) {
		var dom = $('.main-wrapper').children();
		e.preventDefault();
		var $this = $(this);
		$this.parents('.open').removeClass('open');
		dom.find('.global-overlay').removeClass('overlay-open');
	});

	/*----------------------------------------*/
	/*  Offcanvas
/*----------------------------------------*/
	var $offcanvasNav = $('.mobile-menu, .offcanvas-minicart_menu'),
		$offcanvasNavWrap = $(
			'.mobile-menu_wrapper, .offcanvas-menu_wrapper, .offcanvas-minicart_wrapper, .demo-switcher_wrapper'
		),
		$offcanvasNavSubMenu = $offcanvasNav.find('.sub-menu'),
		$menuToggle = $('.menu-btn'),
		$menuClose = $('.btn-close');

	$offcanvasNavSubMenu.slideUp();

	$offcanvasNav.on('click', 'li a, li .menu-expand', function (e) {
		var $this = $(this);
		if (
			$this.parent().attr('class').match(/\b(menu-item-has-children|has-children|has-sub-menu)\b/) &&
			($this.attr('href') === '#' || $this.attr('href') === '' || $this.hasClass('menu-expand'))
		) {
			e.preventDefault();
			if ($this.siblings('ul:visible').length) {
				$this.siblings('ul').slideUp('slow');
			} else {
				$this.closest('li').siblings('li').find('ul:visible').slideUp('slow');
				$this.closest('li').siblings('li').removeClass('menu-open');
				$this.siblings('ul').slideDown('slow');
				$this.parent().siblings().children('ul').slideUp();
			}
		}
		if ($this.is('a') || $this.is('span') || $this.attr('class').match(/\b(menu-expand)\b/)) {
			$this.parent().toggleClass('menu-open');
		} else if ($this.is('li') && $this.attr('class').match(/\b('menu-item-has-children')\b/)) {
			$this.toggleClass('menu-open');
		}
	});

	$('.btn-close').on('click', function (e) {
		e.preventDefault();
		$('.mobile-menu .sub-menu').slideUp();
		$('.mobile-menu .menu-item-has-children').removeClass('menu-open');
	})
	/*----------------------------------------*/
	/*  Nice Select
/*----------------------------------------*/
	if ($('.nice-select').elExists()) {
		$('.nice-select').niceSelect();
	}

	/*----------------------------------------*/
	/*  Countdown
/*----------------------------------------*/
	function makeTimer($endDate, $this, $format) {
		var today = new Date();
		var BigDay = new Date($endDate),
			msPerDay = 24 * 60 * 60 * 1000,
			timeLeft = (BigDay.getTime() - today.getTime()),
			e_daysLeft = timeLeft / msPerDay,
			daysLeft = Math.floor(e_daysLeft),
			e_hrsLeft = (e_daysLeft - daysLeft) * 24,
			hrsLeft = Math.floor(e_hrsLeft),
			e_minsLeft = (e_hrsLeft - hrsLeft) * 60,
			minsLeft = Math.floor((e_hrsLeft - hrsLeft) * 60),
			e_secsLeft = (e_minsLeft - minsLeft) * 60,
			secsLeft = Math.floor((e_minsLeft - minsLeft) * 60);

		var yearsLeft = 0;
		var monthsLeft = 0
		var weeksLeft = 0;

		if ($format != 'short') {
			if (daysLeft > 365) {
				yearsLeft = Math.floor(daysLeft / 365);
				daysLeft = daysLeft % 365;
			}

			if (daysLeft > 30) {
				monthsLeft = Math.floor(daysLeft / 30);
				daysLeft = daysLeft % 30;
			}
			if (daysLeft > 7) {
				weeksLeft = Math.floor(daysLeft / 7);
				daysLeft = daysLeft % 7;
			}
		}

		var yearsLeft = yearsLeft < 10 ? "0" + yearsLeft : yearsLeft,
			monthsLeft = monthsLeft < 10 ? "0" + monthsLeft : monthsLeft,
			weeksLeft = weeksLeft < 10 ? "0" + weeksLeft : weeksLeft,
			daysLeft = daysLeft < 10 ? "0" + daysLeft : daysLeft,
			hrsLeft = hrsLeft < 10 ? "0" + hrsLeft : hrsLeft,
			minsLeft = minsLeft < 10 ? "0" + minsLeft : minsLeft,
			secsLeft = secsLeft < 10 ? "0" + secsLeft : secsLeft,
			yearsText = yearsLeft > 1 ? 'years' : 'year',
			monthsText = monthsLeft > 1 ? 'months' : 'month',
			weeksText = weeksLeft > 1 ? 'weeks' : 'week',
			daysText = daysLeft > 1 ? 'days' : 'day',
			hourText = hrsLeft > 1 ? 'hrs' : 'hr',
			minsText = minsLeft > 1 ? 'mins' : 'min',
			secText = secsLeft > 1 ? 'secs' : 'sec';

		var $markup = {
			wrapper: $this.find('.countdown__item'),
			year: $this.find('.yearsLeft'),
			month: $this.find('.monthsLeft'),
			week: $this.find('.weeksLeft'),
			day: $this.find('.daysLeft'),
			hour: $this.find('.hoursLeft'),
			minute: $this.find('.minsLeft'),
			second: $this.find('.secsLeft'),
			yearTxt: $this.find('.yearsText'),
			monthTxt: $this.find('.monthsText'),
			weekTxt: $this.find('.weeksText'),
			dayTxt: $this.find('.daysText'),
			hourTxt: $this.find('.hoursText'),
			minTxt: $this.find('.minsText'),
			secTxt: $this.find('.secsText')
		}

		var elNumber = $markup.wrapper.length;
		$this.addClass('item-' + elNumber);
		$($markup.year).html(yearsLeft);
		$($markup.yearTxt).html(yearsText);
		$($markup.month).html(monthsLeft);
		$($markup.monthTxt).html(monthsText);
		$($markup.week).html(weeksLeft);
		$($markup.weekTxt).html(weeksText);
		$($markup.day).html(daysLeft);
		$($markup.dayTxt).html(daysText);
		$($markup.hour).html(hrsLeft);
		$($markup.hourTxt).html(hourText);
		$($markup.minute).html(minsLeft);
		$($markup.minTxt).html(minsText);
		$($markup.second).html(secsLeft);
		$($markup.secTxt).html(secText);
	}

	if ($('.countdown').elExists()) {
		$('.countdown').each(function () {
			var $this = $(this);
			var $endDate = $(this).data('countdown');
			var $format = $(this).data('format');
			setInterval(function () {
				makeTimer($endDate, $this, $format);
			}, 0);
		});
	}

	/*----------------------------------------*/
	/*  Cart Plus Minus
/*----------------------------------------*/
	$('.cart-plus-minus-single').append(
		'<div class="dec qtybutton qtybuttonsingle"><i class="icon-chevron-down"></i></div><div class="inc qtybutton qtybuttonsingle"><i class="icon-chevron-up"></i></div>'
	);
	$('.qtybuttonsingle').on('click', function () {
		var $button = $(this);
		var oldValue = $('.cart-plus-minus-box').val();
		if ($button.hasClass('inc')) {
			var newVal = parseInt(oldValue) + 1;
		} else {
			// Don't allow decrementing below zero
			if (oldValue > 1) {
				var newVal = parseInt(oldValue) - 1;
			} else {
				newVal = 1;
			}
		}
		$('.cart-plus-minus-box').val(newVal);
	});

	/*----------------------------------------*/
	/*  CounterUp
/*----------------------------------------*/
	if ($('.count').elExists()) {
		$('.count').counterUp({
			delay: 10,
			time: 1000
		});
	}

	/*----------------------------------------*/
	/*  Light Gallery
/*----------------------------------------*/
	if ($('.lightgallery').elExists()) {
		$('.lightgallery').lightGallery({
			selector: '.gallery-item'
		});
	}

	/*----------------------------------------*/
	/*  Isotope
/*----------------------------------------*/
	if ($('.masonry-grid').elExists()) {
		var $masonryGrid = $('.masonry-grid').isotope({
			itemSelector: '.grid-item',
			percentPosition: true,
			layoutMode: 'masonry',
			masonry: {
				columnWidth: 0
			}
		});
		$masonryGrid.imagesLoaded().progress(function () {
			$masonryGrid.isotope('layout');
		});
	}

	/*----------------------------------------*/
	/*  Swiper All Slider
	/*----------------------------------------*/
	/* ---Main Slider--- */
	if($('.main-slider').elExists()){
		var mySwiper = new Swiper('.main-slider', {
			loop: false,
			slidesPerView: 1,
			autoplay: {},
			speed: 750,
			effect: "slide",
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: 'bullets'
			},
		});
	}
	$(".main-slider").hover(function () {
		(this).swiper.autoplay.stop();
	}, function () {
		(this).swiper.autoplay.start();
	});
	/* ---Main Slider Two--- */
	if($('.main-slider-2').elExists()){
		var mySwiper = new Swiper('.main-slider-2', {
			effect: "fade",
			speed: 750,
			loop: false,
			fadeEffect: { crossFade: true },
			slidesPerView: 1,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true
			},
		});
	}
	/* ---Main Slider Three--- */
	if($('.main-slider-3').elExists()){
		var mySwiper = new Swiper('.main-slider-3', {
			effect: "fade",
			speed: 750,
			loop: false,
			fadeEffect: { crossFade: true },
			slidesPerView: 1,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true
			},
		});
	}
	/* ---Main Slider Four--- */
	if($('.main-slider-4').elExists()){
		var swiper = new Swiper('.main-slider-4', {
			effect: "fade",
			speed: 750,
			loop: false,
			autoplay: {
				delay: 4000,
			},
			fadeEffect: { crossFade: true },
			simulateTouch: false,
			slidesPerView: 1,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true
			},
		});
	}
	$('.mousemove-wrap').mousemove(function (event) {
		var moveX = (($(window).width() / 2) - event.pageX) * 0.05;
		$('.page-back').css('right', moveX + 'px');
	});

	$(".main-slider-4").hover(function () {
		(this).swiper.autoplay.stop();
	}, function () {
		(this).swiper.autoplay.start();
	});

	/* ---Main Slider Five--- */
	if($('.main-slider-5').elExists()){
		var mySwiper = new Swiper('.main-slider-5', {
			effect: "coverflow",
			speed: 750,
			loop: false,
			autoplay: {
				delay: 4000,
			},
			coverflowEffect: {
				rotate: 30,
				slideShadows: false,
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: 'bullets'
			},
			on: {
				init: function () {
					darkNav($(this)[0])
				},
				slideChange: function () {
					darkNav($(this)[0])
				}
			}
		});
	}
	function darkNav($this) {
		var index_currentSlide = $this.realIndex;
		var currentSlide = $this.slides[index_currentSlide]
		if (currentSlide.classList.contains('dark')) {
			$('.lightNav').addClass('darkNav')
		} else {
			$('.lightNav').removeClass('darkNav');
		}
	}

	/* ---Main Slider Six--- */
	if($('.main-slider-6').elExists()){
		var swiper = new Swiper('.main-slider-6', {
			effect: "fade",
			speed: 750,
			loop: false,
			autoplay: {
				delay: 4000,
			},
			fadeEffect: { 
				crossFade: true
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: 'bullets'
			}
		});
		$(".main-slider-6").hover(function() {
			(this).swiper.autoplay.stop();
		}, function() {
			(this).swiper.autoplay.start();
		});
	}

	/* ---Main Slider Seven--- */
	if($('.main-slider-7').elExists()){
		var mySwiper = new Swiper('.main-slider-7', {
			effect: "slide",
			speed: 750,
			loop: false,
			autoplay: {
				delay: 4000,
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: 'bullets'
			},
		});
		$(".main-slider-7").hover(function() {
			(this).swiper.autoplay.stop();
		}, function() {
			(this).swiper.autoplay.start();
		});
	}
	/* ---Main Slider Eight--- */
	if($('.main-slider-8').elExists()){
		var mySwiper = new Swiper('.main-slider-8', {
			effect: "fade",
			speed: 750,
			loop: false,
			autoplay: {
				delay: 4000,
				disableOnInteraction: false,
			},
			fadeEffect: { 
				crossFade: true 
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: 'bullets'
			},
		});
	}

	/* ---Main Slider Nine--- */
	if($('.main-slider-9').elExists()){
		var mySwiper = new Swiper('.main-slider-9', {
			loop: true,
			slidesPerView: 1,
			autoplay: {},
			speed: 750,
			effect: "slide",
			mousewheel: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: 'bullets'
			},
		});
		$(".main-slider-9").hover(function() {
			(this).swiper.autoplay.stop();
		}, function() {
			(this).swiper.autoplay.start();
		});
	}
	/* ---Main Slider--- */
	if($('.main-slider-10').elExists()){
		var mySwiper = new Swiper('.main-slider-10', {
			loop: true,
			slidesPerView: 1,
			autoplay: {
				delay: 5100,
			},
			speed: 750,
			effect: "slide",
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: 'bullets'
			},
		});
	}

	/* ---Featured Product Slider--- */
	if($('.featured-product_slider').elExists()){
		var mySwiper = new Swiper('.featured-product_slider', {
			slidesPerView: 3,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				320: {
				  slidesPerView: 1,
				},
				576: {
				  slidesPerView: 2,
				  spaceBetween: 30
				},
				992: {
				  slidesPerView: 3,
				  spaceBetween: 30
				},
				1501: {
				  slidesPerView: 3,
				  spaceBetween: 60
				}
			}
		});
	}
	/* ---Featured Product Slider--- */
	if($('.featured-product_slider-2').elExists()){
		var mySwiper = new Swiper('.featured-product_slider-2', {
			slidesPerView: 3,
			spaceBetween: 30,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				0: {
				  slidesPerView: 1,
				  spaceBetween: 0,
				},
				768: {
				  slidesPerView: 2
				},
				992: {
				  slidesPerView: 3
				},
			}
		});
	}

	/* ---Brand Slider--- */
	if($('.brand-slider').elExists()){
		var mySwiper = new Swiper('.brand-slider', {
			slidesPerView: 6,
			navigation: {
				nextEl: '.brand-button-next',
				prevEl: '.brand-button-prev',
			},
			breakpoints: {
				320: {
				  slidesPerView: 1
				},
				480: {
				  slidesPerView: 2
				},
				576: {
				  slidesPerView: 3
				},
				992: {
				  slidesPerView: 5
				},
				1200: {
				  slidesPerView: 6
				}
			}
		});
	}
	/* ---Brand Slider Two--- */
	if($('.brand-slider-2').elExists()){
		var mySwiper = new Swiper('.brand-slider-2', {
			spaceBetween: 30,
			navigation: {
				nextEl: '.brand-button-next',
				prevEl: '.brand-button-prev',
			},
			breakpoints: {
				0: {
					slidesPerView: 3,
					slidesPerColumn: 3,
					slidesPerColumnFill: 'row',
				},
				768: {
					slidesPerView: 5,
					slidesPerColumn: 2,
					slidesPerColumnFill: 'row',
				},
			}
		});
	}
	/* ---About Page Brand--- */
	if($('.about-page_brand').elExists()){
		var mySwiper = new Swiper('.about-page_brand', {
			slidesPerView: 5,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				320: {
				  slidesPerView: 2
				},
				576: {
				  slidesPerView: 3
				},
				991: {
				  slidesPerView: 5
				}
			}
		});
	}

	/* ---Featured Product Slider--- */
	if($('.modal-slider').elExists()){
		var mySwiper = new Swiper('.modal-slider', {
			slidesPerView: 1,
			loop: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		});
	}

	/* ---Collection Slider--- */
	if($('.collection-slider').elExists()){
		var mySwiper = new Swiper('.collection-slider', {
			effect: "fade",
			speed: 1,
			loop: true,
			fadeEffect: { crossFade: true },
			slidesPerView: 1,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: ''
			},
		});
	}
	/* ---Banner Slider--- */
	if($('.banner-slider').elExists()){
		var mySwiper = new Swiper('.banner-slider', {
			effect: "slide",
			speed: 750,
			loop: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: ''
			},
			breakpoints: {
				0: {
				  slidesPerView: 1,
				  spaceBetween: 30,
				},
				768: {
				  slidesPerView: 2,
				  spaceBetween: 30,
				},
				992: {
				  slidesPerView: 2,
				  spaceBetween: 30,
				},
				1200: {
				  slidesPerView: 3,
				  spaceBetween: 70,
				}
			}
		});
	}

	/* ---Banner Inner Slider--- */
	if($('.banner-inner_slider').elExists()){
		var mySwiper = new Swiper('.banner-inner_slider', {
			effect: "slide",
			speed: 750,
			loop: true,
			slidesPerView: 1,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: ''
			},
		});
	}

	/*----------------------------------------*/
	/*  Mouseover Active Class
/*----------------------------------------*/
	$('.mouseover-effect, .text-interactive').on('mouseover', 'li', function () {
		var $this = $(this);
		$this.addClass('active').siblings().removeClass('active');
	});

	/* ---Product Detail Vertical Slider--- */
	if($('.pd-vertical_slider').elExists()){
		var verticalSliderNav = new Swiper('.vertical-slider_nav', {
			loop: false,
			draggable: false,
			allowTouchMove: false,
			watchSlidesVisibility: true,
			watchSlidesProgress: true,
			watchOverflow: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				0: {
					direction: 'horizontal',
					spaceBetween: 30,
					slidesPerView: 3,
					draggable: true,
					allowTouchMove: true,
				},
				768: {
					direction: 'vertical',
					spaceBetween: 30,
					slidesPerView: "auto",
				},
			}
		});
		var pdSlider = new Swiper('.pd-vertical_slider', {
			autoplay: false,
			delay: 5000,
			slidesPerView: 1,
			watchSlidesProgress: true,
			watchSlidesVisibility: true,
			allowTouchMove: true,
			setWrapperSize: true,
			waitForTransition: true,
			loop: false,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			thumbs: {
				swiper: verticalSliderNav
			}
		});
	}
	/* ---Product Detail Horizontal Slider--- */
	if($('.gallery-top').elExists()){
		var galleryTop = new Swiper('.gallery-top', {
			spaceBetween: 10,
			loop: true,
			loopedSlides: 4
		});
		var galleryThumbs = new Swiper('.gallery-thumbs', {
			spaceBetween: 10,
			slidesPerView: 4,
			touchRatio: 0.2,
			slideToClickedSlide: true,
			loop: true,
			loopedSlides: 4,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		});
		galleryTop.controller.control = galleryThumbs;
		galleryThumbs.controller.control = galleryTop;
	}

	/* ---Product Demo Slider--- */
	if($('.product-demo_slider').elExists()){
		var mySwiper = new Swiper('.product-demo_slider', {
			slidesPerView: 3,
			spaceBetween: 30,
			loop: false,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				0: {
				  slidesPerView: 1,
				},
				576: {
				  slidesPerView: 2,
				},
				768: {
				  slidesPerView: 3,
				},
			}
		});
	}

	/* ---Testimonial Slider--- */
	if($('.testimonial-slider').elExists()){
		var mySwiper = new Swiper('.testimonial-slider', {
			slidesPerView: 2,
			spaceBetween: 30,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				0: {
				  slidesPerView: 1
				},
				768: {
				  slidesPerView: 2
				}
			}
		});
	}
	if($('.single-testimonial_slider').elExists()){
		var mySwiper = new Swiper('.single-testimonial_slider', {
			slidesPerView: 1,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: 'bullets'
			},
		});
	}
	/* ---Blog Element Slider--- */
	if($('.blog-element_slider').elExists()){
		var mySwiper = new Swiper('.blog-element_slider', {
			slidesPerView: 2,
			spaceBetween: 30,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				0: {
				  slidesPerView: 1
				},
				768: {
				  slidesPerView: 2
				},
				991: {
				  slidesPerView: 2
				}
			}
		});
	}
	/* ---Blog List Slider--- */
	if($('.blog-list_slider').elExists()){
		var mySwiper = new Swiper('.blog-list_slider', {
			slidesPerView: 2,
			spaceBetween: 30,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: 'bullets'
			},
			breakpoints: {
				0: {
				  slidesPerView: 1
				},
				1200: {
				  slidesPerView: 2
				}
			}
		});
	}
	/* ---Blog Element Slider--- */
	if($('.blog-element_slider-2').elExists()){
		var mySwiper = new Swiper('.blog-element_slider-2', {
			slidesPerView: 2,
			spaceBetween: 30,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				320: {
				  slidesPerView: 1
				},
				576: {
				  slidesPerView: 2
				},
				991: {
				  slidesPerView: 3
				}
			}
		});
	}
	/* ---Popular Product Slider--- */
	if($('.popular-product_slider').elExists()){
		var mySwiper = new Swiper('.popular-product_slider', {
			loop: false,
			slidesPerView: 3,
			slidesPerColumn: 1,
			slidesPerGroup: 3,
			centeredSlides: true,
			centeredSlidesBounds: true,
			spaceBetween: 30,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				type: 'bullets'
			},
			breakpoints: {
				0: {
				  slidesPerView: 1,
				  slidesPerGroup: 1,
				},
				576: {
				  slidesPerView: 3,
				  slidesPerGroup: 3,
				},
			}
		});
	}

	/*----------------------------------------*/
	/*  Instagram
/*----------------------------------------*/
	// $.instagramFeed({
    //     'username': 'ecommerce.devitems',
    //     'container': "#instagramFeed",
    //     'display_profile': false,
    //     'display_biography': false,
	// 	'display_gallery': true,
	// 	'lazy_load': true,
    //     'styling': false,
    //     'items': 8,
    //     "image_size": "0",
    //     'items_per_row': 1,
	// 	'margin': 0.5,
    // });
    // $('#instagramFeed').on("DOMNodeInserted", function (e) {
    //     if (e.target.className === "instagram_gallery") {
    //     	$(".instagram_gallery").slick({
    //     		slidesToShow: 5,
	// 			slidesToScroll: 1,
    //     		autoplay: false,
    //     		dots: false,
    //     		arrows: true,
    //     		prevArrow: '<button type="button" class="slick-prev"><i class="lastudioicon-left-arrow"></i></button>',
    //     		nextArrow: '<button type="button" class="slick-next"><i class="lastudioicon-right-arrow"></i></button>',
    //     		responsive: [{
    //     				breakpoint: 1200,
    //     				settings: {
    //     					slidesToShow: 4
    //     				}
    //     			},
    //     			{
    //     				breakpoint: 992,
    //     				settings: {
    //     					slidesToShow: 3
    //     				}
    //     			},
    //     			{
    //     				breakpoint: 576,
    //     				settings: {
    //     					slidesToShow: 2
    //     				}
    //     			}
    //     		]
    //     	});
    //     }
	// });
	
	/* ---Instagram Feed Two--- */
	// $.instagramFeed({
    //     'username': 'ecommerce.devitems',
    //     'container': "#instagramFeedTwo",
    //     'display_profile': false,
    //     'display_biography': false,
    //     'display_gallery': true,
    //     'styling': false,
    //     'items': 8,
    //     "image_size": "480",
    //     'items_per_row': 4,
	// 	'margin': 2,
    // });
    // $('#instagramFeedTwo').on("DOMNodeInserted", function (e) {
    //     if (e.target.className === "instagram_gallery") {
    //     	$(".instagram_gallery").slick({
    //     		slidesToShow: 4,
    //     		slidesToScroll: 1,
    //     		autoplay: false,
    //     		dots: false,
    //     		arrows: true,
    //     		prevArrow: '<button type="button" class="slick-prev"><i class="lastudioicon-left-arrow"></i></button>',
    //     		nextArrow: '<button type="button" class="slick-next"><i class="lastudioicon-right-arrow"></i></button>',
    //     		responsive: [{
    //     				breakpoint: 1200,
    //     				settings: {
    //     					slidesToShow: 4
    //     				}
    //     			},
    //     		]
    //     	});
    //     }
	// });
	
	/* ---Instagram Feed Three--- */
	// $.instagramFeed({
    //     'username': 'ecommerce.devitems',
    //     'container': "#instagramFeedThree",
    //     'display_profile': false,
    //     'display_biography': false,
    //     'display_gallery': true,
    //     'styling': false,
    //     'items': 10,
    //     "image_size": "0",
    //     'items_per_row': 2,
	// 	'margin': 0.5,
    // });
    // $('#instagramFeedThree').on("DOMNodeInserted", function (e) {
    //     if (e.target.className === "instagram_gallery") {
    //     	$(".instagram_gallery").slick({
	// 			slidesToShow: 5,
	// 			rows: 2,
    //     		slidesToScroll: 1,
    //     		autoplay: false,
    //     		dots: false,
    //     		arrows: true,
    //     		prevArrow: '<button type="button" class="slick-prev"><i class="lastudioicon-left-arrow"></i></button>',
    //     		nextArrow: '<button type="button" class="slick-next"><i class="lastudioicon-right-arrow"></i></button>',
    //     		responsive: [{
    //     				breakpoint: 1200,
    //     				settings: {
    //     					slidesToShow: 4
    //     				}
    //     			},
    //     			{
    //     				breakpoint: 992,
    //     				settings: {
    //     					slidesToShow: 3
    //     				}
    //     			},
    //     			{
    //     				breakpoint: 576,
    //     				settings: {
    //     					slidesToShow: 2
    //     				}
    //     			}
    //     		]
    //     	});
    //     }
	// });

	/* ---Instagram Feed Four--- */
	// $.instagramFeed({
    //     'username': 'ecommerce.devitems',
    //     'container': "#instagramFeed",
    //     'display_profile': false,
    //     'display_biography': false,
	// 	'display_gallery': true,
	// 	'lazy_load': true,
    //     'styling': false,
    //     'items': 8,
    //     "image_size": "0",
    //     'items_per_row': 1,
	// 	'margin': 0.5,
    // });
    // $('#instagramFeedFour').on("DOMNodeInserted", function (e) {
    //     if (e.target.className === "instagram_gallery") {
    //     	$(".instagram_gallery").slick({
    //     		slidesToShow: 4,
	// 			slidesToScroll: 1,
    //     		autoplay: false,
    //     		dots: false,
    //     		arrows: true,
    //     		prevArrow: '<button type="button" class="slick-prev"><i class="lastudioicon-left-arrow"></i></button>',
    //     		nextArrow: '<button type="button" class="slick-next"><i class="lastudioicon-right-arrow"></i></button>',
    //     		responsive: [{
    //     				breakpoint: 1200,
    //     				settings: {
    //     					slidesToShow: 4
    //     				}
    //     			},
    //     			{
    //     				breakpoint: 992,
    //     				settings: {
    //     					slidesToShow: 3
    //     				}
    //     			},
    //     			{
    //     				breakpoint: 576,
    //     				settings: {
    //     					slidesToShow: 2
    //     				}
    //     			}
    //     		]
    //     	});
    //     }
	// });

	/* ---Instagram Feed Five--- */
	// $.instagramFeed({
    //     'username': 'ecommerce.devitems',
    //     'container': "#instagramFeedFive",
    //     'display_profile': false,
    //     'display_biography': false,
    //     'display_gallery': true,
    //     'styling': false,
    //     'items': 8,
    //     "image_size": "0",
    //     'items_per_row': 2,
	// 	'margin': 0.5,
    // });
    // $('#instagramFeedFive').on("DOMNodeInserted", function (e) {
    //     if (e.target.className === "instagram_gallery") {
    //     	$(".instagram_gallery").slick({
	// 			slidesToShow: 4,
	// 			rows: 2,
    //     		slidesToScroll: 1,
    //     		autoplay: false,
    //     		dots: false,
    //     		arrows: true,
    //     		prevArrow: '<button type="button" class="slick-prev"><i class="lastudioicon-left-arrow"></i></button>',
    //     		nextArrow: '<button type="button" class="slick-next"><i class="lastudioicon-right-arrow"></i></button>',
    //     		responsive: [{
    //     				breakpoint: 1200,
    //     				settings: {
    //     					slidesToShow: 4
    //     				}
    //     			},
    //     			{
    //     				breakpoint: 992,
    //     				settings: {
    //     					slidesToShow: 3
    //     				}
    //     			},
    //     			{
    //     				breakpoint: 576,
    //     				settings: {
    //     					slidesToShow: 2
    //     				}
    //     			}
    //     		]
    //     	});
    //     }
	// });

	/*----------------------------------------*/
	/*  Offcanvas Inner Nav
/*----------------------------------------*/
	$('.frequently-item li.has-sub a, .widgets-item li.has-sub a, .hassub-item li.has-sub a, .toggle-item  li.has-sub a').on('click', function () {
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp();
		} else {
			element.addClass('open');
			element.children('ul').slideDown();
			element.siblings('li').children('ul').slideUp();
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp();
		}
	});

	/*----------------------------------------*/
	/*  Vendor Button
/*----------------------------------------*/
	$('.vendor-btn').on('click', function (e) {
		e.preventDefault();
		$('.vendor-body').slideToggle();
		$('.vendor-btn').toggleClass('active');
	})

	/*----------------------------------------*/
	/*  Shop filter active 
/*----------------------------------------*/
	$('.filter-btn').on('click', function (e) {
		e.preventDefault();
		$('.filter-body').slideToggle();
		$('.filter-btn').toggleClass('active');
	})

	/*----------------------------------------*/
	/*  jQuery Zoom
/*----------------------------------------*/
	if($('.zoom').elExists()){
		$('.zoom').zoom();
	}

	/*------------------------------------
	    Magnific Popup
		------------------------------------- */
	if ($('.popup-vimeo').elExists()) {
		$('.popup-vimeo').magnificPopup({
			type: 'iframe',
			disableOn: function () {
				if ($(window).width() < 600) {
					return false;
				}
				return true;
			}
		});
	}

	/*----------------------------------------*/
	/*  Sticky Sidebar
/*----------------------------------------*/
	if($('#sticky-sidebar').elExists()){
		$('#sticky-sidebar').theiaStickySidebar({
			additionalMarginTop: 80
		});
	}

	/*----------------------------------------*/
	/*  Slider Range
/*----------------------------------------*/
	var sliderrange = $('#slider-range');
	var amountprice = $('#amount');
	var min = parseInt($('#amount').data('min'));
	var max = parseInt($('#amount').data('max'));
	$(function () {
		sliderrange.slider({
			range: true,
			min: min,
			max: max,
			step: 100,
			values: [min, max],
			slide: function (event, ui) {
				amountprice.val('LKR.' + ui.values[0] + ' - LKR.' + ui.values[1]);
			}
		});
		amountprice.val('LKR.' + sliderrange.slider('values', 0) + ' - LKR.' + sliderrange.slider('values', 1));
	});

	/*----------------------------------------*/
	/*  Product View Mode
/*----------------------------------------*/
	function porductViewMode() {
		$(window).on({
			load: function () {
				var activeChild = $('.product-view-mode a.active');
				var firstChild = $('.product-view-mode').children().first();
				var window_width = $(window).width();

				if (window_width < 768) {
					$('.product-view-mode a').removeClass('active');
					$('.product-view-mode').children().first().addClass('active');
					$('.shop-product-wrap').removeClass('gridview-3 gridview-4 gridview-5').addClass('gridview-2');
				}
			},
			resize: function () {
				var ww = $(window).width();
				var activeChild = $('.product-view-mode a.active');
				var firstChild = $('.product-view-mode').children().first();
				var defaultView = $('.product-view-mode').data('default');

				if (ww < 1200 && ww > 575) {
					if (activeChild.hasClass('grid-5')) {
						$('.product-view-mode a.grid-5').removeClass('active');
						if (defaultView == 4) {
							$('.product-view-mode a.grid-4').addClass('active');
							$('.shop-product-wrap')
								.removeClass('gridview-2 gridview-3 gridview-5')
								.addClass('gridview-4');
						} else if (defaultView == 'list') {
							$('.product-view-mode a.list').addClass('active');
							$('.shop-product-wrap')
								.removeClass('gridview-2 gridview-3 gridview-4 gridview-5')
								.addClass('listview');
						} else {
							$('.product-view-mode a.grid-3').addClass('active');
							$('.shop-product-wrap')
								.removeClass('gridview-2 gridview-4 gridview-5')
								.addClass('gridview-3');
						}
					}
				}

				if (ww < 768 && ww > 575) {
					if (activeChild.hasClass('grid-4')) {
						$('.product-view-mode a.grid-4').removeClass('active');
						if (defaultView == 'list') {
							$('.product-view-mode a.list').addClass('active');
							$('.shop-product-wrap')
								.removeClass('gridview-2 gridview-3 gridview-4 gridview-5')
								.addClass('listview');
						} else {
							$('.product-view-mode a.grid-3').addClass('active');
							$('.shop-product-wrap')
								.removeClass('gridview-2 gridview-4 gridview-5')
								.addClass('gridview-3');
						}
					}
				}
				if (activeChild.hasClass('list')) {} else {
					if (ww < 576) {
						$('.product-view-mode a').removeClass('active');
						$('.product-view-mode').children().first().addClass('active');
						$('.shop-product-wrap').removeClass('gridview-3 gridview-4 gridview-5').addClass('gridview-2');
					} else {
						if (activeChild.hasClass('grid-2')) {
							if (ww < 1200) {
								$('.product-view-mode a:not(:first-child)').removeClass('active');
							} else {
								$('.product-view-mode a').removeClass('active');
								$('.product-view-mode a:nth-child(2)').addClass('active');
								$('.shop-product-wrap')
									.removeClass('gridview-2 gridview-4 gridview-5')
									.addClass('gridview-3');
							}
						}
					}
				}
			}
		});
		$('.product-view-mode a').on('click', function (e) {
			e.preventDefault();

			var shopProductWrap = $('.shop-product-wrap');
			var viewMode = $(this).data('target');

			$('.product-view-mode a').removeClass('active');
			$(this).addClass('active');
			if (viewMode == 'listview') {
				shopProductWrap.removeClass('grid');
			} else {
				if (shopProductWrap.not('.grid')) shopProductWrap.addClass('grid');
			}
			shopProductWrap.removeClass('gridview-2 gridview-3 gridview-4 gridview-5 listview').addClass(viewMode);
		});
	}
	porductViewMode();

	/*----------------------------------------*/
	/*  Tooltip
/*----------------------------------------*/
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});

	/*----------------------------------------*/
	/*  Toggle Function Active
/*----------------------------------------*/
	// showlogin toggle
	$('#showlogin').on('click', function () {
		$('#checkout-login').slideToggle(900);
	});
	// showlogin toggle
	$('#showcoupon').on('click', function () {
		$('#checkout_coupon').slideToggle(900);
	});
	// showlogin toggle
	$('#cbox').on('click', function () {
		$('#cbox-info').slideToggle(900);
	});

	// showlogin toggle
	$('#ship-box').on('click', function () {
		$('#ship-box-info').slideToggle(1000);
	});

	/*--------------------------------
    Scroll To Top
-------------------------------- */
	function scrollToTop() {
		var $scrollUp = $('.scroll-to-top, .scroll-to-top-2'),
			$lastScrollTop = 0,
			$window = $(window);

		$window.on('scroll', function () {
			var topPos = $(this).scrollTop();
			if (topPos > $lastScrollTop) {
				$scrollUp.removeClass('show');
			} else {
				if ($window.scrollTop() > 200) {
					$scrollUp.addClass('show');
				} else {
					$scrollUp.removeClass('show');
				}
			}
			$lastScrollTop = topPos;
		});

		$scrollUp.on('click', function (evt) {
			$('html, body').animate({
				scrollTop: 0
			}, 600);
			evt.preventDefault();
		});
	}

	scrollToTop();

})(jQuery);
