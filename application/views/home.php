<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/topBar'); ?>
<?php $this->load->view('components/common/middleBar'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/common/miniCart'); ?>

<?php $this->load->view('components/section/slider'); ?>
<?php $this->load->view('components/section/featured'); ?>
<?php $this->load->view('components/section/homeProducts'); ?>
<?php $this->load->view('components/section/homeShipping'); ?>
<?php $this->load->view('components/section/productQuickView'); ?>


<?php $this->load->view('components/common/footer'); ?>
<?php $this->load->view('components/common/templateJs'); ?>
<?php $this->load->view('components/common/customJs'); ?>