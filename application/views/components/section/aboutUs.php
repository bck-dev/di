<main class="main-content">
            <!-- Begin Breadcrumb Area -->
            <div class="breadcrumb-area without-bg">
                <div class="container custom-space h-100">
                    <div class="breadcrumb-content black-text h-100">
                        <h1 class="text-uppercase mb-0">About Us</h1>
                        <ul>
                            <li><a href="<?php echo base_url() ?>">Home</a></li>
                            <li><a href="javascript:void(0)"><?php echo $pageName; ?></a></li>                           
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Breadcrumb Area End Here -->
            <?php for ($x = 1; $x < 10; $x+=2) : ?>

           
            <?php foreach ($allData as $allData) : ?>  
            <?php if ($allData->position == $x) { ?>             
            <div class="about-banner_area">
                <div class="container custom-space-2">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="single-img">
                                <img src="<?php echo base_url() . $allData->image ?>" alt="banner">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="single-content">
                                <h2 class="heading text-uppercase mb-0"><?php echo $allData->title; ?></h2>
                                <p class="short-desc mb-0"><?php echo $allData->content; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } else { ?>
            <div class="about-banner_area">
                <div class="container custom-space-2">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6 offset-lg-2 order-lg-2 order-1">
                            <div class="single-img">
                                <img src="<?php echo base_url() . $allData->image ?>" alt="banner">
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 order-lg-1 order-2">
                            <div class="single-content">
                                <h2 class="heading-2 text-uppercase mb-0"><?php echo $allData->title; ?></h2>
                                <p class="short-desc mb-0"><?php echo $allData->content; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?> 
            <?php endforeach; ?> 
            <?php endfor ?>       
 </main>