<!-- <style>
a {
    color: #FFFFFF;
    text-decoration: none;
}
</style> -->

<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.0/jquery.fancybox.min.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.0/jquery.fancybox.min.js"></script>

<!-- Begin Main Content Area -->
<main class="main-content">
    <div class="breadcrumb-area with-border pb-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="with-page_nav">
                        <div class="breadcrumb-content">
                            <ul>
                                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                <li><a href="<?php echo base_url('shop'); ?>">Shop</a></li>
                                <li><?php echo $product->productName; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product-detail_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="product-detail_img horizontal-slider_wrap">
                        <div class="swiper-container gallery-top lightgallery">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="single-img zoom">
                                        <img src="<?php echo base_url() . $product->coverImage; ?>" alt="Product Image">
                                        <div class="inner-stuff">
                                            <ul>
                                                <li class="gallery-item"
                                                    data-src="<?php echo base_url() . $product->coverImage; ?>">
                                                    <a href="javascript:void(0)">
                                                        <i class="lastudioicon-full-screen"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <?php foreach ($product->images as $img) : ?>
                                <div class="swiper-slide">
                                    <div class="single-img zoom">
                                        <img src="<?php echo base_url() . $img->path; ?>" alt="Product Image">
                                        <div class="inner-stuff">
                                            <ul>
                                                <li class="gallery-item"
                                                    data-src="<?php echo base_url() . $img->path; ?>">
                                                    <a href="javascript:void(0)">
                                                        <i class="lastudioicon-full-screen"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="<?php echo base_url() . $product->coverImage; ?>" alt="Product Thumnail">
                                </div>
                                <?php foreach ($product->images as $img) : ?>
                                <div class="swiper-slide">
                                    <img src="<?php echo base_url() . $img->path; ?>" alt="Product Thumnail">
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="product-detail_content">
                        <h1 class="heading text-uppercase mb-0"><?php echo $product->productName; ?></h1>
                        <input type="hidden" name="productId" id="productId" value="<?php echo $product->id; ?>" />
                        <div class="product-info">
                            <div class="product-stock">
                                <?php if ($product->stock > 0) : ?>
                                <p class="m-0"><i class="far fa-check-circle green"></i> In Stock</p>
                                <?php else : ?>
                                <p class="m-0"><i class="far fa-times-circle red"></i> Out of Stock</p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="price-box">
                            <?php if ($product->discount > 0) : ?>
                            <span class="old-price">LKR
                                <?php echo number_format($product->lowestPrice, 2); ?></span>
                            <?php endif; ?>
                            <span class="new-price">LKR
                                <?php echo number_format($product->lowestPrice * ((100 - $product->discount) / 100), 2); ?></span>
                        </div>
                        <p class="short-desc mb-0"><?php echo $product->description; ?></p>
                        <div>
                            <span class="black">Available Sizes</span>
                            <input type="hidden" name="size" id="size" />
                            <div class="product-sizes mt-2">
                                <?php foreach ($product->sizes as $size) : ?>
                                <div class="size-box-outline" id="select-size<?php echo $size->sizeId; ?>"
                                    data-size="<?php echo $size->sizeId; ?>" onclick="selectSize(this);">
                                    <?php echo $size->size; ?>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div>
                            <span class="black">Available Colors</span>
                            <input type="hidden" name="color" id="color" />
                            <div class="product-colors mt-2" id="pc">
                                <?php foreach ($product->colors as $color) : ?>
                                <div class="color-box-outline" id="select-color<?php echo $color->colorId; ?>">
                                    <div class="color-box" style="background-color: <?php echo $color->code; ?>"
                                        data-color="<?php echo $color->colorId; ?>" onclick="selectColor(this);"></div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php if ($product->stock > 0) : ?>
                        <form action="<?php echo base_url('cart/add') ?>" method="post">
                            <div class="quantity-with_btn">
                                <div class="quantity">
                                    <div class="cart-plus-minus cart-plus-minus-single">
                                        <input id="productOption" name="productOption" type="hidden">
                                        <input class="cart-plus-minus-box" name="qty" value="1" type="number">
                                        <div class="dec qtybutton qtybuttonsingle">
                                            <i class="lastudioicon-down-arrow"></i>
                                        </div>
                                        <div class="inc qtybutton qtybuttonsingle">
                                            <i class="lastudioicon-up-arrow"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="add-to_cart">
                                    <div class="skudmart-btn primary-btn primary-hover btn-lg_size"
                                        style="cursor: pointer;" id="decoyButton" onclick="throwWarning();">
                                        Add to
                                        cart</div>
                                    <button class="skudmart-btn primary-btn primary-hover btn-lg_size d-none"
                                        id="mainButton">Add to
                                        cart</button>
                                </div>
                            </div>
                        </form>
                        <?php endif; ?>



                        <ul class="additional-stuff">
                            <li class="product-meta">
                                <ul>
                                    <li>
                                        <span>Category:</span>
                                        <a
                                            href="<?php echo base_url('shop/category/') . $product->categoryId ?>"><?php echo $product->categoryName; ?></a>
                                    </li>
                                    <!-- <li>
                                        <span>Labels:</span>
                                        <?php foreach ($product->labels as $label) : ?>
                                        <a class="label mr-3"
                                            href="<?php echo base_url('shop/label/') . $label->labelId ?>"><?php echo $label->label; ?></a>
                                        <?php endforeach; ?>
                                    </li> -->
                                </ul>
                            </li>
                        </ul> <br />


                        <div class="product-stock">
                            <h6 class="heading text-uppercase mb-0">Island Wide Delivery</h6>
                            <p class="m-0"><i class="far fa-check-circle green"></i> 14 Days easy return Policy.</p>

                            <p class="m-0"><i class="far fa-check-circle green"></i> Order Yours bfore 2:30pm for same
                                day dispatch.</p>
                            <h6 class="heading text-uppercase mt-4 mb-3">Payment Options</h6>
                            <div class="row">
                                <div class="col-lg-4">
                                    <img class=""
                                        src="<?php echo base_url(); ?>assets/template/images/payment/master1.jpg"
                                        alt="Payment Type">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php if (count($product->details)>0): ?>
        <div class="product-tab_area pt-95 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tab-section_area pb-40">
                            <div class="product-tab-2">
                                <ul class="nav product-menu">
                                    <li>
                                        <a class="active" data-toggle="tab" href="#info">Additional information</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php foreach ($product->details as $detail) : ?>
                    <div class="col-lg-3">
                        <div class="tab-content sku-tab_content">
                            <div id="info" class="tab-pane active show" role="tabpanel">
                                <div class="description-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <strong><?php echo $detail->title; ?>: </strong>
                                            <br />
                                            <?php echo $detail->content; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php endforeach; ?>

                </div>
            </div>
        </div>
        <?php else: ?>
        <div class="my-5 py-5"></div>
        <?php endif; ?>

        <script>
        function selectColor(element) {
            var color = $(element).data('color');
            $("#color").val(color);

            if ($("#size").val()) {
                $(".color-box-outline").removeClass('selected-color');
                $("#select-color" + color).addClass('selected-color');

                getPrice();
            } else {
                alert("Please Select Size First");
            }


        }

        function selectSize(element) {
            var size = $(element).data('size');
            $("#size").val(size);
            $("#color").val();

            $(".size-box-outline").removeClass('selected-size');
            $("#select-size" + size).addClass('selected-size');
            $(".color-box-outline").removeClass('selected-color');

            getColors();

        }

        function throwWarning() {
            alert("Select Product SIze and Color First");
        }

        function getColors() {
            var size = $('#size').val();
            var productId = $('#productId').val();
            var base_url = "<?php echo base_url(); ?>";
            console.log(size, productId);
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url('getColors') ?>',
                data: {
                    size: size,
                    productId: productId
                },
                dataType: 'json',
                success: function(results) {
                    console.log(results);
                    var pc = "";
                    jQuery.each(results, function(key, val) {
                        pc = pc + '<div class="color-box-outline" id="select-color' + val[
                            'colorId'] + '">';
                        pc = pc + '<div class="color-box" style="background-color: ' + val['code'] +
                            '" data-color="' + val['colorId'] + '" onclick="selectColor(this);">';
                        pc = pc + '</div>';
                        pc = pc + '</div>';
                    });
                    $('#pc').html(pc);

                    if (Object.keys(results).length == 1) {
                        $(".color-box-outline").removeClass('selected-color');
                        $("#select-color" + results[0]['colorId']).addClass('selected-color');
                        $("#color").val(results[0]['colorId']);
                        const nf = new Intl.NumberFormat('en-US', {
                            style: 'currency',
                            currency: 'LKR',
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                        });
                        var price = results[0]['price'];
                        var id = results[0]['id'];
                        var discount = results[0]['price'] * results[0]['discount'] / 100;
                        var newPrice = price - discount;
                        $('.old-price').html(nf.format(price));
                        $('.new-price').html(nf.format(newPrice));
                        $('#productOption').val(id);
                    }
                },

                error: function() {
                    console.log('error');
                }

            });
        }

        function getPrice() {

            var color = $('#color').val();
            var size = $('#size').val();
            var productId = $('#productId').val();
            var base_url = "<?php echo base_url(); ?>";
            // console.log(color, size, productId);
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url('getPrice') ?>',
                data: {
                    size: size,
                    color: color,
                    productId: productId
                },
                dataType: 'json',
                success: function(results) {

                    $(".color-box-outline").removeClass('selected-color');
                    $("#select-color" + results[0]['colorId']).addClass('selected-color');
                    const nf = new Intl.NumberFormat('en-US', {
                        style: 'currency',
                        currency: 'LKR',
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2,
                    });
                    var price = results[0]['price'];
                    var id = results[0]['id'];
                    var discount = results[0]['price'] * results[0]['discount'] / 100;
                    var newPrice = price - discount;
                    $('.old-price').html(nf.format(price));
                    $('.new-price').html(nf.format(newPrice));
                    $('#productOption').val(id);

                    $("#decoyButton").addClass('d-none');
                    $("#mainButton").removeClass('d-none');
                    $("#mainButton").addClass('d-block');
                },

                error: function() {
                    console.log('error');
                }

            });
        }
        </script>