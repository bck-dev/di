<!-- Begin Slider Area -->
<div class="slider-area">

    <!-- Main Slider -->
    <div class="swiper-container main-slider-7 nav-pagination_wrap circle-arrow" data-scrollax-parent="true">
        <div class="swiper-wrapper">
            <?php foreach ($sliderData as $sliderData) : ?>
            <div class="swiper-slide animation-style-06 style-01"
                style="background-image: url(<?php echo base_url() . $sliderData->image ?>); height:550px; width:100%; position:relative; repeat: no-repeat;">

                <div class="container h-100">
                    <div class="inner-slide" data-scrollax="properties: { 'translateY': '10%' }">

                        <div class="slide-content white-text">
                            <span class="category"><?php echo $sliderData->title; ?></span>
                            <h4 class="product-offer text-uppercase mb-0"><?php echo $sliderData->sub_title; ?></h4>
                            <div class="button-wrap position-center">
                                <a class="skudmart-btn white-border_btn"
                                    href="<?php echo base_url('/') . $sliderData->buttn_link; ?>"><?php echo $sliderData->buttn_text; ?></a>
                            </div>
                        </div>

                        <div class="slide-img">
                            <div class="front-img">
                                <img src="<?php echo base_url() . $sliderData->image2 ?>" alt="Inner Image">
                            </div>
                            <div class="sticker-img">
                                <img class="img-full" src="<?php echo base_url() . $sliderData->image1 ?>"
                                    alt="Inner Image">
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <?php endforeach; ?>
            <!-- <div class="swiper-slide animation-style-06 style-02 bg-2">
                <div class="inner-slide" data-scrollax="properties: { 'translateY': '10%' }">
                    <div class="slide-content">
                        <span class="category text-uppercase pb-35"><?php echo $sliderData->title; ?></span>
                        <h4 class="product-offer pb-55 mb-0"><?php echo $sliderData->sub_title; ?></h4>
                        <div class="button-wrap position-center">
                            <a class="skudmart-btn white-border_btn" href="shop-fullwidth.html"><?php echo $sliderData->buttn_text; ?></a>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>

        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
    <!-- -->
</div>
<!-- Slider Area End Here -->