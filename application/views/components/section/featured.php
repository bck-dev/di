<!-- Begin Featured Product Area -->
<div class="featured-product_area pt-30">
    <div class="container-fluid p-0 overflow-hidden">
        <div class="row">
            <div class="col-lg-12">
                <div class="swiper-container featured-product_slider-2 nav-pagination_wrap">
                    <div class="swiper-wrapper">
                        <?php foreach ($featureData as $featureData) : ?>
                        <div class="swiper-slide">
                            <div class="featured-product_item style-02">
                                <div class="single-img img-hover_effect" style="max-height:300px; ">
                                    <img class="img-full" src="<?php echo base_url() . $featureData->image ?>"
                                        alt="Featured Product">
                                    <div class="fp-btn_wrap position-bottom_left">
                                        <a class="skudmart-btn primary-btn btn-lg_size"
                                            href="<?php echo $featureData->buttn_link; ?>"><?php echo $featureData->title; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>


                    <!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Featured Product Area End Here -->