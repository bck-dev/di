<!-- Breadcrumb Area End Here -->

<!-- Begin Checkout Area -->
<div class="checkout-area pt-100 pb-100">
    <div class="container">
        <form action="<?php echo base_url('checkout/add'); ?>" method="POST" name="addForm">
            <div class="row">
                <div class="col-lg-6 col-12">

                    <div class="checkbox-form">
                        <h3>Customer Details</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>First Name <span class="required">*</span></label>
                                    <input placeholder="" type="text" name='first_name' required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>Last Name <span class="required">*</span></label>
                                    <input placeholder="" type="text" name='last_name' required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>Email Address <span class="required">*</span></label>
                                    <input placeholder="" type="email" name='email' id="email" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>Phone <span class="required">*</span></label>
                                    <input placeholder="" type="text" name='telephone' required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <h5>Shipping Address</h5>
                                <div class="checkout-form-list">
                                    <label>Address <span class="required">*</span></label>
                                    <input placeholder="Address line 1" type="text" name='address1' required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="checkout-form-list">
                                    <input placeholder="Address line 2 (optional)" type="text" name='address2'>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="country-select clearfix">
                                    <label>City/Town <span class="required">*</span></label>
                                    <select class="myniceselect nice-select wide" name="city" id="city"
                                        onchange="setArea(this)">
                                        <option disabled="disabled" selected="selected">Select City/Town</option>
                                        <?php foreach ($deliveryData as $d) : ?>
                                        <option value="<?php echo $d->area; ?>"><?php echo $d->area; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <h5>Billing Address</h5>
                                <div class="check-box checkout-form-list create-acc">
                                    <input class="input-checkbox" type="checkbox" id="billingCheck">
                                    <label class="input-label mb-0" for="billingCheck">Same As Shipping Address</label>
                                </div>
                                <div class="checkout-form-list billingAddressBox">
                                    <label>Address <span class="required">*</span></label>
                                    <input placeholder="" type="text" name='billing_address' id='billingAddress'
                                        required />
                                </div>
                            </div>

                            <div class="col-md-12">
                                <h5>Gift Voucher Email</h5>
                                <div class="checkout-form-list">
                                    <label>Email Address <span class="required">*</span></label>
                                    <input placeholder="" type="text" name='gift_email' id="giftEmail" />
                                </div>
                            </div>

                            <div class="col-md-12">
                                <h5>Special Note</h5>
                                <div class="check-box checkout-form-list create-acc">
                                    <input class="input-checkbox" type="checkbox" id="noteCheck">
                                    <label class="input-label mb-0" for="noteCheck">Add a Special Note to Order</label>
                                </div>
                                <div class="checkout-form-list noteBox d-none">
                                    <label>Special Note</label><br />
                                    <textarea class="form-control" name='note' id='note'></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 col-12">
                    <div class="your-order">
                        <h3>Your order</h3>
                        <div class="your-order-table table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="cart-product-name">Product</th>
                                        <th class="cart-product-total">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($cartData as $cartData) : ?>
                                    <tr class="cart_item">

                                        <td class="cart-product-name"> <?php echo $cartData->productName ?><strong
                                                class="product-quantity">
                                                × <?php echo $cartData->quantity ?></strong></td>
                                        <td class="cart-product-total" align="right"><span class="amount">LKR
                                                <?php echo number_format($cartData->price * ((100 - $cartData->discount) /100) * ($cartData->quantity), 2); ?></span>
                                        </td>

                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr class="cart-subtotal">
                                        <th>Cart Subtotal</th>

                                        <td align="right"><span class="amount">LKR
                                                <?php echo number_format($cartTotal, 2 )?></span>
                                            <input type="hidden" id="cartTotal" name="cart_total"
                                                value="<?php echo $cartTotal?>" />
                                        </td>
                                    </tr>
                                    <?php if ($this->session->userdata('deduction_amount')>0): ?>
                                    <tr class="cart-subtotal">
                                        <th>Voucher</th>

                                        <td align="right"><span class="amount">LKR
                                                <?php echo number_format($this->session->userdata('deduction_amount'), 2 )?></span>
                                            <input type="hidden" id="deductionAmount"
                                                value="<?php echo $this->session->userdata('deduction_amount')?>" />
                                        </td>
                                    </tr>
                                    <?php endif; ?>
                                    <tr class="cart-subtotal">
                                        <th>Delivery Fees</th>
                                        <td align="right"><span class="amount" id='df'> Select City/Town</span>
                                            <input type="hidden" id="dfi" name="df" />
                                        </td>
                                    </tr>
                                    <tr class="order-total">
                                        <th>Grand Total</th>
                                        <?php
                                            $grandTotal = $cartTotal -$this->session->userdata('deduction_amount');
                                            if($grandTotal<0){
                                                $grandTotal = 0;
                                            }
                                        ?>
                                        <td align="right"><strong><span class="amount" id="total">LKR
                                                    <?php echo number_format($grandTotal,2) ?></span></strong>

                                            <input type="hidden" id="grandTotal" name="grand_total"
                                                value="<?php echo $grandTotal ?>" />
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="payment-method">
                            <div class="payment-accordion">
                                <div class="check-box checkout-form-list create-acc">
                                    <input class="input-checkbox" type="checkbox" id="cbox" required>
                                    <label class="input-label mb-0" for="cbox">By click to Accept the all terms &
                                        conditions.</label>
                                </div>
                                <div class="order-button-payment">
                                    <input value="Pay Now!" type="submit">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
$("#email").change(function() {
    $("#giftEmail").val($("#email").val());
});

$("#billingCheck").change(function() {
    if ($("#billingCheck").is(":checked") == true) {
        $(".billingAddressBox").removeClass('d-block');
        $(".billingAddressBox").addClass('d-none');
        $("#billingAddress").prop('required', false);
    } else {
        $(".billingAddressBox").removeClass('d-none');
        $(".billingAddressBox").addClass('d-block');
        $("#billingAddress").prop('required', true);
    }
});

$("#noteCheck").change(function() {
    if ($("#noteCheck").is(":checked") == true) {
        $(".noteBox").removeClass('d-none');
        $(".noteBox").addClass('d-block');
        $("#note").prop('required', true);
    } else {
        $(".noteBox").removeClass('d-block');
        $(".noteBox").addClass('d-none');
        $("#note").prop('required', false);
    }
});

function setArea(element) {
    var city = $('#city').val();
    var base_url = "<?php echo base_url(); ?>";

    $.ajax({
        type: 'GET',
        url: '<?php echo base_url('getArea') ?>',
        data: {
            city: city,
        },
        dataType: 'json',
        success: function(results) {
            const nf = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'LKR',
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
            });
            $('#df').html(nf.format(results));
            $('#dfi').val(results);

            var cartTotal = $('#cartTotal').val();

            if ($('#deductionAmount').val() > 0) {

                var deduct = parseInt($('#deductionAmount').val());

            } else {
                var deduct = 0;
            }

            var total = parseFloat(results) + parseFloat(cartTotal) - deduct;
            if (total < 0) {
                total = 0;
            }
            $('#total').html(nf.format(total));
            $('#grandTotal').val(total);
        },
        error: function() {
            console.log('error');
        }
    });
}
</script>