<!-- Begin Shipping Area -->
<div class="shipping-area bg-smoke_color">
    <div class="container custom-space-2">
        <div class="shipping-nav pt-50 pb-45">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="shipping-item style-02">
                        <span class="lastudioicon-car-parking"></span>
                        <h4 class="heading pb-10 mb-0">Freeship Wordwide</h4>
                        <p class="short-desc">In ac hendrerit turpis. Aliquam ultrices dolor dolor, at commodo
                            diam feugiat</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="shipping-item style-02">
                        <span class="dlicon shopping_gift"></span>
                        <h4 class="heading pb-10 mb-0">Freeship Wordwide</h4>
                        <p class="short-desc">In ac hendrerit turpis. Aliquam ultrices dolor dolor, at commodo
                            diam feugiat</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="shipping-item style-02 pb-sm-0">
                        <span class="dlicon tech-2_l-security"></span>
                        <h4 class="heading pb-10 mb-0">Freeship Wordwide</h4>
                        <p class="short-desc">In ac hendrerit turpis. Aliquam ultrices dolor dolor, at commodo
                            diam feugiat</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="shipping-item style-02 pb-sm-0 pb-xs-0">
                        <span class="dlicon holidays_message"></span>
                        <h4 class="heading pb-10 mb-0">Freeship Wordwide</h4>
                        <p class="short-desc">In ac hendrerit turpis. Aliquam ultrices dolor dolor, at commodo
                            diam feugiat</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Shipping Area End Here -->