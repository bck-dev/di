<div class="product-tab_area pt-95 pb-95">
    <div class="container custom-space-2">
        <div class="section-title_area pb-55">
            <h2 class="heading text-uppercase mb-0">New Arrival</h2>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-section_area pb-35">
                    <div class="product-tab style-01">
                        <ul class="nav product-menu">
                            <li><a class="active" data-toggle="tab" href="#new">New</a></li>
                            <li><a data-toggle="tab" href="#men">Men</a></li>
                            <li><a data-toggle="tab" href="#women">Women</a></li>
                            <li><a data-toggle="tab" href="#kids">Kids</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="tab-content sku-tab_content">
                    <div id="new" class="tab-pane active show" role="tabpanel">
                        <div class="product-item_wrap row">
                            <?php foreach ($newProducts as $product) : ?>
                            <div class="custom-xxl-col col-xl-3 col-lg-4 col-sm-6">
                                <div class="product-item">
                                    <div class="single-product">
                                        <div class="single-img border">
                                            <a href="<?php echo base_url('shop/view/') . $product->id; ?>">
                                                <img class="primary-img"
                                                    src="<?php echo base_url() . $product->coverImage ?>"
                                                    alt="Product Image">
                                            </a>
                                        </div>
                                        <div class="single-content text-center with-padding">
                                            <a class="product-name h3 mb-0"
                                                href="<?php echo base_url('shop/view/') . $product->id; ?>">
                                                <?php echo $product->productName ?>
                                            </a>
                                            <div class="price-box">
                                                <?php if ($product->discount > 0) : ?>
                                                <span class="old-price">LKR.
                                                    <?php echo number_format($product->lowestPrice, 2); ?></span>
                                                <?php endif; ?>
                                                <span class="new-price">LKR.
                                                    <?php echo number_format($product->lowestPrice * ((100 - $product->discount) / 100), 2); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div id="men" class="tab-pane show" role="tabpanel">
                        <div class="product-item_wrap row">
                            <?php foreach ($menProducts as $product) : ?>
                            <div class="custom-xxl-col col-xl-3 col-lg-4 col-sm-6">
                                <div class="product-item">
                                    <div class="single-product">
                                        <div class="single-img border">
                                            <a href="<?php echo base_url('shop/view/') . $product->id; ?>">
                                                <img class="primary-img"
                                                    src="<?php echo base_url() . $product->coverImage ?>"
                                                    alt="Product Image">
                                            </a>
                                        </div>
                                        <div class="single-content text-center with-padding">
                                            <a class="product-name h3 mb-0"
                                                href="<?php echo base_url('shop/view/') . $product->id; ?>">
                                                <?php echo $product->productName ?>
                                            </a>
                                            <div class="price-box">
                                                <span class="new-price">LKR. <?php echo $product->lowestPrice ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div id="women" class="tab-pane show" role="tabpanel">
                        <div class="product-item_wrap row">
                            <?php foreach ($womenProducts as $product) : ?>
                            <div class="custom-xxl-col col-xl-3 col-lg-4 col-sm-6">
                                <div class="product-item">
                                    <div class="single-product">
                                        <div class="single-img border">
                                            <a href="<?php echo base_url('shop/view/') . $product->id; ?>">
                                                <img class="primary-img"
                                                    src="<?php echo base_url() . $product->coverImage ?>"
                                                    alt="Product Image">
                                            </a>
                                        </div>
                                        <div class="single-content text-center with-padding">
                                            <a class="product-name h3 mb-0"
                                                href="<?php echo base_url('shop/view/') . $product->id; ?>">
                                                <?php echo $product->productName ?>
                                            </a>
                                            <div class="price-box">
                                                <span class="new-price">LKR. <?php echo $product->lowestPrice ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div id="kids" class="tab-pane show" role="tabpanel">
                        <div class="product-item_wrap row">
                            <?php foreach ($kidsProducts as $product) : ?>
                            <div class="custom-xxl-col col-xl-3 col-lg-4 col-sm-6">
                                <div class="product-item">
                                    <div class="single-product">
                                        <div class="single-img border">
                                            <a href="<?php echo base_url('shop/view/') . $product->id; ?>">
                                                <img class="primary-img"
                                                    src="<?php echo base_url() . $product->coverImage ?>"
                                                    alt="Product Image">
                                            </a>
                                        </div>
                                        <div class="single-content text-center with-padding">
                                            <a class="product-name h3 mb-0"
                                                href="<?php echo base_url('shop/view/') . $product->id; ?>">
                                                <?php echo $product->productName ?>
                                            </a>
                                            <div class="price-box">
                                                <span class="new-price">LKR. <?php echo $product->lowestPrice ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <div class="button-wrap position-center">
                        <a class="more-product text-uppercase" href="<?php echo base_url('shop') ?>">View All Products
                            <i class="lastudioicon-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>