<main class="main-content">
    <div class="shop-area pb-100">
        <div class="container custom-space">
            <div class="row">
                <div class="col-lg-12">
                    <div class="skukmart-toolbar">
                    </div>

                    <div class="shop-product-wrap list listview row">
                        <?php foreach ($vouchers as $dataRow) : ?>
                        <div class="col-12">
                            <div class="list-product_item">
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="">
                                            <img class="primary-img" src="<?php echo base_url() . $dataRow->image ?>"
                                                alt="Voucher">
                                            <img class="secondary-img" src="<?php echo base_url() . $dataRow->image ?>"
                                                alt="Voucher">
                                        </a>
                                    </div>
                                    <div class="product-content">
                                        <div class="product-detail_content">
                                            <h4 class="heading text-uppercase mb-0"><?php echo $dataRow->name ?></h4>
                                            <div class="price-box">
                                                <span class="new-price">
                                                    <h2>LKR. <?php echo number_format($dataRow->amount,2); ?></h2>
                                                </span>
                                            </div>
                                            <p class="short-desc mb-0"><b>Valid For:</b>
                                                <?php echo $dataRow->valid_period ?> Days from Purchasing</p>

                                            <div class="rating-box">
                                                <p class="short-desc mb-0"><?php echo $dataRow->description ?></p>
                                            </div>
                                        </div>
                                        <form action="<?php echo base_url('cart/addVoucher') ?>" method="post">
                                            <div class="quantity-with_btn d-flex">
                                                <div class="quantity mr-2">
                                                    <input name="voucherId" value="<?php echo $dataRow->id ?>"
                                                        type="hidden">
                                                    <input name="amount" value="<?php echo $dataRow->amount ?>"
                                                        type="hidden">
                                                    <div class="cart-plus-minus">
                                                        <input class="cart-plus-minus-box" name="qty" value="1"
                                                            type="number">
                                                        <div class="dec qtybutton">
                                                            <i class="lastudioicon-down-arrow"></i>
                                                        </div>
                                                        <div class="inc qtybutton">
                                                            <i class="lastudioicon-up-arrow"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="add-to_cart">
                                                    <button
                                                        class="skudmart-btn primary-btn primary-hover btn-lg_size">Add
                                                        to
                                                        cart</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>