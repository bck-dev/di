<div class="col-lg-3 order-lg-1 order-2">
    <div class="sidebar-area">
        <div class="widgets-area pb-35">
            <h2 class="heading text-uppercase mb-0">Type</h2>
            <div class="widgets-item">
                <input type="hidden" id="type" value="<?php echo $this->uri->segment(3); ?>">
                <ul>
                    <li>
                        <a href="<?php echo base_url('shop/type/Kids') ?>" class="<?php if ($this->uri->segment(3) == "Kids") : echo "category-active";
                                                                                                    endif; ?>">KIDS</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('shop/type/Men') ?>" class="<?php if ($this->uri->segment(3) == "Men") : echo "category-active";
                                                                                                    endif; ?>">MEN</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('shop/type/Women') ?>"
                            class="<?php if ($this->uri->segment(3) == "Women") : echo "category-active";
                                                                                                    endif; ?>">WOMEN</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="widgets-area pb-35">
            <h2 class="heading text-uppercase mb-0">Categories</h2>
            <div class="widgets-item">
                <input type="hidden" id="category" value="<?php echo $this->uri->segment(5) ?>">
                <ul>
                    <?php foreach ($categoryData as $cat) : ?>
                    <li>
                        <a href="javascript:void(0)" class="categoryItem" data-categoryId="<?php echo $cat->id ?>"
                            onclick="setCategory(this);"><?php echo $cat->categoryName; ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="widgets-area pb-40">
            <h2 class="heading text-uppercase mb-0">Size</h2>
            <div class="widgets-item">
                <input type="hidden" id="size" value="">
                <ul class="tags-item">
                    <?php foreach ($sizeData as $size) : ?>
                    <li>
                        <a href="javascript:void(0)" class="size" name="size" data-sizeId="<?php echo $size->id ?>"
                            onclick="setSize(this);"><?php echo $size->size; ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="widgets-area pb-45">
            <h2 class="heading text-uppercase mb-0">Price</h2>
            <div class="price-filter">
                <div id="slider-range"></div>
                <div class="price-slider-amount">
                    <button id="filter" class="range-btn" onclick="setPrice();">Filter</button>
                    <div class="label-input position-relative">
                        <label>price : </label>
                        <input type="text" id="amount" data-min="<?php echo $min; ?>" step="0.00"
                            data-max="<?php echo $max; ?>" name="price" readonly="" placeholder="Add Your Price" />
                        <input type="hidden" id="priceMin" value="">
                        <input type="hidden" id="priceMax" value="">
                    </div>
                </div>
            </div>
        </div>
        <div class="widgets-area pb-40">
            <h2 class="heading text-uppercase mb-0">Color</h2>
            <div class="widgets-item">
                <input type="hidden" id="color" value="">
                <ul class="color-option">
                    <?php foreach ($colorData as $color) : ?>
                    <style>
                    <?php echo '#colora'. $color->id ?>::before {
                        background-color: <?php echo $color->code ?>;
                        border: 1px solid #c2bfba !important;
                    }
                    </style>
                    <li>
                        <input type="checkbox">
                        <label id="colora<?php echo $color->id ?>" class="label-checkbox mb-0" for="color-selection-1">
                            <a href="javascript:void(0)" class="color" readonly=""
                                data-colorId="<?php echo $color->id ?>" onclick="setColor(this);">
                                <?php echo $color->color; ?>
                            </a>
                        </label>
                    </li>

                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="widgets-area pb-40">
            <div class="price-filter">
                <div class="price-slider-amount">
                    <a href="<?php echo base_url('shop/') ?>">
                        <button id="" class="range-btn">Reset Filter</button></a>
                    <div class="label-input position-relative">

                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="widgets-area">
            <h2 class="heading text-uppercase mb-0">Label</h2>
                <div class="widgets-item">
                    <ul class="tags-item">
                        <?php foreach ($labelData as $label) : ?>
                        <li>
                            <a href="javascript:void(0)"><?php echo $label->label; ?></a>
                        </li>
                        <?php endforeach; ?>
                                            
                    </ul>
                </div>
        </div>-->

    </div>
</div>

<script>
function setColor(element) {
    $('#color').val(($(element).data('colorid')));
    $('.color').removeClass('selected-filter');
    $(element).addClass('selected-filter');

    filter();
}

function setSize(element) {
    $('#size').val(($(element).data('sizeid')));
    $('.size').removeClass('selected-filter');
    $(element).addClass('selected-filter');

    filter();
}

function setCategory(element) {
    $('#category').val(($(element).data('categoryid')));
    $('.categoryItem').removeClass('category-active');
    $(element).addClass('category-active');

    filter();
}

function setPrice() {
    var amount = $('#amount').val();
    var amounts = amount.split(" - ");
    var priceMin = amounts[0].replace("LKR.", "");
    var priceMax = amounts[1].replace("LKR.", "");

    $('#priceMin').val(priceMin);
    $('#priceMax').val(priceMax);

    filter();
}

function filter() {
    var type = $('#type').val();
    var category = $('#category').val();
    var color = $('#color').val();
    var size = $('#size').val();
    var priceMin = $('#priceMin').val();
    var priceMax = $('#priceMax').val();
    //var pageNo = $(id).attr('data-id'); 
    var base_url = "<?php echo base_url(); ?>";

    $.ajax({
        type: 'GET',
        url: '<?php echo base_url('filterProducts') ?>',
        data: {
            min: priceMin,
            max: priceMax,
            category: category,
            size: size,
            color: color,
            type: type
        },
        dataType: 'json',
        success: function(results) {
            console.log(results);
            const nf = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'LKR',
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
            });

            var products = "";
            jQuery.each(results['products'], function(key, val) {
                products = products + '<div class="col-12">';
                products = products + '<div class="product-item">';
                products = products + '<div class="single-product">';
                products = products + '<div class="single-img">';
                if (val['stock'] < 1) {
                    products = products +
                        '<span class="out-of-stock"><i class="far fa-times-circle red"></i> Out of Stock</span>';
                }
                products = products + '<a href="' + base_url + '/shop/view/' +
                    val['id'] + '">';
                products = products + '<img class="primary-img"  src="' +
                    base_url + val['coverImage'] + '">';
                products = products + '<img class="secondary-img" src="' +
                    base_url + val['coverImage'] + '">';
                products = products + '</a>';
                products = products + '</div>';
                products = products +
                    '<div class="single-content">';
                products = products + '<h3 class="product-name mb-0">';
                products = products + '<a href="' + base_url + '/shop/view/' +
                    val['id'] + '">';
                products = products + val['productName'];
                products = products + '</a>';
                products = products + '</h3>';
                products = products + '<div class="price-box">';
                if (val['discount'] > 0) {
                    products = products + '<span class="old-price">';
                    products = products + nf.format(val['lowestPrice'])
                    products = products + '</span>';
                }
                products = products + '<span class="new-price">';
                products = products + nf.format(val['lowestPrice'] * ((100 - val[
                    'discount']) / 100));
                products = products + '</span>';
                products = products + '</div>';
                products = products + '</div>';
                products = products + '</div>';
                products = products + '</div>';
                products = products + '</div>';
            });

            $('#products').html(products);

            if (results.pagination) {
                var pagination = "";
                jQuery.each(results.pagination.buttons, function(key, val) {

                    if (val['status'] == "current") {
                        pagination = pagination + '<li class="active">';
                    } else {
                        pagination = pagination + '<li>';
                    }
                    pagination = pagination + '<a href="#" class="next" data-id="' + val['id'] +
                        '" onclick="getPaginationData(this); ">';

                    pagination = pagination + val['value'];
                    pagination = pagination + '</a>';
                    pagination = pagination + '</li>';
                });
                $('#pagination').html(pagination);

                if (results.pagination.count < 12) {
                    var limit = results.pagination.count;

                } else {
                    var limit = results.pagination.end;
                }
            }

            if (results.totalProducts > 0) {
                $('#start').html(results.pagination.start + '-' + limit);
                $('#total').html(results.pagination.count);
            } else {
                $('.product-page_count').html("<p>No Product Available for the Selected Filters</p>");
            }

        },

        error: function() {
            console.log('error');
        }

    });
}
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>