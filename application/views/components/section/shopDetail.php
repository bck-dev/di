 <!-- Begin Breadcrumb Area -->
 <div class="breadcrumb-area without-bg with-border">
     <div class="container custom-space h-100">
         <div class="breadcrumb-content white-text h-100">
             <h1 class="text-uppercase mb-0">Shop </h1>
             <ul>
                 <li><a href="<?php echo base_url(); ?>">Home</a></li>
                 <li>Shop</li>
                 <?php if ($this->uri->segment(3)) : echo "<li>".$this->uri->segment(3)."</li>"; endif; ?>

             </ul>
         </div>
     </div>
 </div>
 <!-- Breadcrumb Area End Here -->

 <main class="main-content">
     <div class="shop-area with-sidebar pb-100">
         <div class="container custom-space">
             <div class="row">
                 <?php $this->load->view('components/section/shopDetailSidebar'); ?>
                 <div class="col-lg-9 order-lg-2 order-1">
                     <div class="skukmart-toolbar">
                         <div class="product-page_count">
                             <p>Showing
                                 <span id="start"><?php echo $pagination['start'].'-'.$pagination['end']; ?></span>
                                 <!-- <span id="currentTab"><?php echo $count; ?></span>  -->
                                 Out of <span id="total"><?php echo $pagination['count']; ?></span> Products
                                 <?php if($categoryId): echo "for "; echo $category; endif; ?>
                             </p>
                         </div>
                         <!--<div class="toolbar-right_side">
                                    <div class="product-view-mode">
                                        <a href="javascript:void(0)" class="active grid-3" data-target="gridview-4" title="Grid View">
                                            <i class="lastudioicon-microsoft"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="list" data-target="listview" title="List View">
                                            <i class="lastudioicon-list-bullet-2"></i>
                                        </a>
                                    </div>                                    
                                </div>-->
                     </div>

                     <div class="shop-product-wrap grid gridview-4 row" id="products">
                         <?php foreach ($products as $product) : ?>
                         <div class="col-12">
                             <div class="product-item">
                                 <div class="single-product">
                                     <div class="single-img">
                                         <?php if ($product->stock<1): ?>
                                         <span class="out-of-stock"><i class="far fa-times-circle red"></i> Out of
                                             Stock</span>
                                         <?php endif; ?>
                                         <a href="<?php echo base_url('shop/view/') . $product->id; ?>">
                                             <img class="primary-img"
                                                 src="<?php echo base_url() . $product->coverImage ?>">
                                             <img class="secondary-img"
                                                 src="<?php echo base_url() . $product->coverImage ?>"
                                                 alt="Product Image">
                                         </a>
                                     </div>
                                     <div class="single-content">
                                         <h3 class="product-name mb-0">
                                             <a href="<?php echo base_url('shop/view/') . $product->id; ?>">
                                                 <?php echo $product->productName ?>
                                             </a>
                                         </h3>
                                         <div class="price-box">
                                             <?php if ($product->discount > 0) : ?>
                                             <span class="old-price">LKR.
                                                 <?php echo number_format($product->lowestPrice, 2); ?></span>
                                             <?php endif; ?>
                                             <span class="new-price">LKR.
                                                 <?php echo number_format($product->lowestPrice * ((100 - $product->discount) / 100), 2); ?></span>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <?php endforeach; ?>
                     </div>

                 </div>

             </div>
         </div>
         <?php $this->load->view('components/common/pagination'); ?>
     </div>

 </main>