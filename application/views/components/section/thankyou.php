<div class="checkout-area pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-12">
                <h3>Thank You!</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
                    and scrambled it to make a type specimen book. </p>

                <h5>Delivery Address</h5>
                <p>
                    <?php echo $orderData->first_name." ".$orderData->last_name; ?><br />
                    <?php echo $orderData->address1; ?><br />
                    <?php echo $orderData->address2; ?><br />
                    <?php echo $orderData->city; ?><br />
                    <br />
                    <?php echo $orderData->email; ?><br />
                    <?php echo $orderData->telephone; ?><br />
                </p>
            </div>
            <div class="col-lg-6 col-12">
                <div class="your-order">
                    <h3>Your order</h3>
                    <div class="your-order-table table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="cart-product-name">Product</th>
                                    <th class="cart-product-total">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($orderItems as $i) : ?>
                                <tr class="cart_item">

                                    <td class="cart-product-name"> <?php echo $i->productName ?><strong
                                            class="product-quantity">
                                            × <?php echo $i->quantity ?></strong></td>
                                    <td class="cart-product-total" align="right"><span class="amount">LKR
                                            <?php echo number_format($i->total, 2); ?></span>
                                    </td>

                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr class="cart-subtotal">
                                    <th>Cart Subtotal</th>

                                    <td align="right"><span class="amount">LKR
                                            <?php echo number_format($orderData->cart_total, 2 )?></span>
                                    </td>
                                </tr>
                                <?php if ($orderData->deduction_amount >0): ?>
                                <tr class="cart-subtotal">
                                    <th>Voucher</th>

                                    <td align="right"><span class="amount">LKR
                                            <?php echo number_format($orderData->deduction_amount, 2 )?></span>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <tr class="cart-subtotal">
                                    <th>Delivery Fees</th>
                                    <td align="right"><span class="amount">LKR
                                            <?php echo number_format($orderData->delivery_fees, 2 )?></span>
                                    </td>
                                </tr>
                                <tr class="order-total">
                                    <th>Grand Total</th>
                                    <td align="right"><strong><span class="amount">LKR
                                                <?php echo number_format($orderData->grand_total,2) ?></span></strong>

                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>