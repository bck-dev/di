<main class="main-content pt-100 pb-100">
    <!-- Begin Main Content Area -->
    <div class="skudmart-cart-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form action="">
                        <div class="table-content table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="skudmart-product_remove">remove</th>
                                        <th class="skudmart-product-thumbnail">images</th>
                                        <th class="cart-product-name">Product</th>
                                        <th class="skudmart-product-price">Unit Price</th>
                                        <th class="skudmart-product-quantity">Quantity</th>
                                        <th class="skudmart-product-subtotal">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($allData as $item) : ?>
                                    <tr>
                                        <td class="skudmart-product_remove">
                                            <a href="<?php echo base_url('cart/delete/'); ?><?php echo $item->id ?>">
                                                <i class="lastudioicon-e-remove" title="Remove"></i>
                                            </a>
                                        </td>
                                        <td class="skudmart-product-thumbnail">
                                            <a
                                                href="<?php if($item->productOptionId==0):?>javascript:void(0)<?php else: echo base_url('shop/view/') . $item->productId; endif; ?>">
                                                <img src="<?php echo base_url() . $item->coverImage ?>"
                                                    alt="Cart Thumbnail" style="max-width: 80px;">
                                            </a>
                                        </td>
                                        <td class="skudmart-product-name text-left">
                                            <a
                                                href="<?php if($item->productOptionId==0):?>javascript:void(0)<?php else: echo base_url('shop/view/') . $item->productId; endif; ?>"><?php echo $item->productName ?></a>
                                            <?php if($item->productOptionId!=0):?>
                                            <div class="d-flex">
                                                <div class="color-box-outline">
                                                    <div class="color-box"
                                                        style="background-color: <?php echo $item->code; ?>"></div>

                                                </div>
                                                <div class="size-box-outline">
                                                    <?php echo $item->size; ?>
                                                </div>
                                            </div>
                                            <?php endif;?>
                                        </td>
                                        <td class="skudmart-product-price">
                                            <span class="amount">
                                                <input type="hidden" id="prices" class="price" />
                                                LKR.<?php echo number_format($item->price, 2); ?>
                                            </span>
                                        </td>
                                        <td class="quantity">
                                            <!-- <label>Quantity</label> -->
                                            <div class="cart-plus-minus">
                                                <input class="cart-plus-minus-box qty"
                                                    value='<?php echo $item->quantity; ?>'
                                                    id="qty<?php echo $item->id; ?>" data-id="<?php echo $item->id; ?>"
                                                    type="text">
                                                <div class="dec qtybutton" onclick="decQty(this);"
                                                    data-id="<?php echo $item->id; ?>">
                                                    <input type="hidden" id="minqty" value="">
                                                    <i class="lastudioicon-down-arrow"></i>
                                                </div>
                                                <div class="inc qtybutton" onclick="incQty(this);"
                                                    data-id="<?php echo $item->id; ?>">
                                                    <input type="hidden" id="maxqty" value="">
                                                    <i class="lastudioicon-up-arrow"></i>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="product-subtotal">
                                            <span class="amount"
                                                id="amount<?php echo $item->id; ?>">LKR.<?php echo number_format($item->total, 2); ?></span>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="coupon-all">
                                    <div class="coupon d-flex">
                                        <input id="reference" class="input-text mr-3" name="reference"
                                            value="<?php echo $this->session->userdata('reference'); ?>"
                                            placeholder="Voucher Number" type="text" style="min-width: 150px;">
                                        <input type="hidden" id="deductionAmount"
                                            value="<?php echo $this->session->userdata('deduction_amount'); ?>" />
                                        <div class="skudmart-btn primary-btn primary-hover input-btn"
                                            style="cursor: pointer;" onclick="redeem();">
                                            Apply</div>
                                    </div>


                                </div>

                                <div class="mt-2" id="couponMsg"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5 ml-auto">
                                <div class="cart-page-total">
                                    <h2>Cart totals</h2>
                                    <ul id="totalUl">
                                        <li>Total
                                            <input id="totalInput" type="hidden" value="<?php echo $cartTotal; ?>" />
                                            <span id="total"
                                                class="total">LKR.<?php echo number_format($cartTotal, 2)?></span>
                                        </li>
                                        <?php if ($this->session->userdata('deduction_amount')>0): ?>
                                        <li>Voucher <span id="deduction"
                                                class="total">LKR.<?php echo number_format($this->session->userdata('deduction_amount'), 2); ?></span>
                                        </li>
                                        <?php endif; ?>
                                        <li>Delivery Charge will be added in checkout page</li>
                                        <?php if ($this->session->userdata('deduction_amount')>0): ?>
                                        <li>Grand Total
                                            <?php
                                            $grandTotal = $cartTotal - $this->session->userdata('deduction_amount');
                                            if($grandTotal<0){
                                                $grandTotal = 0;
                                            }
                                        ?>
                                            <span id="GrandTotal"
                                                class="total">LKR.<?php echo number_format($grandTotal, 2); ?></span>
                                        </li>
                                        <?php endif; ?>
                                    </ul>
                                    <a href="<?php echo base_url('checkout') ?>">Proceed to checkout</a>
                                </div>
                            </div>
                        </div>
                </div>
            </div>


            </form>
        </div>
    </div>
    </div>
    </div>
    <!-- Main Content Area End Here -->
</main>


<script>
$('input.qty').each(function() {
    $(this).change(function() {
        var qty = $(this).val();
        var id = $(this).attr('data-id');

        updateCart(qty, id);
    });
});


function incQty(element) {
    var id = $(element).attr('data-id');
    var qty = parseInt($('#qty' + id).val());

    var newQty = qty + 1;
    $('#qty' + id).val(newQty);
    updateCart(newQty, id);
}

function decQty(element) {
    var id = $(element).attr('data-id');
    var qty = parseInt($('#qty' + id).val());

    if (qty > 1) {
        var newQty = qty - 1;
    } else {
        var newQty = 1;
    }

    $('#qty' + id).val(newQty);
    updateCart(newQty, id);
}

function updateCart(qty, id) {

    $.ajax({
        type: 'GET',
        url: '<?php echo base_url('updateCart') ?>',
        data: {
            qty: qty,
            id: id
        },
        dataType: 'json',
        success: function(results) {
            const nf = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'LKR',
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
            });

            $('#amount' + id).html(nf.format(results['amount']));
            $('#total').html(nf.format(results['cartTotal']));

            calculateGrandTotal(results['cartTotal']);
        }
    });
}

function redeem() {
    var reference = $('#reference').val();

    $.ajax({
        type: 'GET',
        url: '<?php echo base_url('redeem') ?>',
        data: {
            reference: reference
        },
        dataType: 'json',
        success: function(results) {

            $('#deductionAmount').val(results['amount']);
            calculateGrandTotal();
            $('#couponMsg').html(results['msg']);
        }
    });
}

function calculateGrandTotal(cartTotal) {
    var deduct = $('#deductionAmount').val();
    var cartTotal = $('#totalInput').val();

    const nf = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'LKR',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
    });

    if (deduct > 0) {
        if (parseInt(cartTotal) < parseInt(deduct)) {
            deduct = cartTotal;
        }
        var grandTotal = parseInt(cartTotal) - parseInt(deduct);
        var html = "";
        html = html + '<li>Total';
        html = html + '<input id="totalInput" type="hidden" value="' + cartTotal + '" />';
        html = html + '<span id="total" class="total">' + nf.format(cartTotal) + '</span></li>';
        html = html + '<li>Voucher <span id="deduction" class="total">' + nf.format(deduct) + '</span></li>';
        html = html + '<li>Delivery Charge will be added in checkout page</li>';
        html = html + '<li>Grand Total <span id="GrandTotal" class="total">' + nf.format(grandTotal) + '</span></li>';

    } else {
        var html = "";
        html = html + '<li>Total';
        html = html + '<input id="totalInput" type="hidden" value="' + cartTotal + '" />';
        html = html + '<span id="total" class="total">' + nf.format(cartTotal) + '</span></li>';
        html = html + '<li>Delivery Charge will be added in checkout page</li>';
    }

    $('#totalUl').html(html);
}
</script>