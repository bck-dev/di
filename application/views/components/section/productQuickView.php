<!-- Begin Modal Area -->
<div class="modal fade modal-wrapper" id="exampleModalCenter">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close-btn" data-dismiss="modal" aria-label="Close">
                    <i class="lastudioicon-e-remove"></i>
                </button>
                <div class="modal-inner-area row no-gutters">
                    <div class="col-md-6">
                        <div class="swiper-container modal-slider circle-arrow with-bg_white swiper-arrow_sm">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="single-img">
                                        <img class="img-full"
                                            src="<?php echo base_url(); ?>assets/template/images/product/medium-size/1-1.jpg"
                                            alt="Product Image">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="single-img">
                                        <img class="img-full"
                                            src="<?php echo base_url(); ?>assets/template/images/product/medium-size/1-2.jpg"
                                            alt="Product Image">
                                    </div>
                                </div>
                            </div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="inner-content">
                            <h3 class="product-name mb-0">
                                <a class="text-uppercase" href="product-demo-01.html">Labore Pennant</a>
                            </h3>
                            <div class="product-info">
                                <div class="customer-feedback">
                                    <div class="rating-box">
                                        <ul>
                                            <li><i class="lastudioicon-star-rate-1"></i></li>
                                            <li><i class="lastudioicon-star-rate-1"></i></li>
                                            <li><i class="lastudioicon-star-rate-1"></i></li>
                                            <li><i class="lastudioicon-star-rate-1"></i></li>
                                            <li><i class="lastudioicon-star-rate-1"></i></li>
                                        </ul>
                                    </div>
                                    <span>Customer Reviews</span>
                                </div>
                                <div class="product-stock">
                                    <span>09 Sold40 / in stock</span>
                                </div>
                            </div>
                            <div class="price-box">
                                <span class="new-price">£11.05</span>
                            </div>
                            <p class="short-desc mb-0">Sed ligula sapien, fermentum id est eget, viverra auctor
                                sem. Vivamus maximus enim vitae urna porta, ut euismod nibh lacinia.
                                Pellentesque at diam sed libero tincidunt feugiat. Morbi efficitur augue leo.
                                Morbi convallis augue viverra purus gravida feugiat.</p>
                            <div class="button-wrap">
                                <ul class="group-btn">
                                    <li>
                                        <a class="btn-with_padding primary-btn primary-hover" href="cart.html">Add to
                                            cart</a>
                                    </li>
                                    <li>
                                        <a class="" href="wishlist.html">
                                            <i class="lastudioicon-heart-2"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="" href="wishlist.html">
                                            <i class="lastudioicon-compare"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="product-meta">
                                <ul>
                                    <li>
                                        <span>SKU: REF. LA-35</span>
                                    </li>
                                    <li>
                                        <span>Category:</span>
                                        <a href="javascript:void(0)">Kids</a>
                                    </li>
                                    <li>
                                        <span>Tag:</span>
                                        <a href="javascript:void(0)">demo 01</a>
                                    </li>
                                </ul>
                            </div>
                            <ul class="social-link">
                                <li class="title">Share This Product:</li>
                                <li class="facebook">
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Facebook">
                                        <i class="lastudioicon-b-facebook"></i>
                                    </a>
                                </li>
                                <li class="twitter">
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Twitter">
                                        <i class="lastudioicon-b-twitter-circled"></i>
                                    </a>
                                </li>
                                <li class="pinterest">
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Pinterest">
                                        <i class="lastudioicon-b-pinterest-circled"></i>
                                    </a>
                                </li>
                                <li class="email">
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Email">
                                        <i class="lastudioicon-mail"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Area End Here -->