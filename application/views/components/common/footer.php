<!-- Begin Footer Area -->
<div class="footer-area bg-dark_color">
    <div class="footer-top_area pt-55 pb-45">
        <div class="container custom-space-2">
            <div class="row align-items-center">
                <div class="col-lg-4">
                    <div class="footer-widgets_area">
                        <h2 class="footer-title mb-0">
                            <a href="<?php echo base_url() ?>"><img
                                    src="<?php echo base_url(); ?>assets/images/logo.svg" style="width: 80px !important"
                                    alt="Header Logo"></a>
                        </h2>
                        <p class="short-desc mb-0 pb-0"> Building upon the legacy of over 80 years, we are stepping into
                            a new chapter while keeping true to our watchword and philosophy that guided us throughout
                            our impressive journey.</p>
                        <ul class="social-link pt-20">
                            <li class="facebook">
                                <a href="javascript:void(0)" data-toggle="tooltip" title="Facebook">
                                    <i class="lastudioicon-b-facebook"></i>
                                </a>
                            </li>
                            <li class="email">
                                <a href="javascript:void(0)" data-toggle="tooltip" title="Email">
                                    <i class="lastudioicon-mail"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="footer-widgets_wrap row">
                        <div class="col-md-3 col-sm-6">
                            <div class="footer-widgets_area">
                                <h3 class="heading text-uppercase mb-0">Company</h3>
                                <ul class="footer-widget">
                                    <li>
                                        <a href="javascript:void(0)">About Us</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">Contact Us</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">FAQ</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">Terms & Conditions</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="footer-widgets_area">
                                <h3 class="heading text-uppercase mb-0">Shop</h3>
                                <ul class="footer-widget">
                                    <li>
                                        <a href="<?php echo base_url('shop/type/Men') ?>">Men</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('shop/type/Women') ?>">Women</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('shop/type/Kids') ?>">Kids</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-6">
                            <div class="footer-widgets_area">
                                <h3 class="heading text-uppercase mb-0">Contact info</h3>
                                <ul class="footer-widget">
                                    <li class="widgets-mail">
                                        <span>Email:</span>
                                        <a class="text-lowercase" href="mailto://info@example.com">info@example.com</a>
                                    </li>
                                    <li class="widgets-phone">
                                        <span>Phone:</span>
                                        <a href="tel://+94115535417">+94 115 535 417</a>
                                    </li>
                                    <li>
                                        <span>Open time: 9:00 - 19:00, Monday - Saturday</span>
                                    </li>
                                    <li>
                                        <a target="_blank"
                                            href="https://www.google.lk/maps/place/89,+4+Galle+Rd,+Colombo/@6.9202344,79.844208,17z/data=!3m1!4b1!4m5!3m4!1s0x3ae25938b33e168b:0x9804b0561c77e353!8m2!3d6.9202344!4d79.8463967">Address:
                                            No. 89, Galle Road, Colombo 04</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom_area pt-0">
        <div class="container custom-space-2">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright">
                        <span>&copy; <?php echo date('Y'); ?>
                            <a href="index.html">Di Leather.</a>
                            <a href="https://bckonnect.com/" target="_blank">Designed By BC Konnect.</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer Area End Here -->

<!-- Begin Scroll To Top -->
<a class="scroll-to-top" href="">
    <i class="lastudioicon-arrow-up"></i>
</a>
<!-- Scroll To Top End Here -->

</div>