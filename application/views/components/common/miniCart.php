<div class="offcanvas-minicart_wrapper" id="miniCart">
    <div class="offcanvas-body">
        <div class="minicart-content">
            <div class="minicart-heading">
                <h4 class="mb-0">Shopping Cart</h4>
                <a href="#" class="btn-close"><i class="lastudioicon-e-remove"></i></a>
            </div>
            <ul class="minicart-list">
                <?php foreach ($allData as $product) : ?>
                <li class="minicart-product">
                    <a class="product-item_remove" href="javascript:void(0)"><i class="lastudioicon-e-remove"></i></a>
                    <div class="product-item_img">
                        <img class="img-full" src="<?php echo base_url() . $product->coverImage ?>"
                            alt="skudmart's Product Image">
                    </div>
                    <div class="product-item_content">
                        <a class="product-item_title" href="shop-fullwidth.html"><?php echo $product->ProductName ?></a>
                        <span class="product-item_quantity"><?php echo $product->quantity ?> x
                            LKR.<?php echo number_format($product->price, 2); ?></span>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="minicart-item_total">
            <span>Subtotal</span>
            <span class="ammount">LKR. <?php echo $cartTotal ?></span>
        </div>
        <div class="group-btn_wrap">
            <a href="<?php echo base_url('cart'); ?>"
                class="skudmart-btn primary-btn primary-hover fullwidth-btn mb-2">View
                Cart</a>
            <a href="<?php echo base_url('checkout'); ?>"
                class="skudmart-btn primary-btn primary-hover fullwidth-btn">Checkout</a>
        </div>
    </div>
</div>
<div class="global-overlay"></div>
</header>
<!-- Main Header Area End Here -->