<div class="middle-header_area pt-30 pb-30">
    <div class="container custom-space">
        <div class="row">
            <div class="col-lg-4 d-none d-lg-block">
                <div class="search-box with-border_bottom with-dark_color different-width">
                    <form id="searchbox" action="<?php echo base_url('search'); ?>" method='GET'>
                        <input class="input-field w-100" type="text" name="search" value="<?php echo $search; ?>"
                            placeholder="Search products..">
                        <button class="search-btn" type="submit">
                            <i class="lastudioicon-zoom-2"></i>
                        </button>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 col-6">

                <div class="header-logo">
                    <a href="<?php echo base_url(); ?>">
                        <img src="<?php echo base_url(); ?>assets/images/logo.svg" style="width: 80px !important"
                            alt="Header Logo">
                    </a>
                </div>

            </div>
            <div class="col-lg-4 col-6">
                <div class="header-right">
                    <ul>
                        <li class="d-block d-lg-none">
                            <a href="#searchBar" class="search-btn toolbar-btn">
                                <i class="lastudioicon-zoom-1"></i>
                            </a>
                        </li>
                        <li class="minicart-wrap">
                            <a href="#miniCart" class="minicart-btn toolbar-btn">
                                <i class="cart-i_icon lastudioicon-shopping-cart-3"></i>
                            </a>
                            <span class="cart-counter"><?php echo $countCart ?></span>
                        </li>
                        <li class="mobile-menu_wrap d-block d-lg-none">
                            <a href="#mobileMenu" class="mobile-menu_btn toolbar-btn pl-0">
                                <i class="lastudioicon-menu-3-1"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>