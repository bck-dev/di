<?php if($pagination['buttons']): ?>
<div class="pagination-area position-center">
    <ul class="pagination-box" id="pagination">
        <?php foreach ($pagination['buttons'] as $button ):?>
        <li <?php if($button['status']=="current"): echo 'class="active"'; endif; ?>>
            <a href="#" data-id="<?php echo $button['id']; ?>" class="next" onclick="getPaginationData(this);">
                <?php echo $button['value']; ?>
                <!--<i class="lastudioicon-arrow-right"></i>-->
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?>

<input type="hidden" id="limit" value="<?php echo $pagination['limit'];?>" />
<input type="hidden" id="cnt" value="<?php echo $pagination['count'];?>" />
<input type="hidden" id="str" value="<?php echo $pagination['start'];?>" />
<input type="hidden" id="end" value="<?php echo $pagination['end'];?>" />


<script>
function getPaginationData(id) {
    var type = $('#type').val();
    var category = $('#category').val();
    var color = $('#color').val();
    var size = $('#size').val();
    var priceMin = $('#priceMin').val();
    var priceMax = $('#priceMax').val();
    var base_url = "<?php echo base_url(); ?>";
    var pageNo = $(id).attr('data-id');
    $.ajax({
        type: 'GET',
        url: '<?php echo base_url('filterProducts') ?>',
        data: {
            min: priceMin,
            max: priceMax,
            category: category,
            size: size,
            color: color,
            pageNo: pageNo,
            type: type
        },
        dataType: 'json',
        success: function(results) {
            // console.log(results);
            const nf = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'LKR',
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
            });

            var products = "";
            jQuery.each(results['products'], function(key, val) {
                products = products + '<div class="col-12">';
                products = products + '<div class="product-item">';
                products = products + '<div class="single-product">';
                products = products + '<div class="single-img">';
                if (val['stock'] < 1) {
                    products = products +
                        '<span class="out-of-stock"><i class="far fa-times-circle red"></i> Out of Stock</span>';
                }
                products = products + '<a href="' + base_url + '/shop/view/' +
                    val['id'] + '">';
                products = products + '<img class="primary-img"  src="' +
                    base_url + val['coverImage'] + '">';
                products = products + '<img class="secondary-img" src="' +
                    base_url + val['coverImage'] + '">';
                products = products + '</a>';
                products = products + '</div>';
                products = products +
                    '<div class="single-content">';
                products = products + '<h3 class="product-name mb-0">';
                products = products + '<a href="' + base_url + '/shop/view/' +
                    val['id'] + '">';
                products = products + val['productName'];
                products = products + '</a>';
                products = products + '</h3>';
                products = products + '<div class="price-box">';
                if (val['discount'] > 0) {
                    products = products + '<span class="old-price">';
                    products = products + nf.format(val['lowestPrice']);
                    products = products + '</span>';
                }
                products = products + '<span class="new-price">';
                products = products + nf.format(val['lowestPrice'] * ((100 - val[
                    'discount']) / 100));
                products = products + '</span>';

                products = products + '</div>';
                products = products + '</div>';
                products = products + '</div>';
                products = products + '</div>';
                products = products + '</div>';
            });
            $('#products').html(products);

            if (results.pagination) {
                var pagination = "";
                jQuery.each(results.pagination.buttons, function(key, val) {

                    if (val['status'] == "current") {
                        pagination = pagination + '<li class="active">';
                    } else {
                        pagination = pagination + '<li>';
                    }
                    pagination = pagination + '<a href="#" class="next" data-id="' + val['id'] +
                        '" onclick="getPaginationData(this);">';

                    pagination = pagination + val['value'];
                    pagination = pagination + '</a>';
                    pagination = pagination + '</li>';
                });
                $('#pagination').html(pagination);

                if (results.pagination.count < 12) {
                    var limit = results.pagination.count;
                } else {
                    var limit = results.pagination.end;
                }
            }

            if (results.totalProducts > 0) {
                $('#start').html(results.pagination.start + '-' + limit);
                $('#total').html(results.pagination.count);
            } else {
                $('.product-page_count').html("<p>No Product Available for the Selected Filters</p>");
            }
        },

        error: function() {
            console.log('error');
        }

    });
}
</script>