<!-- Begin Breadcrumb Area -->
<div class="breadcrumb-area without-bg with-border">
    <div class="container custom-space h-100">
        <div class="breadcrumb-content white-text h-100">
            <h1 class="text-uppercase mb-0"><?php echo $pageName; ?></h1>
            <ul>
                <li><a href="home">Home</a></li>
                <li><?php echo $pageName; ?></li>
            </ul>
        </div>
    </div>
</div>
<!-- Breadcrumb Area End Here -->