<!DOCTYPE html>
<html class="no-js" lang="en">

<head class="loadOverlay">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Di Leather | <?php echo $pageName; ?></title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/template/images/favicon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/vendor/bootstrap.min.css">
    <!-- La Studio Icon -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/vendor/lastudioicon.css">
    <!-- DL Icon -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/vendor/dlicon.css">
    <!-- Animation -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/plugins/animate.css">
    <!-- jQuery Ui -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/plugins/jquery-ui.min.css">

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/plugins/swiper.css">
    <!-- Slick CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/plugins/slick.min.css">
    <!-- Nice Select -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/plugins/nice-select.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/plugins/lightgallery.css">
    <!-- Style CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/css/style.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/custom/css/custom.css">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?php echo base_url('assets/adminLte/plugins/fontawesome-free/css/all.min.css') ?>">


    <!-- Modernizer JS -->
    <script src="<?php echo base_url(); ?>assets/template/js/vendor/modernizr-2.8.3.min.js"></script>
    <!-- jQuery JS -->
    <script src="<?php echo base_url(); ?>assets/template/js/vendor/jquery-3.5.0.min.js"></script>
    <!-- jQuery Migrate JS -->
    <script src="<?php echo base_url(); ?>assets/template/js/vendor/jquery-migrate-3.1.0.min.js"></script>

</head>

<body class="template-color-2">
    <div class="main-wrapper">
        <!-- Begin Main Header Area -->
        <header class="main-header_area">