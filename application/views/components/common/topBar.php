<div class="header-top_area bg-dark_color white-text">
    <div class="container custom-space">
        <div class="row">
            <div class="col-lg-12">
                <div class="header-top style-02">

                    <a class="header-shipping d-none d-lg-block" href="shop-fullwidth.html">Islandwide delivery -
                        <span>Super Fast Delivery</span></a>
                    <ul class="social-link d-none d-lg-block">
                        <li class="facebook">
                            <a href="javascript:void(0)">
                                <i class="lastudioicon-b-facebook"></i>
                            </a>
                        </li>
                        <li class="email">
                            <a href="javascript:void(0)">
                                <i class="lastudioicon-mail"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>