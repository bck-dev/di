<div class="main-header_area d-none d-lg-block">
    <div class="main-header header-style-07 bg-smoke_color">
        <div class="container custom-space">
            <div class="inner-container position-relative">
                <div class="row main-header_nav position-static p-lg-0">
                    <div class="col-xl-9 col-lg-9 position-static">
                        <div class="main-menu menu-style_three">
                            <nav class="main-nav">
                                <ul>
                                    <li>
                                        <a class="<?php if($pageName=="Home"): echo 'active'; endif; ?>"
                                            href="<?php echo base_url() ?>">Home</a>
                                    </li>
                                    <li>
                                        <a class="<?php if($pageName=="About"): echo 'active'; endif; ?>"
                                            href="<?php echo base_url('about') ?>">About Us</a>
                                    </li>
                                    <li>
                                        <a class="" href="<?php echo base_url('shop/type/Men') ?>">Men
                                            <i class="lastudioicon-down-arrow"></i>
                                        </a>
                                        <ul class="skudmart-dropdown">

                                            <?php foreach ($categories as $cat): ?>
                                            <li>
                                                <a
                                                    href="<?php echo base_url('shop/type/Men/category/').$cat->id; ?>"><?php echo $cat->categoryName; ?></a>
                                            </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                    <li>
                                        <a class="" href="<?php echo base_url('shop/type/Women') ?>">Women
                                            <i class="lastudioicon-down-arrow"></i>
                                        </a>
                                        <ul class="skudmart-dropdown">

                                            <?php foreach ($categories as $cat): ?>
                                            <li>
                                                <a
                                                    href="<?php echo base_url('shop/type/Women/category/').$cat->id; ?>"><?php echo $cat->categoryName; ?></a>
                                            </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                    <li>
                                        <a class="" href="<?php echo base_url('shop/type/Kids') ?>">Kids
                                            <i class="lastudioicon-down-arrow"></i>
                                        </a>
                                        <ul class="skudmart-dropdown">

                                            <?php foreach ($categories as $cat): ?>
                                            <li>
                                                <a
                                                    href="<?php echo base_url('shop/type/Kids/category/').$cat->id; ?>"><?php echo $cat->categoryName; ?></a>
                                            </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                    <li>
                                        <a class="<?php if($pageName=="Voucher"): echo 'active'; endif; ?>"
                                            href="<?php echo base_url('voucher') ?>">Gift Voucher</a>
                                    </li>
                                    <li>
                                        <a class="<?php if($pageName=="FAQ"): echo 'active'; endif; ?>"
                                            href="<?php echo base_url('faq') ?>">FAQ</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="contact-number">
                            <span>
                                Hot line:
                                <a href="tel://+94115535417">+94 115 535 417 </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mobile-menu_wrapper" id="mobileMenu">
    <div class="offcanvas-body">
        <div class="inner-body">
            <div class="offcanvas-top">
                <a href="#" class="btn-close"><i class="lastudioicon-e-remove"></i></a>
            </div>
            <div class="offcanvas-menu_area">
                <nav class="offcanvas-navigation">
                    <ul class="mobile-menu">
                        <li>
                            <a class="<?php if($pageName=="Home"): echo 'active'; endif; ?>"
                                href="<?php echo base_url() ?>">Home</a>
                        </li>
                        <li>
                            <a class="<?php if($pageName=="About"): echo 'active'; endif; ?>"
                                href="<?php echo base_url() ?>">About Us</a>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="#">
                                <span class="mm-text">Men
                                    <i class="lastudioicon-down-arrow"></i>
                                </span>
                            </a>
                            <ul class="sub-menu">
                                <?php foreach ($categories as $cat): ?>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo base_url('shop/type/Men/category/').$cat->id; ?>">
                                        <span class="mm-text"><?php echo $cat->categoryName; ?></span>
                                    </a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="#">
                                <span class="mm-text">Women
                                    <i class="lastudioicon-down-arrow"></i>
                                </span>
                            </a>
                            <ul class="sub-menu">
                                <?php foreach ($categories as $cat): ?>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo base_url('shop/type/Women/category/').$cat->id; ?>">
                                        <span class="mm-text"><?php echo $cat->categoryName; ?></span>
                                    </a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="#">
                                <span class="mm-text">Kids
                                    <i class="lastudioicon-down-arrow"></i>
                                </span>
                            </a>
                            <ul class="sub-menu">
                                <?php foreach ($categories as $cat): ?>
                                <li class="menu-item-has-children">
                                    <a href="<?php echo base_url('shop/type/Kids/category/').$cat->id; ?>">
                                        <span class="mm-text"><?php echo $cat->categoryName; ?></span>
                                    </a>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                        <li>
                            <a class="<?php if($pageName=="Voucher"): echo 'active'; endif; ?>"
                                href="<?php echo base_url('voucher') ?>">Gift Voucher</a>
                        </li>
                        <li>
                            <a class="<?php if($pageName=="FAQ"): echo 'active'; endif; ?>"
                                href="<?php echo base_url('faq') ?>">FAQ</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>