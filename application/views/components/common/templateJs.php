<!-- Bootstrap JS -->
<script src="<?php echo base_url(); ?>assets/template/js/vendor/bootstrap.bundle.min.js"></script>

<!-- Jquery-ui JS -->
<script src="<?php echo base_url(); ?>assets/template/js/plugins/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template/js/plugins/jquery.ui.touch-punch.min.js"></script>
<!-- Waypoints JS -->
<script src="<?php echo base_url(); ?>assets/template/js/plugins/waypoints.min.js"></script>
<!-- Wow JS -->
<script src="<?php echo base_url(); ?>assets/template/js/plugins/wow.min.js"></script>
<!-- Instagram JS -->
<!-- <script src="<?php echo base_url(); ?>assets/template/js/plugins/jquery.instagramFeed.js"></script> -->
<!-- Scrollax JS -->
<script src="<?php echo base_url(); ?>assets/template/js/plugins/scrollax.min.js"></script>

<!-- Swiper Slider JS -->
<script src="<?php echo base_url(); ?>assets/template/js/plugins/swiper.js"></script>
<!-- Slick JS -->
<script src="<?php echo base_url(); ?>assets/template/js/plugins/slick.min.js"></script>
<!-- Nice Select JS -->
<script src="<?php echo base_url(); ?>assets/template/js/plugins/jquery.nice-select.js"></script>

<!-- Lightgallery JS -->
<script src="<?php echo base_url(); ?>assets/template/js/plugins/lightgallery.js"></script>
<!-- jquery Zoom -->
<script src="<?php echo base_url(); ?>assets/template/js/plugins/jquery.zoom.min.js"></script>

<!-- Main JS -->
<script src="<?php echo base_url(); ?>assets/template/js/main.js"></script>