<!-- Start Bradcaump area -->
<div class="ht__bradcaump__area bg-login py-6">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="bradcaump__inner bg-none">
                    <h2 class="bradcaump-title"><?php echo $pageName ?></h2>
                    <nav class="bradcaump-content">
                        <a class="breadcrumb_item" href="<?php echo base_url(); ?>">Home</a>
                        <span class="brd-separetor">/</span>
                        <span class="breadcrumb_item active"><?php echo $pageName ?></span>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Bradcaump area -->