<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('profile/common/bcprofile'); ?> 

<section class="my_account_area py-5 bg--white">
    <div class="container">
        <div class="row">
            <?php $this->load->view('profile/common/sidebar'); ?> 
             
            <div class="col-lg-9 col-12">
                <span class="profile-name"><?php echo $profile->fullName; ?></span>
                <h3 class="account__title mb-3">Welcome to dashboard</h3>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('components/common/footer'); ?>