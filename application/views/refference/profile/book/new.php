<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('profile/common/bcprofile'); ?>   

<section class="my_account_area py-5 bg--white">
    <div class="container">
        <div class="row">
            <?php $this->load->view('profile/common/sidebar'); ?> 
             
            <div class="col-lg-9 col-12">
                <div class="my__account__wrapper">
                    <form action="<?php echo base_url('book/update'); ?>" method="post"  enctype="multipart/form-data">
                        
                        <h3 class="account__title mb-3">Edit Book</h3>
                    
                        <div class="account__form row">
                            
                            <div class="input__box col-lg-12">
                                <label><b>Title</b><span>*</span></label>
                                <input type="text" name="title" id="title" value="<?php echo $book->title; ?>" autocomplete="off" required>
                                <input type="hidden" name="bookId" value="<?php echo $book->id; ?>" />
                                <input type="hidden" name="copyId" value="<?php echo $bookCopy->id; ?>" />
                            </div>
                            <div class="input__box col-lg-4">
                                <label><b>Author</b><span>*</span></label>
                                <input type="text" name="author" id="author" value="<?php echo $author->name; ?>" autocomplete="off" required>
                            </div>
                            <div class="input__box col-lg-4">
                                <label><b>Category</b><span>*</span></label>
                                <select name="category" class="select" required>
                                    <option selected><?php echo $book->category; ?></option>                                    
                                    <option disabled>------------------------</option>
                                    <?php foreach($categories as $c): ?>
                                        <option><?php echo $c->category ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="input__box col-lg-4">
                                <label><b>Condition</b><span>*</span></label>
                                <select name="condition" class="select" required>
                                    <option selected><?php echo $bookCopy->condition; ?></option>                                    
                                    <option disabled>------------------------</option>
                                    <option>Mint Condition</option>
                                    <option>Good Condition</option>
                                    <option>Average Condition</option>
                                </select>
                            </div>
                            <div class="input__box col-lg-3">
                                <label><b>Price</b><span>*</span></label>
                                <input type="number" name="price" min="0" value="<?php echo $bookCopy->price; ?>" required>
                            </div>
                            <div class="input__box col-lg-3">
                                <label><b>Charity Pledge (%)</b><span>*</span></label>
                                <input type="number" name="charity" min="0" value="<?php echo $bookCopy->charity; ?>" required>
                            </div>
                            <div class="input__box col-lg-3">
                                <label><b>Pages</b><span>*</span></label>
                                <input type="number" name="pages" min="0" value="<?php echo $bookCopy->pages; ?>" required>
                            </div>
                            <div class="input__box col-lg-3">
                                <label><b>Year</b><span>*</span></label>
                                <input type="number" name="year" min="1900" value="<?php echo $bookCopy->year; ?>" required>
                            </div>
                            
                            <div class="input__box col-lg-6">
                                <label><b>Cover</b><span>*</span></label>
                                <img src="<?php echo base_url() ?>/<?php echo $bookCopy->cover; ?>" style="height: 200px;" />
                                <label>Select different cover</label>
                                <input type="file" name="cover" id="cover" class="pt-2"/>
                            </div>

                            <div class="input__box col-lg-6">
                                <label><b>Back Cover</b><span>*</span></label>
                                <img src="<?php echo base_url() ?>/<?php echo $bookCopy->backCover; ?>" style="height: 200px;" />
                                <label>Select different back cover</label>
                                <input type="file" name="backCover" id="backCover" class="pt-2" />
                            </div>
                            <div class="input__box col-lg-12">
                                <label><b>Description about book</b></label>
                                <textarea class="text" name="description" id="description"><?php echo $bookCopy->description; ?></textarea>
                            </div>

                            <div class="input__box col-lg-12">
                                <label><b>Special note</b></label>
                                <textarea class="text" name="specialNote" id="specialNote"><?php echo $bookCopy->specialNote; ?></textarea>
                            </div>
                            <div class="form__btn col-lg-4">
                                <button type="submit" class="mt-2">Update Book</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
 
    $('#author').typeahead({
        source: function(query, result){
            $.ajax({
            url:"<?php echo base_url('ajax/author'); ?>",
            method:"POST",
            data:{query:query},
            dataType:"json",
            success:function(data)
                {
                    result($.map(data, function(item){
                    return item;
                    }));
                }
            })
        }
    });

    $('#title').typeahead({
        source: function(query, result){
            $.ajax({
            url:"<?php echo base_url('ajax/title'); ?>",
            method:"POST",
            data:{query:query},
            dataType:"json",
            success:function(data)
                {
                    result($.map(data, function(item){
                    return item;
                    }));
                }
            })
        }
    });
 
});
</script>

<?php $this->load->view('components/common/footer'); ?>