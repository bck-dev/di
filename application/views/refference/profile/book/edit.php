<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('profile/common/bcprofile'); ?>   

<section class="my_account_area py-5 bg--white">
    <div class="container">
        <div class="row">
            <?php $this->load->view('profile/common/sidebar'); ?> 
             
            <div class="col-lg-9 col-12">
                <div class="my__account__wrapper">
                    <form action="<?php echo base_url('book/'); echo $action; ?>" method="post"  enctype="multipart/form-data">
                        <div class="account__form">
                            <h3 class="account__title mb-3">Add New Book</h3>
                            <div class="input__box">
                                <label>Title<span>*</span></label>
                                <input type="text" name="title" id="title" value="<?php ?>" autocomplete="off" required>
                            </div>
                            <div class="input__box w-50 float-left pr-3">
                                <label>Author<span>*</span></label>
                                <input type="text" name="author" id="author" value="<?php ?>" autocomplete="off" required>
                            </div>
                            <div class="input__box w-50 float-left">
                                <label>Category<span>*</span></label>
                                <select name="category" class="select" required>
                                    <option selected disabled>Select Category</option>
                                    <?php foreach($categories as $c): ?>
                                        <option><?php echo $c->category ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="input__box w-50 float-left pr-3">
                                <label>Price<span>*</span></label>
                                <input type="text" name="price" value="<?php ?>" required>
                            </div>
                            <div class="input__box w-50 float-left">
                                <label>Charity Pledge (%)<span>*</span></label>
                                <input type="number" name="charity" min="0" id="charity" value="<?php ?>" required>
                            </div>
                            <div class="input__box w-50 float-left">
                                <label>Pages<span>*</span></label>
                                <input type="text" name="pages" value="<?php ?>" required>
                            </div>
                            <div class="input__box w-50 float-left pr-3">
                                <label>Year<span>*</span></label>
                                <input type="text" name="year" value="<?php ?>" required>
                            </div>
                            <div class="input__box w-50 float-left">
                                <label>Condition<span>*</span></label>
                                <select name="condition" class="select" required>
                                    <option selected disabled>Select Condition</option>
                                    <option>Mint Condition</option>
                                    <option>Good Condition</option>
                                    <option>Average Condition</option>
                                </select>
                            </div>
                            <div class="input__box w-50 float-left">
                                <label>Cover<span>*</span></label>
                                <input type="file" name="cover" id="cover" class="pt-2" required/>
                            </div>

                            <div class="input__box w-50 float-left pr-3">
                                <label>Back Cover<span>*</span></label>
                                <input type="file" name="backCover" id="backCover" class="pt-2" required/>
                            </div>
                            <div class="input__box">
                                <label>Description about book</label>
                                <textarea class="text" name="description" id="description"><?php ?></textarea>
                            </div>

                            <div class="input__box">
                                <label>Special note</label>
                                <textarea class="text" name="specialNote" id="specialNote"><?php ?></textarea>
                            </div>
                            <div class="form__btn">
                                <button type="submit" class="mt-2">Add Book</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
 
    $('#author').typeahead({
        source: function(query, result){
            $.ajax({
            url:"<?php echo base_url('ajax/author'); ?>",
            method:"POST",
            data:{query:query},
            dataType:"json",
            success:function(data)
                {
                    result($.map(data, function(item){
                    return item;
                    }));
                }
            })
        }
    });

    $('#title').typeahead({
        source: function(query, result){
            $.ajax({
            url:"<?php echo base_url('ajax/title'); ?>",
            method:"POST",
            data:{query:query},
            dataType:"json",
            success:function(data)
                {
                    result($.map(data, function(item){
                    return item;
                    }));
                }
            })
        }
    });
 
});
</script>

<?php $this->load->view('components/common/footer'); ?>