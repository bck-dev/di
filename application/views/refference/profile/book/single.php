<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('profile/common/bcprofile'); ?>   

<section class="my_account_area py-5 bg--white">
    <div class="container">
        <div class="row">
            <?php $this->load->view('profile/common/sidebar'); ?> 
             
            <div class="col-lg-9 col-12">
                <div class="my__account__wrapper">
                    <div class="account__form">
                        <h3 class="account__title mb-3"><?php echo $book->title ?></h3>
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="<?php echo base_url(); ?>/<?php echo $bookCopy->cover; ?>" class="w-100"/>
                            </div>
                            <div class="col-lg-8">
                                <label><b>Author:</b> <?php echo $author->name; ?></label><br />
                                <label><b>Category:</b> <?php echo $book->category; ?></label><br />
                                <label><b>Price:</b> <?php echo $bookCopy->price; ?></label><br />
                                <label><b>Pages:</b> <?php echo $bookCopy->pages; ?></label><br />
                                <label><b>Year:</b> <?php echo $bookCopy->year; ?></label><br />
                                <label><b>Condition:</b> <?php echo $bookCopy->condition; ?></label><br />
                                <label><b>Description:</b> <?php echo $bookCopy->description; ?></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('components/common/footer'); ?>