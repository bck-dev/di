<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<!-- Start Bradcaump area -->
<div class="ht__bradcaump__area bg-login py-6">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="bradcaump__inner bg-none">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Bradcaump area -->
        
<!-- Start main Content -->
<div class="maincontent bg--white pt--80 pb--55">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-12">
                <div class="wn__single__product">
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <div class="wn__fotorama__wrapper w-100">
                                <div class="fotorama wn__fotorama__action w-100" data-nav="thumbs">
                                    <a href="1.jpg"><img src="<?php echo base_url(); ?>/<?php echo $bookCopy->cover; ?>" alt="" class="w-100"></a>
                                    <a href="2.jpg"><img src="<?php echo base_url(); ?>/<?php echo $bookCopy->backCover; ?>" alt="" class="w-100"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="product__info__main">
                                <h1><?php echo $book->title ?></h1>
                                <div class="price-box d-flex">
                                    <span class="w-50">Rs. <?php echo $bookCopy->price; ?></span>
									<span class="charity-ruppee w-50">Charity Pledge: Rs.<?php echo number_format((($bookCopy->price/100)*$bookCopy->charity), 2); ?></span>
                                </div>
                                <div class="product__overview">
                                    <div class="row">
                                        <div class="col-6"><b>Pages: </b><?php echo $bookCopy->pages; ?></div>
                                        <div class="col-6"><b>Printed Year: </b><?php echo $bookCopy->year; ?></div>
                                        <div class="col-6"><b>Condition: </b><?php echo $bookCopy->condition; ?></div>
                                        <div class="col-6">
                                            <b>Genre: </b>
                                            <a href="<?php echo base_url('shop/genre/'); ?><?php echo $book->category; ?>">
                                                <?php echo $book->category; ?>
                                            </a>
                                        </div>
                                    </div>
                                    <hr class="my-3"/>
                                    <?php if($bookCopy->description): ?><p><b>Description:</b><br /><?php echo $bookCopy->description; ?></p><?php endif; ?>
                                    <?php if($bookCopy->specialNote): ?><p><b>Special Note:</b><br /><?php echo $bookCopy->specialNote; ?></p><?php endif; ?>
                                </div>

                                <div class="owner-deatils mt-2">
                                    <h4>Contact Information</h4>
                                    <hr />
                                    <ul class="mt-3">
                                        <li><b><?php echo $profile->fullName; ?></b></li>
                                        <li><?php echo $profile->district; ?></li>
                                        <li class=" mt-2">
                                            <div id="hidden-num">
                                                <span class="telephone">+94xxxxxxxxx</span>
                                                <p class="instruction">click to see the number</p>
                                            </div>

                                            <a href="tel:+94<?php echo $profile->telephone; ?>" id="visible-num" class="telephone d-none">
                                                +94<?php echo $profile->telephone; ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="mt-5 sharethis-inline-share-buttons"></div>
                                <!-- <div class="">
                                    <a href="whatsapp://send?text=I found this interesting book. Hope You like it. <?php echo base_url('shop/single/').$this->uri->segment(3); ?>" class="whatsapp"><i class="fab fa-whatsapp fa-lg"></i> Share with friend</a>
                                </div> -->
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-12 md-mt-40 sm-mt-40">
                <div class="shop__sidebar">
                    <aside class="wedget__categories poroduct--cat">
                        <h3 class="wedget__title">Genres</h3>
                        <ul>
                            <?php foreach ($allCategoriesWithBooks as $cat): ?>
                                <li><a href="<?php echo base_url('shop/genre/'); ?><?php echo $cat->category; ?>"><?php echo $cat->category; ?> <span>(<?php echo $cat->count; ?>)</span></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </aside>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End main Content -->

<script>
    $( document ).ready(function() {
		$('#hidden-num').click(function(){
            $('#hidden-num').addClass('d-none');
            $('#visible-num').removeClass('d-none');
            $('#visible-num').addClass('d-block');
        });
    });
</script>

<?php $this->load->view('components/common/footer'); ?>