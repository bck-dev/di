<div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
    <div class="shop__sidebar">
        <aside class="wedget__categories poroduct--cat">
            <h3 class="wedget__title">GENRES</h3>
            <ul>
                <li><a href="<?php echo base_url('shop/all'); ?><?php echo $cat->category; ?>">All Books</a></li>
                <?php foreach ($allCategoriesWithBooks as $cat): ?>
                    <li><a href="<?php echo base_url('shop/genre/'); ?><?php echo $cat->category; ?>"><?php echo $cat->category; ?> <span>(<?php echo $cat->count; ?>)</span></a></li>
                <?php endforeach; ?>
            </ul>
        </aside>
        <aside class="wedget__categories pro--range">
            <h3 class="wedget__title">Filter by price</h3>
            <div class="content-shopby">
                <div class="price_filter s-filter clear">
                    <div id="slider-range"></div>
                    <div class="slider__range--output">
                        <div class="price__output--wrap">
                            <div class="price--output">
                                <span>Price :</span><input type="text" id="amount" readonly="">
                            </div>
                            <div class="price--filter">
                                <input type="hidden" id="category" value="<?php echo $this->uri->segment(3); ?>">
                                <!-- <div id="filter">Filter</div> -->
                            </div>
                            
                            <div id="filter" class="fliterBtn mt-3">Filter</div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>

        <aside class="wedget__categories pro--range">
            <h3 class="wedget__title">Filter by District</h3>
            <div class="content-shopby">
                <div class="price_filter s-filter clear">
                    <div id="slider-range"></div>
                    <div class="slider__range--output">
                        <div class="price__output--wrap">
                            <select name="district" class="select" id="district" required>
                                <option selected disabled>All</option>
                                <option>Ampara</option>
                                <option>Anuradhapura</option>
                                <option>Badulla</option>
                                <option>Batticaloa</option>
                                <option>Colombo</option>
                                <option>Galle</option>
                                <option>Gampaha</option>
                                <option>Hambantota</option>
                                <option>Jaffna</option>
                                <option>Kalutara</option>
                                <option>Kandy</option>
                                <option>Kegalle</option>
                                <option>Kilinochchi</option>
                                <option>Kurunegala</option>
                                <option>Mannar</option>
                                <option>Matale</option>
                                <option>Matara</option>
                                <option>Moneragala</option>
                                <option>Mullaitivu</option>
                                <option>Nuwara Eliya</option>
                                <option>Polonnaruwa</option>
                                <option>Puttalam</option>
                                <option>Ratnapura</option>
                                <option>Trincomalee</option>
                                <option>Vavuniya</option>
                            </select>
                            <div id="filter" class="fliterBtn mt-3">Filter</div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>

        <aside class="wedget__categories pro--range">
            <h3 class="wedget__title">Filter by Condition</h3>
            <div class="content-shopby">
                <div class="price_filter s-filter clear">
                    <div id="slider-range"></div>
                    <div class="slider__range--output">
                        <div class="price__output--wrap">
                            <select name="condition" class="select" id="condition" required>
                                <option selected disabled>Select Condition</option>
                                <option>Mint Condition</option>
                                <option>Good Condition</option>
                                <option>Average Condition</option>
                            </select>
                            <div id="filter" class="fliterBtn mt-3">Filter</div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
    </div>
</div>

<script>
    $( document ).ready(function() {
		$('.fliterBtn').click(function(){
            var amount = $('#amount').val();
			var category = $('#category').val();         
			var condition = $('#condition').val();      
			var district = $('#district').val();
            var amounts = amount.split(" - ");
            var min = amounts[0].replace("Rs.", "");
            var max = amounts[1].replace("Rs.", "");
            var base_url = "<?php echo base_url(); ?>";

			$.ajax({
				type:'GET',
				url: '<?php echo base_url('filterBooks') ?>', 
				data: {min: min, max: max, category: category, condition: condition, district: district},
				dataType: 'json',
				success: function(results){ 
                    
                    var books =""; 
                    jQuery.each(results['books'], function( key, val ) {
                        books = books + '<div class="col-lg-3 product product__style--3">';
                        books = books + '<div class="product__thumb">';
                        books = books + '<a href="' + base_url + '/shop/single/' + val['id'] + '">';
                        books = books + '<div style="background-image: url(' + base_url + '/' + val['cover'] + ');" class="book-cover-shop"></div>';
                        books = books + '</a>';
                        books = books + '</div>';
                        books = books + '<div class="product__content content--center">';
                        books = books + '<h4>';
                        books = books + '<a href="' + base_url + '/shop/single/' + val['id'] + '">';
                        books = books + val['title'];
                        books = books + '</a>';
                        books = books + '</h4>';
                        books = books + '<ul class="prize d-flex">';
                        books = books + '<li>Rs. ' + val['price'] + '</li>';
                        books = books + '</ul>';
                        books = books + '<div class="action">';
                        books = books + '<div class="actions_inner">';
                        books = books + '<ul class="add_to_links">';
                        books = books + '<li>';
                        books = books + '<a class="cart" href="' + base_url + '/shop/single/' + val['id'] + '">';
                        books = books + '<i class="bi bi-search"></i>';
                        books = books + '</a>';
                        books = books + '</li>';
                        books = books + '</ul>';
                        books = books + '</div>';
                        books = books + '</div>';
                        books = books + '</div>';
                        books = books + '</div>';
                    });
                    $('#books').html(books);
                    
                    var pagination =""; 
                    jQuery.each(results.pagination.buttons, function( key, val ) {

                        if(val['status']=="current"){
                            pagination = pagination + '<li class="active">';
                            }
                        else{
                            pagination = pagination + '<li>';
                        }
                        pagination = pagination + '<a href="#" data-id="' + val['id'] + '" onclick="getPaginationData(this); updatePagination(this);">';
                        
                        pagination = pagination + val['value']; 
                        pagination = pagination + '</a>';
                        pagination = pagination + '</li>';
                    });
                    $('#pagination').html(pagination);
                    
                    if(results.pagination.count<12){
                        var limit = results.pagination.count;
                    }else{
                        var limit = 12;
                    }
                    $('#currentTab').html('1-'+limit);
                    $('#total').html(results.pagination.count);
				},
			
				error:function(){
					console.log('error');
                }
                
            });
		});
	});
</script>