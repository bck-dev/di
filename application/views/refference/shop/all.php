<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<!-- Start Bradcaump area -->
<div class="ht__bradcaump__area bg-login py-6">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="bradcaump__inner bg-none">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Bradcaump area -->

<!-- Start Shop Page -->
<div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
    <div class="container">
        <div class="row">
            <!-- sidebar -->
            <?php $this->load->view('shop/sidebar'); ?>

            <div class="col-lg-9 col-12 order-1 order-lg-2">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="shop__list__wrapper d-flex flex-wrap flex-md-nowrap justify-content-between">
                            <p>Showing 
                                <span id="currentTab">1-<?php echo $count; ?></span> 
                                Out of <span id="total"><?php echo $totalCount; ?></span> Books 
                                <?php if($category): echo "for "; echo $category; endif; ?>
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="row" id="books">
                    <?php foreach($books as $book): ?>
                        <!-- Start Single Product -->
                        <div class="col-lg-3 product product__style--3">
                            <div class="product__thumb">
                                <a href="<?php echo base_url('shop/single/'); echo $book->id; ?>" target="_blank">
                                    <div style="background-image: url(<?php echo base_url(); ?>/<?php echo $book->cover; ?>);" class="book-cover-shop"></div>
                                </a>
                            </div>
                            <div class="product__content content--center">
                                <h4>
                                    <a href="<?php echo base_url('shop/single/'); echo $book->id; ?>" target="_blank">
                                        <?php echo $book->title; ?>
                                    </a>
                                </h4>
                                <ul class="prize d-flex">
                                    <li>Rs. <?php echo number_format($book->price, 2); ?></li>
                                </ul>
                                <!-- <div class="action">
                                    <div class="actions_inner">
                                        <ul class="add_to_links">
                                            <li>
                                                <a class="cart" href="<?php echo base_url('shop/single/'); echo $book->id; ?>" target="_blank">
                                                    <i class="bi bi-search"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <!-- Start Single Product -->
                    <?php endforeach; ?>
                </div>

                <?php $this->load->view('components/common/pagination'); ?>
            </div>
        </div>
    </div>
</div>
<!-- End Shop Page -->

<?php $this->load->view('components/common/footer'); ?>