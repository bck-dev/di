<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
		<!-- Start Bradcaump area -->
		<div class="ht__bradcaump__area bg-login">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="bradcaump__inner text-center">
							<h2 class="bradcaump-title">Reset Password</h2>
							<nav class="bradcaump-content">
								<a class="breadcrumb_item" href="<?php echo base_url(); ?>">Home</a>
								<span class="brd-separetor">/</span>
								<span class="breadcrumb_item active">Reset Password</span>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Bradcaump area -->
		<!-- Start My Account Area -->
		<section class="my_account_area pt--80 pb--55 bg--white">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-12 m-auto">
						<div class="my__account__wrapper">
							<h3 class="account__title text-center">Reset Password</h3>
							<form id="resetForm" action="#">
								<div class="account__form">
									<div class="input__box">
										<label>Password<span>*</span></label>
										<input type="password" name="password" id="password" required>
										<div id="passwordError"></div>
									</div>
									<div class="input__box">
										<label>Re-type Password<span>*</span></label>
										<input type="password" name="rePassowrd" id="rePassword" required>
										<div id="rePasswordError"></div>
									</div>
									<div class="form__btn">
										<input type="hidden" name="id" id="id" value="<?php echo $user->id ?>" required>
										<button type="submit" class="mt-2">Reset</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>

<script>
	$( document ).ready(function() {
		
		$('#password').keyup(function(){
			var password = $('#password').val();
			if(password.length>=6){
				var msg = '<div class="success">Password Ok!</div>';
				var sMsg = '<div class="error">Password not matching!</div>';
			}
			else{
				var msg = '<div class="error">Password must be atleast 6 letters</div>';
				var sMsg = '<div class="error">Password not matching!</div>';
			}

			$('#passwordError').html(msg);
			$('#rePasswordError').html(sMsg);
			
		});

		$('#rePassword').keyup(function(){
			var password = $('#password').val();
			var rePassword = $('#rePassword').val();
			if(password==rePassword){
				var msg = '<div class="success">Password matching</div>';
			}
			else{
				var msg = '<div class="error">Password not matching!</div>';
			}

			$('#rePasswordError').html(msg);
			
		});

		$( "#resetForm" ).submit(function( event ) {
			var pErr = $('#passwordError').children().attr('class');
			var rPErr = $('#rePasswordError').children().attr('class');

			if(pErr != "error" && rPErr != "error" ){
				var id = $('#id').val();
				var password = $('#password').val();
				$.ajax({
				type:'POST',
				url: '<?php echo base_url('updateNewPassword') ?>', 
				data: {id: id, password: password},
				dataType: 'json',
				success: function(results){ 
					window.location.href = '<?php echo base_url('resetSuccess')?>';
				},
			
				error:function(){
					console.log('error');
				}
			});
			}
			else{
				
			}
			event.preventDefault();
		});
	});
</script>
        
<?php $this->load->view('components/common/footer'); ?>