<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
	<!-- Start Bradcaump area -->
	<div class="ht__bradcaump__area bg-login">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="bradcaump__inner text-center">
						<h2 class="bradcaump-title">Forgot Password</h2>
						<nav class="bradcaump-content">
							<a class="breadcrumb_item" href="<?php echo base_url(); ?>">Home</a>
							<span class="brd-separetor">/</span>
							<span class="breadcrumb_item active">Forgot Password</span>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Bradcaump area -->
	<!-- Start My Account Area -->
	<section class="my_account_area pt--80 pb--55 bg--white">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-12 m-auto">
					<div class="my__account__wrapper">
						<h3 class="account__title text-center">Forgot Password</h3>
						<form action="<?php echo base_url('sendForgotMail') ?>" method="post">
							<div class="account__form">
								<div class="input__box">
									<label>Email<span>*</span></label>
									<input type="email"	name="email" required>
								</div>
								<div class="form__btn">
									<button type="submit" id="sub" class="mt-2">Submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End My Account Area -->
	
<script>
	$( document ).ready(function() {
		$('#sub').click(function(){
			$('#sub').addClass('d-none');
		});
	});
</script>

<?php $this->load->view('components/common/footer'); ?>