<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
		<!-- Start Bradcaump area -->
		<div class="ht__bradcaump__area bg-login">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="bradcaump__inner text-center">
							<h2 class="bradcaump-title">Welcome</h2>
							<nav class="bradcaump-content">
								<a class="breadcrumb_item" href="<?php echo base_url(); ?>">Home</a>
								<span class="brd-separetor">/</span>
								<span class="breadcrumb_item active">Welcome</span>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Bradcaump area -->
		<!-- Start My Account Area -->
		<section class="my_account_area pt--80 pb--55 bg--white">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-12 m-auto">
						<div class="my__account__wrapper text-center">
							<h3 class="account__title text-center">Welcome</h3>
							<p>Dear User<br />Thank you for registering with us. Please click the link below to login<br /> 
								<br />
								<a href="<?php echo base_url('login'); ?>" class="white-link" >Login!</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
        
<?php $this->load->view('components/common/footer'); ?>