<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
		<!-- Start Bradcaump area -->
		<div class="ht__bradcaump__area bg-login">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="bradcaump__inner text-center">
							<h2 class="bradcaump-title">Register</h2>
							<nav class="bradcaump-content">
								<a class="breadcrumb_item" href="<?php echo base_url(); ?>">Home</a>
								<span class="brd-separetor">/</span>
								<span class="breadcrumb_item active">Register</span>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Bradcaump area -->
		<!-- Start My Account Area -->
		<section class="my_account_area pt--80 pb--55 bg--white">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-12 m-auto">
						<div class="my__account__wrapper">
							<h3 class="account__title text-center">Register</h3>
							<form id="regForm" action="#">
								<div class="account__form">
									<div class="input__box">
										<label>Email<span>*</span></label>
										<input type="email" name="email" id="email" required>
										<div id="emailError"></div>
									</div>
									<div class="input__box">
										<label>Contact Number<span>*</span></label>
										<input class="contact-no-input" type="text" name="contact" id="contact" required>
									</div>
									<div class="input__box">
										<label>District<span>*</span></label>
										<select name="district" id="district" class="select" required>
											<option selected disabled>Select District</option>
											<option>Ampara</option>
											<option>Anuradhapura</option>
											<option>Badulla</option>
											<option>Batticaloa</option>
											<option>Colombo</option>
											<option>Galle</option>
											<option>Gampaha</option>
											<option>Hambantota</option>
											<option>Jaffna</option>
											<option>Kalutara</option>
											<option>Kandy</option>
											<option>Kegalle</option>
											<option>Kilinochchi</option>
											<option>Kurunegala</option>
											<option>Mannar</option>
											<option>Matale</option>
											<option>Matara</option>
											<option>Moneragala</option>
											<option>Mullaitivu</option>
											<option>Nuwara Eliya</option>
											<option>Polonnaruwa</option>
											<option>Puttalam</option>
											<option>Ratnapura</option>
											<option>Trincomalee</option>
											<option>Vavuniya</option>
										</select>
									</div>
									<div class="input__box">
										<label>Password<span>*</span></label>
										<input type="password" name="password" id="password" required>
										<div id="passwordError"></div>
									</div>
									<div class="input__box">
										<label>Re-type Password<span>*</span></label>
										<input type="password" name="rePassowrd" id="rePassword" required>
										<div id="rePasswordError"></div>
									</div>
									<div class="form__btn">
										<button type="submit" class="mt-2">Register</button>
									</div>
									<a class="forget_pass" href="<?php echo base_url('login'); ?>">Already registered? Login Here!</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>

<script>
	$( document ).ready(function() {
		$('#email').change(function(){
			var email = $('#email').val();

			$.ajax({
				type:'POST',
				url: '<?php echo base_url('ajaxEmail') ?>', 
				data: {email: email},
				dataType: 'json',
				success: function(results){ 
					var msg = '<div class="' + results['cls'] + '">' + results['msg'] + '</div>';
					$('#emailError').html(msg);
				},
			
				error:function(){
					console.log('error');
				}
			});
		});

		$('#password').keyup(function(){
			var password = $('#password').val();
			if(password.length>=6){
				var msg = '<div class="success">Password Ok!</div>';
				var sMsg = '<div class="error">Password not matching!</div>';
			}
			else{
				var msg = '<div class="error">Password must be atleast 6 letters</div>';
				var sMsg = '<div class="error">Password not matching!</div>';
			}

			$('#passwordError').html(msg);
			$('#rePasswordError').html(sMsg);
			
		});

		$('#rePassword').keyup(function(){
			var password = $('#password').val();
			var rePassword = $('#rePassword').val();
			if(password==rePassword){
				var msg = '<div class="success">Password matching</div>';
			}
			else{
				var msg = '<div class="error">Password not matching!</div>';
			}

			$('#rePasswordError').html(msg);
			
		});

		$( "#regForm" ).submit(function( event ) {
			var eErr = $('#emailError').children().attr('class');
			var pErr = $('#passwordError').children().attr('class');
			var rPErr = $('#rePasswordError').children().attr('class');

			if(eErr != "error" && pErr != "error" && rPErr != "error" ){
				var contact = $('#contact').val();
				var district = $('#district').val();
				var email = $('#email').val();
				var password = $('#password').val();
				$.ajax({
				type:'POST',
				url: '<?php echo base_url('registerUser') ?>', 
				data: {contact: contact, district: district, email: email, password: password},
				dataType: 'json',
				success: function(results){ 
					window.location.href = '<?php echo base_url('regMsg')?>';
				},
			
				error:function(){
					console.log('error');
				}
			});
			}
			else{
				
			}
			event.preventDefault();
		});
	});
</script>
        
<?php $this->load->view('components/common/footer'); ?>