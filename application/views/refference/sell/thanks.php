<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<!-- Start Bradcaump area -->
<div class="ht__bradcaump__area bg-login py-6">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="bradcaump__inner bg-none">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Bradcaump area -->

<section class="my_account_area py-5 bg--white">
    <div class="container">
        <div class="row">             
            <div class="col-lg-6 col-12 m-auto">
                <div class="my__account__wrapper">
                    <div class="account__form">
                        <h3 class="account__title mb-3">Thank you</h3>
                        <p>Your Book is listed Successfully.</p>
                        <a href="<?php echo base_url('sell') ?>">Add More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('components/common/footer'); ?>