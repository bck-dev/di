<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<!-- Start Bradcaump area -->
<div class="ht__bradcaump__area bg-login py-6">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="bradcaump__inner bg-none">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Bradcaump area -->

<section class="my_account_area py-5 bg--white">
    <div class="container">
        <div class="row">             
            <div class="col-lg-6 col-12 m-auto">
                <div class="my__account__wrapper">
                    <div class="account__form">
                        <h3 class="account__title mb-3">Thank you</h3>
                        <p class="py-2">Your book has been listed successfully.</p>
                        
                        <p class="py-2">A book with more detail is a book that more people might be interested in. Click below to add more details</p>
                        
                        <div class="form__btn">
                            <button id="description-reveal">Add More Detail</button>
                        </div>

                        <div id="book-description-box" class="d-none mt-3">
                            <form id="book-description-form" enctype="multipart/form-data">
                                <input type="hidden" id="copyId" value="<?php echo $copyId; ?>"/>
                                <div class="input__box">
                                    <p>Description about book</p>
                                    <textarea class="text" name="description" id="description"><?php ?></textarea>
                                </div>

                                <div class="input__box">
                                    <p>Special note</p>
                                    <textarea class="text" name="specialNote" id="specialNote"><?php ?></textarea>
                                </div>
                                
                                <div class="form__btn">
                                    <button id="book-description-submit" type="submit" class="mt-2">Submit</button>
                                </div>
                            </form>
                        </div>
                        <br />
                        <a href="<?php echo base_url('sell') ?>" class="default-btn">Add Another Book</a>
                        <a href="<?php echo base_url('shop/single/').$copyId; ?>" class="default-btn">View Listed Book</a>                        
                        <a href="<?php echo base_url('book/edit/').$copyId; ?>" class="default-btn">Edit Listed Book</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){
    $('#description-reveal').click(function(){
        $('#book-description-box').removeClass("d-none");
        $('#book-description-box').addClass("d-block");
        $('#description-reveal').addClass("d-none");
    });

    $('#book-description-form').submit(function(event){
        event.preventDefault();

        var copyId = $('#copyId').val();
        var description = $('#description').val();
        var specialNote = $('#specialNote').val();


        $('#book-description-submit').addClass("d-none");
        // alert(1);
        $.ajax({
            type:'POST',
            url: '<?php echo base_url('sell/bookDescription') ?>', 
            data: {copyId: copyId, description: description, specialNote: specialNote},
            dataType: 'json',
            success: function(results){               
                $('#book-description-box').html("<p>Additional details has been added succesfully!</p>");
            },
        
            error:function(){
                console.log('error');
            }
        });

    });

     
});
</script>
<?php $this->load->view('components/common/footer'); ?>