<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<!-- Start Bradcaump area -->
<div class="ht__bradcaump__area bg-login py-6">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="bradcaump__inner bg-none">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Bradcaump area -->

<section class="my_account_area py-5 bg--white">
    <div class="container">
        <div class="row">             
            <div class="col-lg-6 col-12 m-auto">
                <div class="my__account__wrapper">
                    <div class="account__form">
                        <h3 class="account__title mb-3">Sell Your Book</h3>
                        <input type="hidden" id="bookId" />
                        <input type="hidden" id="copyId" />                        
                        <input type="hidden" id="userId" value="<?php echo $this->session->userdata('userId'); ?>"/>
                        <div id="title-box">
                            <form id="title-form">
                                <div class="input__box">
                                    <label>Title<span>*</span></label>
                                    <input type="text" name="title" id="title" value="<?php ?>" autocomplete="off" required>
                                </div>
                                
                                <div class="form__btn">
                                    <button id="title-submit" type="submit" class="mt-2">Next</button>
                                </div>
                            </form>
                        </div>

                        <div id="book-detail-box" class="d-none">
                            <form id="book-detail-form" enctype="multipart/form-data">
                                <div class="input__box w-50 float-left pr-3">
                                    <label id="author-label">Author<span>*</span></label>
                                    <input type="text" name="author" id="author" value="<?php ?>" autocomplete="off" required>
                                </div>
                                
                                <div class="input__box w-50 float-left">
                                    <label id="category-label">Genre<span>*</span></label>
                                    <select name="category" id="category" class="select" required>
                                        <option selected disabled>Select Genre</option>
                                        <?php foreach($categories as $c): ?>
                                            <option><?php echo $c->category ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <input type="hidden" id="category-hidden" name="category-hidden"/>
                                </div>
                                
                                <div class="input__box w-50 float-left pr-3">
                                    <label>Price<span>*</span></label>
                                    <input type="number" name="price" min="0" id="price" value="<?php ?>" required>
                                </div>

                                <div class="input__box w-50 float-left">
                                    <label>Charity Pledge (%)<span>*</span></label>
                                    <input type="number" name="charity" min="0" id="charity" value="<?php ?>" required>
                                </div>

                                <div class="input__box w-50 float-left pr-3">
                                    <label>Pages<span>*</span></label>
                                    <input type="number" name="pages" min="0" id="pages" value="<?php ?>" required>
                                </div>
                                <div class="input__box w-50 float-left">
                                    <label>Year<span>*</span></label>
                                    <input type="number" name="year" min="1900" id="year" value="<?php ?>" required>
                                </div>
                                <div class="input__box w-50 float-left pr-3">
                                    <label>Condition<span>*</span></label>
                                    <select name="condition" id="condition" class="select" required>
                                        <option selected disabled>Select Condition</option>
                                        <option>Mint Condition</option>
                                        <option>Good Condition</option>
                                        <option>Average Condition</option>
                                    </select>
                                </div>

                                <div class="input__box w-50 float-left">
                                    <label>Cover<span>*</span></label>
                                    <input type="file" name="cover" id="cover" class="pt-2" required/>
                                </div>

                                <div class="input__box w-50 float-left pr-3">
                                    <label>Back Cover<span>*</span></label>
                                    <input type="file" name="backCover" id="backCover" class="pt-2" required/>
                                </div>

                                <div class="form__btn">
                                    <button id="book-detail-submit" type="submit" class="mt-4">Next</button>
                                </div>
                            </form>
                        </div>

                        <div id="book-description-box" class="d-none">
                            <form id="book-description-form" enctype="multipart/form-data">
                                <div class="input__box">
                                    <label>Description about book</label>
                                    <textarea class="text" name="description" id="description"><?php ?></textarea>
                                </div>

                                <div class="input__box">
                                    <label>Special note</label>
                                    <textarea class="text" name="specialNote" id="specialNote"><?php ?></textarea>
                                </div>
                                
                                <div class="form__btn">
                                    <button id="book-description-submit" type="submit" class="mt-2">Submit</button>
                                </div>
                            </form>
                        </div>

                        <div id="user-box" class="d-none">
                            <form id="user-form">
                                <div class="input__box">
                                    <label>Email<span>*</span></label>
                                    <input type="email" name="email" id="email" required>
                                    <div id="emailError"></div>
                                </div>
                                <div class="input__box">
                                    <label>Contact Number<span>*</span></label>
                                    <input class="contact-no-input" type="text" name="contact" id="contact" required>
                                </div>
                                <div class="input__box">
                                    <label>District<span>*</span></label>
                                    <select name="district" id="district" class="select" required>
                                        <option selected disabled>Select District</option>
                                        <option>Ampara</option>
                                        <option>Anuradhapura</option>
                                        <option>Badulla</option>
                                        <option>Batticaloa</option>
                                        <option>Colombo</option>
                                        <option>Galle</option>
                                        <option>Gampaha</option>
                                        <option>Hambantota</option>
                                        <option>Jaffna</option>
                                        <option>Kalutara</option>
                                        <option>Kandy</option>
                                        <option>Kegalle</option>
                                        <option>Kilinochchi</option>
                                        <option>Kurunegala</option>
                                        <option>Mannar</option>
                                        <option>Matale</option>
                                        <option>Matara</option>
                                        <option>Moneragala</option>
                                        <option>Mullaitivu</option>
                                        <option>Nuwara Eliya</option>
                                        <option>Polonnaruwa</option>
                                        <option>Puttalam</option>
                                        <option>Ratnapura</option>
                                        <option>Trincomalee</option>
                                        <option>Vavuniya</option>
                                    </select>
                                </div>
                                <div class="input__box">
                                    <label>Password<span>*</span></label>
                                    <input type="password" name="password" id="password" required>
                                    <div id="passwordError"></div>
                                </div>
                                <div class="input__box">
                                    <label>Re-type Password<span>*</span></label>
                                    <input type="password" name="rePassowrd" id="rePassword" required>
                                    <div id="rePasswordError"></div>
                                </div>
                                <div class="form__btn">
                                    <button type="submit" class="mt-2">Complete</button>
                                </div>
                                <p class="forget_pass" id="login">Already registered? Login Here!</p>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function(){

    $('#title').typeahead({
        source: function(query, result){
            $.ajax({
            url:"<?php echo base_url('ajax/title'); ?>",
            method:"POST",
            data:{query:query},
            dataType:"json",
            success:function(data)
                {
                    result($.map(data, function(item){
                    return item;
                    }));
                }
            })
        }
    });

    $('#author').typeahead({
        source: function(query, result){
            $.ajax({
            url:"<?php echo base_url('ajax/author'); ?>",
            method:"POST",
            data:{query:query},
            dataType:"json",
            success:function(data)
                {
                    result($.map(data, function(item){
                    return item;
                    }));
                }
            })
        }
    });

    $('#category').change(function(){
        $('#category-hidden').val($('#category').val());
    });

    $('#title-form').submit(function(e){
        e.preventDefault();
        var title = $('#title').val();
        $('#title-submit').addClass("d-none");

        $.ajax({
            type:'GET',
            url: '<?php echo base_url('sell/title') ?>', 
            data: {title: title},
            dataType: 'json',
            success: function(results){ 
                $('#bookId').val(results.bookId);
                $('#title-box').addClass("d-none");
                $('#book-detail-box').removeClass("d-none");                
                $('#book-detail-box').addClass("d-block");
                
                if(results.new==0){
                    $('#category').attr('disabled', true);
                    $('#author').attr('readonly', true);
                    $('#author').addClass('disabled-input');
                    $('#author').val(results.author);
                    $('#category').val(results.category);
                    $('#category-hidden').val(results.category);
                }
            },
        
            error:function(){
                console.log('error');
            }
        });
    });

    $('#book-detail-form').submit(function(event){
        event.preventDefault();

        var bookId = $('#bookId').val();  
        var userId = $('#userId').val();  

        var data = new FormData(this);
        console.log(data);
        console.log(bookId);
        data.append('bookId', bookId);

        $('#book-detail-submit').addClass("d-none");
        // alert(1);
        $.ajax({
            type:'POST',
            url: '<?php echo base_url('sell/bookDetail') ?>', 
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json',
            success: function(results){ 
                $('#copyId').val(results.copyId);
                console.log(results);
                if(userId>0){
                    var reurl= '<?php echo base_url('sell/finish/')?>'+results.copyId;
                    window.location.href = reurl;
                }
                else{
                    $('#book-detail-box').removeClass("d-block");  
                    $('#book-detail-box').addClass("d-none");
                    $('#user-box').removeClass("d-none");                
                    $('#user-box').addClass("d-block");
                }
            },
        
            error:function(){
                console.log('error');
            }
        });

    });

    $('#login').click(function(){
        var copyId = $('#copyId').val();  
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url('sell/login') ?>', 
            data: {copyId, copyId},
            dataType: 'json',
            success: function(results){ 
                window.location.href = '<?php echo base_url('login')?>';
            },
        
            error:function(){
                console.log('error');
            }
        });

    });

    $('#email').change(function(){
        var email = $('#email').val();

        $.ajax({
            type:'POST',
            url: '<?php echo base_url('ajaxEmail') ?>', 
            data: {email: email},
            dataType: 'json',
            success: function(results){ 
                var msg = '<div class="' + results['cls'] + '">' + results['msg'] + '</div>';
                $('#emailError').html(msg);
            },
        
            error:function(){
                console.log('error');
            }
        });
    });

    $('#password').keyup(function(){
        var password = $('#password').val();
        if(password.length>=6){
            var msg = '<div class="success">Password Ok!</div>';
            var sMsg = '<div class="error">Password not matching!</div>';
        }
        else{
            var msg = '<div class="error">Password must be atleast 6 letters</div>';
            var sMsg = '<div class="error">Password not matching!</div>';
        }

        $('#passwordError').html(msg);
        $('#rePasswordError').html(sMsg);
        
    });

    $('#rePassword').keyup(function(){
        var password = $('#password').val();
        var rePassword = $('#rePassword').val();
        if(password==rePassword){
            var msg = '<div class="success">Password matching</div>';
        }
        else{
            var msg = '<div class="error">Password not matching!</div>';
        }

        $('#rePasswordError').html(msg);
        
    });

    $( "#user-form" ).submit(function( event ) {
        var eErr = $('#emailError').children().attr('class');
        var pErr = $('#passwordError').children().attr('class');
        var rPErr = $('#rePasswordError').children().attr('class');
        
        if(eErr != "error" && pErr != "error" && rPErr != "error" ){
            var contact = $('#contact').val();
            var district = $('#district').val();
            var email = $('#email').val();
            var password = $('#password').val();
            var copyId = $('#copyId').val();
            $.ajax({
            type:'POST',
            url: '<?php echo base_url('sell/user') ?>', 
            data: {contact: contact, district: district, email: email, password: password, copyId: copyId},
            dataType: 'json',
            success: function(results){ 
                var reurl= '<?php echo base_url('sell/finish/')?>'+copyId;
                window.location.href = reurl;
            },
        
            error:function(){
                console.log('error');
            }
        });
        }
        else{
            
        }
        event.preventDefault();
    });
 
});
</script>

<?php $this->load->view('components/common/footer'); ?>