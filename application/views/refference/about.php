<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
	<!-- Start Bradcaump area -->
	<div class="ht__bradcaump__area bg-login">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="bradcaump__inner text-center">
						<h2 class="bradcaump-title">About Us</h2>
						<nav class="bradcaump-content">
							<a class="breadcrumb_item" href="<?php echo base_url(); ?>">Home</a>
							<span class="brd-separetor">/</span>
							<span class="breadcrumb_item active">About Us</span>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Bradcaump area -->
    
    <!-- Start About Area -->
    <div class="page-about about_area bg--white section-padding--lg">
        <div class="container">
            <div class="row align-items-center pb-5">
                <div class="col-lg-6 col-sm-12 col-12">
                    <img src="<?php echo base_url('assets/images/about-slider.jpg'); ?>" />
                </div>
                <div class="col-lg-6 col-sm-12 col-12">
                    <div class="content">
                        <h3>Bookworthy</h3>
                        <p><strong>About Us</strong></p>
                        <p class="mt--20 mb--20">
                            Bookworthy is a second-hand trading bookstore – made for readers, collectors, learners, or traders. 
                            <br /><br />
                            Our website can connect you to other potential buyers and sellers, trading all sorts of books from novels to textbooks, science fiction to non-fiction.
                        </p>
                        
                    </div>
                </div>
            </div>

            <div class="row align-items-center py-5">
                <div class="col-lg-6 col-sm-12 col-12">
                    <div class="content">
                        <h3>Bookworthy</h3>
                        <p><strong>CSR</strong></p>
                        <p class="mt--20 mb--20">
                            “Not he who has much is rich, but he who gives much” – Erich Fromm
                            <br /><br />
                            Although we are a second-hand trading bookstore at our core, we believe in a bigger purpose. A percentage of proceedings from trades with us will go towards funding schools around the island. 
                        </p>
                        
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 col-12">
                    <img src="<?php echo base_url('assets/images/csr-slider.jpg'); ?>" />
                </div>
            </div>

            <div class="row align-items-center pt-5">
                <div class="col-lg-6 col-sm-12 col-12">
                    <img src="<?php echo base_url('assets/images/objective-slider.jpg'); ?>" />
                </div>
                <div class="col-lg-6 col-sm-12 col-12">
                    <div class="content">
                        <h3>Bookworthy</h3>
                        <p><strong>Objective</strong></p>
                        <p class="mt--20 mb--20">
                            “fill your house with stacks of books, in all the crannies and all the nooks.” – Dr. Seuss
                            <br /><br />
                            To us, books are more than pages bound to a cover. To us, it can store knowledge, take you to new worlds, and teach you new things. What we want, is for those who do not read to start reading, and for those to do read to read more.
                        </p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End About Area -->
        
<?php $this->load->view('components/common/footer'); ?>