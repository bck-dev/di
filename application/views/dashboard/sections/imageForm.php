<div class="col-12">
    <div class="card p-3">
        <div class="row">
            <?php $this->load->view('dashboard/sections/productBasicDetail') ?>
        </div>
        <div class="row">
            <div class="w-100 mt-4">
                <div class="bg-warning px-2 py-1 color-palette"><b>Product Images</b></div>
            </div>
            <div class="col-lg-6">
                <form action="<?php echo base_url('admin/inventory/image/update/');  ?><?php echo $product->id ?>"
                    method="POST" enctype="multipart/form-data">
                    <div class="row mt-3">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="coverImage">Cover Image</label>
                                <input type="file" name="coverImage" id="coverImage" class="form-control"
                                    <?php if (!isset($product->coverImage)) : echo 'required';
                                                                                                            endif; ?> />
                            </div>
                            <?php foreach ($colors as $color) : ?>
                            <div class="form-group">
                                <label for="color<?php echo $color->colorId; ?>">
                                    <div class="float-left mr-3 d-flex mt-1">
                                        <div class="color-circle mr-2"
                                            style="background-color: <?php echo $color->code; ?>"></div>
                                        <div><?php echo $color->color; ?> Image</div>
                                    </div>
                                </label>
                                <input type="file" name="color<?php echo $color->colorId; ?>"
                                    id="color<?php echo $color->colorId; ?>" class="form-control" />
                            </div>
                            <?php endforeach; ?>

                            <input type="submit" class="btn btn-md btn-warning" value="Update Images" />
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-lg-6">
                <div class="row">
                    <?php if ($product->coverImage) : ?>
                    <div class="col-3 p-1 text-center">
                        <div class="bg-dark disabled px-2 py-1 color-palette">
                            <span>Cover</span>
                        </div>
                        <img src="<?php echo base_url() . $product->coverImage; ?>" class="w-100" />
                    </div>
                    <?php endif; ?>
                    <?php foreach ($productImages as $image) : ?>
                    <div class="col-3 p-1 text-center" id="imageBox<?php echo $image->id; ?>">
                        <div class="bg-dark px-2 py-1 color-palette"
                            style="background-color: <?php echo $image->code; ?> !important">
                            <span><?php if ($image->color) : echo $image->color;
                                        else : echo "Gallery";
                                        endif; ?></span>
                        </div>
                        <img src="<?php echo base_url() . $image->path; ?>" class="w-100" />
                        <button class="btn btn-block btn-danger btn-sm" data-id="<?php echo $image->id; ?>"
                            onclick="deleteImage(this);">Delete</button>
                    </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
function deleteImage(element) {
    var imageId = $(element).data('id');

    console.log(imageId);
    $.ajax({
        url: "<?php echo base_url('admin/inventory/image/delete'); ?>",
        type: 'post',
        data: {
            id: imageId
        },
        dataType: 'json',
        success: function(results) {
            $("#imageBox" + imageId).addClass('d-none');
        },
        error: function() {
            console.log('error');
        }
    });

}
</script>