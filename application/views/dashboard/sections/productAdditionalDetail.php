<div class="w-100 mt-4 px-2">
    <div class="bg-warning px-2 py-1 color-palette">
        <b>Product Additional Detail</b>
        <a class="btn btn-xs ml-2 btn-danger float-right white"
            href="<?php echo base_url('admin/inventory/detail/') . $product->id ?>">Edit</a>
    </div>

    <div class="row mt-3">
        <div class="col-lg-2"></div>
        <div class="col-lg-4">
            <div class="bg-dark p-1 color-palette text-center small-font">
                <span><b>Title</b></span>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="bg-dark p-1 color-palette text-center small-font">
                <span>Content</span>
            </div>
        </div>
    </div>
    <?php $i = 1;
    foreach ($productDetails as $d) : ?>
    <div class="row mt-3">
        <div class="col-lg-2">
            Detail <b>#<?php echo $i ?></b>
        </div>
        <div class="col-lg-4"><?php echo $d->title; ?></div>
        <div class="col-lg-6"><?php echo $d->content; ?></div>
    </div>
    <?php $i++;
    endforeach; ?>
</div>