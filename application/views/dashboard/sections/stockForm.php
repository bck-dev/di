<div class="col-12">
    <div class="card p-3">
        <div class="row">
            <?php $this->load->view('dashboard/sections/productBasicDetail') ?>
        </div>
        <div class="row">
            <div class="w-100 mt-4">
                <div class="bg-warning px-2 py-1 color-palette"><b>Product Stock Detail</b></div>

                <div class="d-flex flex-row-reverse mt-3">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input custom-control-input-danger" type="checkbox"
                            id="singlePrice">
                        <label for="singlePrice" class="custom-control-label">Single Price Product</label>
                    </div>
                </div>
                <form action="<?php echo base_url('admin/inventory/stock/update/');  ?><?php echo $product->id ?>"
                    method="POST">
                    <?php $i = 1;
                    foreach ($productOptions as $opt) : ?>
                    <div class="row mt-3">
                        <div class="col-lg-2">
                            Option <b>#<?php echo $i ?></b>
                        </div>
                        <div class="col-lg-2">
                            <div class="bg-dark disabled p-1 color-palette text-center small-font">
                                <span><?php echo $opt->size; ?></span>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="bg-dark p-1 color-palette text-center small-font"
                                style="color: #fff; background-color: <?php echo $opt->code; ?> !important">
                                <span><?php echo $opt->color; ?></span>
                            </div>
                        </div>
                        <div class="col-lg-3 d-flex">
                            <label class="mr-3">Price</label>
                            <input type="text" class="form-control form-control-sm price" name="price<?php echo $i; ?>"
                                required placeholder="Price" id="price<?php echo $i; ?>"
                                value="<?php if ($opt->price > 0) : echo $opt->price;
                                                                                                                                                                                            endif; ?>" />
                        </div>
                        <div class="col-lg-3 d-flex">
                            <label class="mr-3">Stock</label>
                            <input type="text" class="form-control form-control-sm" name="stock<?php echo $i; ?>"
                                required placeholder="Stock"
                                value="<?php if ($opt->price > 0) : echo $opt->stock;
                                                                                                                                                            endif; ?>" />
                        </div>
                        <input type="hidden" name="optId<?php echo $i; ?>" value="<?php echo $opt->id; ?>" />
                    </div>
                    <?php $i++;
                    endforeach; ?>
                    <div class="row mt-3">
                        <div class="col-lg-12">
                            <input type="hidden" name="count" value="<?php echo $i - 1; ?>" />
                            <input type="submit" class="btn btn-md btn-warning" value="Update Stock Detail" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$("#singlePrice").change(function() {
    if ($("#singlePrice").is(":checked")) {
        console.log($('#price1').val());
        $('.price').val($('#price1').val());
    }
});
</script>