<!-- column -->
<div class="col-lg-6">
    <!-- general form elements -->
    <?php $this->load->view('dashboard/sections/error') ?>
    <?php echo validation_errors(); ?>

    <?php if ($this->session->flashdata('error')) : ?>
    <div class="alert alert-dismissible" style="background: red;">
        <?= $this->session->flashdata('error'); ?>
    </div>
    <?php elseif ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible">
        <?= $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Discount Form</h3>
        </div>
        <!-- form start -->

        <form action="<?php echo base_url('admin/inventory/discount/add'); ?>" method="POST" name="addForm">
            <div class="card-body">
                <div class="form-group">

                    <label for="inputTitle">Discount Percentage</label>
                    <input type="text" class="form-control" placeholder="Enter Discount Percentage" name='percentage'
                        value="<?php echo $updateData->categoryName; ?>" required>

                    <label>Products</label>
                    <select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger"
                        multiple="multiple" data-placeholder="Select Products" name="products[]" style="width: 100%;">
                        <?php foreach ($products as $p) : ?>
                        <option value="<?php echo $p->id; ?>"><?php echo $p->productName; ?> |
                            <?php echo $p->categoryName; ?> | <?php echo $p->lowestPrice; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="save">Update Discount</button>
            </div>
        </form>
    </div>
</div>