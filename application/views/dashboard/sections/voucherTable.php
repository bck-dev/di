<div class="col-lg-6">
    <div class="card p-3">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Voucher Image</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Valid-Period</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allData as $dataRow) : ?>
                <tr>
                    <td>
                    <?php if ($dataRow->image) : ?>
                     <img src="<?php echo base_url() . $dataRow->image ?>" class="small-thump" height="50px" />
                    <?php endif ?>
                    </td>  
                    <td><?php echo $dataRow->name; ?></td>                   
                    <td><?php echo number_format($dataRow->amount,2); ?></td>
                    <td><?php echo $dataRow->valid_period; ?>days</td>                    
                    <td>
                        <a class="btn btn-xs btn-warning"
                            href="<?php echo base_url('admin/voucher/loadUpdate/'); ?><?php echo $dataRow->id ?>">Edit</a>
                        <a class="btn btn-xs btn-danger"
                           onclick="confirm('Are you sure want to delete --<?php echo $dataRow->name; ?>')" href="<?php echo base_url('admin/voucher/delete/'); ?><?php echo $dataRow->id ?>">Delete</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>