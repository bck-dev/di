<div class="col-6">
    <div class="card p-3">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Material</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allData as $dataRow) : ?>
                <tr>
                    <td><?php echo $dataRow->material; ?></td>
                    <td>
                        <a class="btn btn-xs btn-warning"
                            href="<?php echo base_url('admin/material/loadUpdate/'); ?><?php echo $dataRow->id ?>">Edit</a>
                        <a class="btn btn-xs btn-danger"
                            href="<?php echo base_url('admin/material/delete/'); ?><?php echo $dataRow->id ?>">Delete</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>