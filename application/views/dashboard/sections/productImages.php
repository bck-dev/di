<div class="w-100 mt-4 px-2">
    <div class="bg-warning px-2 py-1 color-palette">
        <b>Product Images</b>
        <a class="btn btn-xs ml-2 btn-danger float-right white"
            href="<?php echo base_url('admin/inventory/image/') . $product->id ?>">Edit</a>
    </div>
    <div class="row m-0 pt-3">
        <?php foreach ($productImages as $image) : ?>
        <div class="col-12 p-1 text-center" id="imageBox<?php echo $image->id; ?>">
            <div class="bg-dark px-2 py-1 color-palette"
                style="background-color: <?php echo $image->code; ?> !important">
                <span><?php if ($image->color) : echo $image->color;
                            else : echo "Gallery";
                            endif; ?></span>
            </div>
            <img src="<?php echo base_url() . $image->path; ?>" class="w-100" />

        </div>
        <?php endforeach; ?>
    </div>
</div>