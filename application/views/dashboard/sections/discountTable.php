<div class="col-6">
    <div class="card p-3">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Category</th>
                    <th>Lowest Price</th>
                    <th>Discount (%)</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($discountedproducts as $product) : ?>
                <tr>
                    <td><?php echo $product->productName; ?></td>
                    <td><?php echo $product->categoryName; ?></td>
                    <td><?php echo $product->lowestPrice; ?></td>
                    <td><?php echo $product->discount; ?></td>
                    <td>
                        <a class="btn btn-xs btn-danger"
                            onclick="confirm('Are you sure want to remove --<?php echo $product->productName; ?> --Discount?')"
                            href="<?php echo base_url('admin/inventory/discount/remove/'); ?><?php echo $product->id ?>">Remove</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>