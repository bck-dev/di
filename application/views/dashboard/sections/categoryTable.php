<div class="col-6">
    <div class="card p-3">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Parent</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allData as $dataRow) : ?>
                <tr>
                    <td><?php echo $dataRow->categoryName; ?></td>
                    <td><?php echo $dataRow->parentName; ?></td>
                    <td>
                        <a class="btn btn-xs btn-warning"
                            href="<?php echo base_url('admin/category/loadUpdate/'); ?><?php echo $dataRow->id ?>">Edit</a>
                        <a class="btn btn-xs btn-danger"
                           onclick="confirm('Are you sure want to delete --<?php echo $dataRow->categoryName; ?> --category?')" href="<?php echo base_url('admin/category/delete/'); ?><?php echo $dataRow->id ?>">Delete</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>