<div class="col-12">
    <div class="card p-3">
        <div class="row">
            <?php $this->load->view('dashboard/sections/productBasicDetail') ?>
        </div>
        <div class="row">
            <div class="w-100 mt-4">
                <div class="bg-warning px-2 py-1 color-palette"><b>Product Additional Detail</b></div>
            </div>
            <div class="col-lg-6">
                <form
                    action="<?php echo base_url('admin/inventory/detail/') . $action . '/' . $product->id . '/' . $updateData->id ?>"
                    method="POST" enctype="multipart/form-data">
                    <div class="row mt-3">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Detail Title</label>
                                <select name="title" class="form-control" required>
                                    <?php if ($updateData->title) : ?>
                                    <option selected="selected"><?php echo $updateData->title; ?></option>
                                    <?php else : ?>
                                    <option disabled="disabled" selected="selected">Select Title</option>
                                    <?php endif; ?>
                                    <?php foreach ($detailTitles as $t) : ?>
                                    <option><?php echo $t->detailTitle; ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="inputTitle">Detail Content</label>
                                <textarea class="textarea" name='content'
                                    required><?php echo $updateData->content; ?></textarea>
                            </div>

                            <input type="submit" class="btn btn-md btn-warning" value="Save Detail" />
                            <a class="btn btn-md ml-2 btn-danger"
                                href="<?php echo base_url('admin/inventory/view/') . $product->id ?>">Finalize
                                Product Details</a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-lg-6 pl-4">
                <div class="row mt-3 mb-2">
                    <div class="col-1 p-0">
                        <div class="bg-dark disabled px-2 py-1 color-palette">
                            <span>#</span>
                        </div>
                    </div>
                    <div class="col-3 p-0">
                        <div class="bg-dark px-2 py-1 color-palette">
                            <span>Title</span>
                        </div>
                    </div>
                    <div class="col-6 p-0">
                        <div class="bg-dark disabled px-2 py-1 color-palette">
                            <span>Content</span>
                        </div>
                    </div>
                    <div class="col-2 p-0">
                        <div class="bg-dark px-2 py-1 color-palette">
                            <span>Actions</span>
                        </div>
                    </div>
                </div>
                <?php $i = 1;
                foreach ($productDetails as $pd) : ?>
                <div class="row">
                    <div class="col-1">
                        <?php echo "#" . $i; ?>
                    </div>
                    <div class="col-3">
                        <?php echo $pd->title ?>
                    </div>
                    <div class="col-6">
                        <?php echo $pd->content ?>
                    </div>
                    <div class="col-2">
                        <a class="btn btn-xs btn-warning"
                            href="<?php echo base_url('admin/inventory/detail/loadUpdate/') . $pd->productId . '/' . $pd->id ?>">Edit</a>
                        <a class="btn btn-xs btn-danger"
                            href="<?php echo base_url('admin/inventory/detail/delete/') . $pd->productId . '/' . $pd->id ?>">Delete</a>

                    </div>
                </div>
                <?php $i++;
                endforeach; ?>
            </div>
        </div>
    </div>
</div>