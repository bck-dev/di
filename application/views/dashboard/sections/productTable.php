<div class="col-12">
    <div class="card p-3">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Type</th>
                    <th>Category</th>
                    <th>Material</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $dataRow) : ?>
                <tr>
                    <td>
                        <?php if ($dataRow->coverImage) : ?>
                        <img src="<?php echo base_url() . $dataRow->coverImage ?>" class="small-thump" />
                        <?php endif ?>
                        <?php echo $dataRow->productName; ?>
                    </td>
                    <td><?php echo $dataRow->type; ?></td>
                    <td><?php echo $dataRow->categoryName; ?></td>
                    <td><?php echo $dataRow->material; ?></td>
                    <td><?php echo $dataRow->description; ?></td>
                    <td>
                        <a class="btn btn-xs btn-warning"
                            href="<?php echo base_url('admin/inventory/view/'); ?><?php echo $dataRow->id ?>">View</a>
                        <a class="btn btn-xs btn-danger"
                            href="<?php echo base_url('admin/inventory/delete/'); ?><?php echo $dataRow->id ?>">Delete</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>