<div class="w-100 p-0 m-0">


    <div class="row">
        <div class="col-lg-12 pb-3">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
                <div class="card-header border-transparent">
                    <h3 class="card-title">All Orders</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <div class="card p-3">
                            <table class="table table-bordered table-striped" id="datatable">
                                <thead>
                                    <tr>
                                        <th>Order No</th>
                                        <th>Customer Info</th>
                                        <th>Items</th>
                                        <th>Status</th>
                                        <th>Order Time</th>
                                        <th style="text-align:right">Total Value</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($orders as $order) : ?>
                                    <tr>
                                        <td><a
                                                href="<?php echo base_url('admin/order/view/') ?><?php echo $order->id ?>"><?php echo $order->order_no; ?></a>
                                        </td>

                                        <td>
                                            <?php echo $order->first_name; ?><br />
                                            <?php echo $order->telephone; ?><br />
                                        </td>

                                        <td>
                                            <?php foreach ($order->items as $item) : ?>
                                            <?php echo $item->productName; ?><br />
                                            <?php endforeach; ?>
                                        </td>

                                        <td>
                                            <?php if ($order->order_status == '0') { ?>
                                            <span class="badge badge-success">Order Received</span>
                                            <?php } else if ($order->order_status == '1') { ?>
                                            <span class="badge badge-info">On Process</span>
                                            <?php } else if ($order->order_status == '2') { ?>
                                            <span class="badge badge-warning">On Route</span>
                                            <?php } else if ($order->order_status == '3') { ?>
                                            <span class="badge badge-danger">Delivered</span>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php echo $order->order_time; ?>
                                        </td>
                                        <td>
                                            <div class="sparkbar text-right" data-color="#00c0ef" data-height="20">LKR.
                                                <?php echo number_format($order->grand_total,2); ?></div>
                                        </td>
                                        <td>
                                            <a class="btn btn-xs btn-danger text-right"
                                                href="<?php echo base_url('admin/order/view/'); ?><?php echo $order->id ?>">View</a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.table-responsive -->
                </div>

            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->


        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>