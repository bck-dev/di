<div class="col-lg-12">
    <div class="card p-3">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Main Banner</th>
                    <th>Slider-1</th>
                    <th>Slider-2</th>
                    <!-- <th>Slider-3</th> -->
                    <th>Title</th>
                    <th>Sub-Title</th>
                    <th>Action</th>
                    <!-- <th></th>
                    <th></th> -->
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allData as $dataRow) : ?>
                <tr>
                <td>
                    <?php if ($dataRow->image) : ?>
                     <img src="<?php echo base_url() . $dataRow->image ?>" class="small-thump" height="50px" />
                    <?php endif ?>
                    </td>
                    <td>
                    <?php if ($dataRow->image1) : ?>
                     <img src="<?php echo base_url() . $dataRow->image1 ?>" class="small-thump" height="50px" />
                    <?php endif ?>
                    </td> 
                    <td>
                    <?php if ($dataRow->image2) : ?>
                     <img src="<?php echo base_url() . $dataRow->image2 ?>" class="small-thump" height="50px" />
                    <?php endif ?>
                    </td> 
                    <td><?php echo $dataRow->title; ?></td>                   
                    <td><?php echo $dataRow->sub_title; ?></td>                    
                    <td>
                        <a class="btn btn-xs btn-warning"
                            href="<?php echo base_url('admin/slider/loadUpdate/'); ?><?php echo $dataRow->id ?>">Edit</a>
                        <a class="btn btn-xs btn-danger"
                           onclick="confirm('Are you sure want to delete --<?php echo $dataRow->title; ?>')" href="<?php echo base_url('admin/slider/delete/'); ?><?php echo $dataRow->id ?>">Delete</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>