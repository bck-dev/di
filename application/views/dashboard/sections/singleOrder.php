<div class="w-100 p-0 m-0">
    <div class="row">
        <div class="col-lg-12 pb-3">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
                <div class="row p-4 m-0">
                    <div class="col-lg-4 col-12 ">
                        <h3>Customer Details</h3>
                        <p>FirstName: <?php echo $orderData->first_name; ?><br />
                            LastName: <?php echo $orderData->last_name; ?><br />
                            Email: <?php echo $orderData->email; ?><br />
                            Telephone: <?php echo $orderData->telephone; ?><br />
                            <?php if($order_data->billing_address):?>
                            Billing Address: <?php echo $orderData->billing_address; ?>
                            <?php endif ?>
                        </p>

                        <h5>Delivery Address</h5>
                        <p>
                            <?php echo $orderData->address1; ?><br />
                            <?php echo $orderData->address2; ?><br />
                            <?php echo $orderData->city; ?><br />
                        </p>

                        <h5>Order Status</h5>
                        <?php if ($orderData->order_status == 0): ?>
                        <span class="badge badge-success">Order Received</span>
                        <br /><br /><br />
                        <a class="btn btn-danger"
                            href="<?php echo base_url('admin/order/changeStatus/').$orderData->order_no.'/'.$orderData->order_status; ?>">Change
                            to "On Progress"</a>
                        <?php elseif ($orderData->order_status == 1): ?>
                        <span class="badge badge-info">On Process</span>
                        <br /><br /><br />
                        <a class="btn btn-danger"
                            href="<?php echo base_url('admin/order/changeStatus/').$orderData->order_no.'/'.$orderData->order_status; ?>">Change
                            to "On Route"</a>
                        <?php elseif ($orderData->order_status == 2): ?>
                        <span class="badge badge-warning">On Route</span>
                        <br /><br /><br />
                        <a class="btn btn-danger"
                            href="<?php echo base_url('admin/order/changeStatus/').$orderData->order_no.'/'.$orderData->order_status; ?>">Change
                            to "Delivered"</a>
                        <?php elseif ($orderData->order_status == 3): ?>
                        <span class="badge badge-danger">Delivered</span>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-8 col-12">
                        <div class="your-order">
                            <h3>Order Items</h3>
                            <div class="your-order-table table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Product</th>
                                            <th>Qty</th>
                                            <th align="right">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($orderItems as $i) :?>
                                        <tr>
                                            <td><img src="<?php echo base_url().$i->coverImage; ?>"
                                                    style="max-width: 60px;" /></td>
                                            <td>
                                                <?php echo $i->productName ?><br />
                                                <?php if($i->color): ?>
                                                Color: <?php echo $i->color ?><br />
                                                <?php endif; ?>
                                                <?php if($i->size): ?>
                                                Size: <?php echo $i->size ?>
                                                <?php endif; ?>
                                            </td>

                                            <td><?php echo $i->quantity ?></td>
                                            <td class="cart-product-total" align="right"><span class="amount">LKR
                                                    <?php echo number_format($i->total, 2); ?></span>
                                            </td>

                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3">Cart Subtotal</th>

                                            <td align="right"><span class="amount">LKR
                                                    <?php echo number_format($orderData->cart_total, 2 )?></span>
                                            </td>
                                        </tr>
                                        <?php if ($orderData->deduction_amount >0): ?>
                                        <tr>
                                            <th colspan="3">Voucher (Reference: <?php echo $orderData->reference; ?>)
                                            </th>

                                            <td align="right"><span class="amount">LKR
                                                    <?php echo number_format($orderData->deduction_amount, 2 )?></span>
                                            </td>
                                        </tr>
                                        <?php endif; ?>
                                        <tr>
                                            <th colspan="3">Delivery Fees (<?php echo $orderData->city; ?>)</th>
                                            <td align="right"><span class="amount">LKR
                                                    <?php echo number_format($orderData->delivery_fees, 2 )?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="3">Grand Total</th>
                                            <td align="right"><strong><span class="amount">LKR
                                                        <?php echo number_format($orderData->grand_total,2) ?></span></strong>

                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>
</div>
<!-- /.row -->
</div>