<!-- column -->
<div class="col-lg-6">
    <!-- general form elements -->
    <?php $this->load->view('dashboard/sections/error') ?>
    <?php echo validation_errors(); ?>

            <?php if($this->session->flashdata('error')) : ?>
                <div class="alert alert-dismissible" style="background: red;">
                <?= $this->session->flashdata('error'); ?>
                </div>
                <?php elseif($this->session->flashdata('success')) : ?>
                    <div class="alert alert-success alert-dismissible">
                <?= $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            
    <div class="card card-primary">
        <div class="card-header">           
            <h3 class="card-title">Category Form</h3>
        </div>
        <!-- form start -->
        
        <form action="<?php echo base_url('admin/category/' . $action . '/'); ?><?php echo $updateData->id ?>"
            method="POST" name="addForm">
            <div class="card-body">
                <div class="form-group">

                    <label for="inputTitle">Category</label>
                    <input type="text" class="form-control" placeholder="Enter Category" name='categoryName'
                        value="<?php echo $updateData->categoryName; ?>" required>

                    <label>Parent Category</label>
                    <select name="parent" class="form-control" required>
                        <?php if ($updateData->parent) : ?>
                        <option selected="selected"><?php echo $a->categoryName; ?></option>
                        <?php else : ?>
                        <option disabled="disabled" selected="selected">Select Parent Category</option>
                        <?php endif; ?>
                        <option>None</option>
                        <?php foreach ($dropDown as $d) : ?>
                        <option value="<?php echo $d['id']; ?>"><?php echo $d['displayValue']; ?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <!-- /.card-body -->
            <?php if ($action == 'update') { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="update">Update</button>
            </div>
            <?php } else { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="submit">Add</button>
            </div>
            <?php } ?>
        </form>
    </div>
</div>