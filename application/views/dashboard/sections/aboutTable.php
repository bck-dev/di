<div class="col-lg-6">
    <div class="card p-3">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Image</th>                    
                    <th>Title</th>
                    <th>Position</th>
                    <!-- <th>Alignment</th> -->
                    <th>Action</th>
                    <!-- <th></th>
                    <th></th> -->
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allData as $dataRow) : ?>
                <tr>
                    <td>
                    <?php if ($dataRow->image) : ?>
                     <img src="<?php echo base_url() . $dataRow->image ?>" class="small-thump" height="50px" />
                    <?php endif ?>
                    </td> 
                    <td><?php echo $dataRow->title; ?></td>
                    <td><?php echo $dataRow->position; ?></td>                                       
                    <!-- <td><?php echo $dataRow->alignment; ?></td> -->
                    <td>
                        <a class="btn btn-xs btn-warning"
                            href="<?php echo base_url('admin/about/loadUpdate/'); ?><?php echo $dataRow->id ?>">Edit</a>
                        <a class="btn btn-xs btn-danger"
                           onclick="confirm('Are you sure want to delete --<?php echo $dataRow->title; ?>')" href="<?php echo base_url('admin/about/delete/'); ?><?php echo $dataRow->id ?>">Delete</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>