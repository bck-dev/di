<!-- column -->
<div class="col-lg-6">
    <!-- general form elements -->
    <?php $this->load->view('dashboard/sections/error') ?>
    <?php echo validation_errors(); ?>

            <?php if($this->session->flashdata('error')) : ?>
                <div class="alert alert-dismissible" style="background: red;">
                <?= $this->session->flashdata('error'); ?>
                </div>
                <?php elseif($this->session->flashdata('success')) : ?>
                    <div class="alert alert-success alert-dismissible">
                <?= $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
     
    <div class="card card-primary">
        <div class="card-header">           
            <h3 class="card-title">Delivery Fees Form</h3>
        </div>
        <!-- form start -->
        
        <form action="<?php echo base_url('admin/fees/' . $action . '/'); ?><?php echo $updateData->id ?>"
            method="POST" name="addForm">
           
            <div class="card-body">
                <div class="form-group">
                    <label for="inputTitle">Area</label>
                    <input type="text" class="form-control" placeholder="Enter Area" name='area'
                        value="<?php echo $updateData->area; ?>" required>

                    <label for="inputTitle">Duration</label>                    
                    <input type="text" class="form-control" placeholder="Enter Duration in days" name='duration'
                        value="<?php echo $updateData->duration; ?>" required>

                    <label for="inputTitle">Fees</label>                    
                    <input type="text" class="form-control" placeholder="Enter Fees" name='fees'
                        value="<?php echo $updateData->fees; ?>" required>          

                </div>
            </div>
            <!-- /.card-body -->
            <?php if ($action == 'update') { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="update">Update</button>
            </div>
            <?php } else { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="submit">Add</button>
            </div>
            <?php } ?>
        </form>
    </div>
    
</div>