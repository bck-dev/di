<!-- column -->
<div class="col-lg-6">
    <!-- general form elements -->
    <?php $this->load->view('dashboard/sections/error') ?>
    <?php echo validation_errors(); ?>

            <?php if($this->session->flashdata('error')) : ?>
                <div class="alert alert-dismissible" style="background: red;">
                <?= $this->session->flashdata('error'); ?>
                </div>
                <?php elseif($this->session->flashdata('success')) : ?>
                    <div class="alert alert-success alert-dismissible">
                <?= $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
    
    <div class="card card-primary">
        <div class="card-header">           
            <h3 class="card-title">Voucher Form</h3>
        </div>
        <!-- form start -->
        
        <form action="<?php echo base_url('admin/voucher/' . $action . '/'); ?><?php echo $updateData->id ?>"
            method="POST" name="addForm" enctype="multipart/form-data">
            
            <div class="card-body">
                <div class="form-group">          

                    <label for="inputTitle">Name</label>
                    <input type="text" class="form-control" placeholder="Enter Name" name='name'
                        value="<?php echo $updateData->name; ?>" required>

                    <label for="inputTitle">Amount</label>
                    <input type="text" class="form-control" placeholder="Enter Amount" name='amount'
                        value="<?php echo $updateData->amount; ?>" required>

                    <label for="inputTitle">Valid-Period</label>
                    <input type="text" class="form-control" placeholder="Enter Valid-Period" name='valid_period'
                        value="<?php echo $updateData->valid_period; ?>" required>

                    <label for="inputTitle">Description</label>                    
                    <textarea class="textarea" name='description' required>
                        <?php echo $updateData->description; ?>
                    </textarea>
                    
                    <label for="VoucherImage">Voucher Image</label><br/>
                    <input type="file" name='image' class=""
                        value="<?php echo $updateData->image; ?>" required>
                </div>
            </div>
            <!-- /.card-body -->
            <?php if ($action == 'update') { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="update">Update</button>
            </div>
            <?php } else { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="submit">Add</button>
            </div>
            <?php } ?>
        </form>
    </div>
    
</div>