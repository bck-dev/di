<div class="col-lg-12 bg-warning px-2 py-1 color-palette mb-3"><b>Product Basic Detail</b>
    <a class="btn btn-xs ml-2 btn-danger float-right white" 
        href="<?php echo base_url('admin/inventory/loadUpdate/') . $product->id ?>">Edit</a>
</div>
<?php if ($product->coverImage) : ?>
<div class="col-2 p-1"><img src="<?php echo base_url() . $product->coverImage; ?>" class="w-100" /></div>
<?php endif; ?>
<div class="col">
    <div class="row">
        <div class="col-3 p-1"><b>Product Name </b> <br /><?php echo $product->productName; ?></div>
        <div class="col-3 p-1"><b>Type</b> <br /><?php echo $product->type; ?></div>
        <div class="col-3 p-1"><b>Category</b> <br /><?php echo $product->categoryName; ?></div>

        <div class="col-3 p-1"><b>Material</b> <br /><?php echo $product->material; ?></div>
        <div class="col-3 p-1"><b>Description</b> <br /><?php echo $product->description; ?></div>


        <div class="col-3 p-1">
            <b>Labels</b>
            <br />
            <?php foreach ($labels as $label) : ?>
            <div class="float-left mr-2 d-flex mt-1">
                <div class="bg-danger px-2 py-1 color-palette small-font">
                    <span><?php echo $label->label; ?></span>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="col-3 p-1">
            <b>Sizes</b>
            <br />
            <?php foreach ($sizes as $size) : ?>
            <div class="float-left mr-2 d-flex mt-1">
                <div class="bg-danger px-2 py-1 color-palette small-font">
                    <span><?php echo $size->size; ?></span>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="col-3 p-1">
            <b>Colors</b>
            <br />
            <?php foreach ($colors as $color) : ?>
            <div class="float-left mr-3 d-flex mt-1">
                <div class="color-circle mr-2" style="background-color: <?php echo $color->code; ?>"></div>
                <div><?php echo $color->color; ?></div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>