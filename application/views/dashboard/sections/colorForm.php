<!-- column -->
<div class="col-lg-6">
    <!-- general form elements -->
    <?php $this->load->view('dashboard/sections/error') ?>

    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Color Form</h3>
        </div>
        <!-- form start -->
        <form action="<?php echo base_url('admin/color/' . $action . '/'); ?><?php echo $updateData->id ?>"
            method="POST" name="addForm">
            <div class="card-body">
                <div class="form-group">
                    <label for="inputTitle">Color Name</label>
                    <input type="text" class="form-control" placeholder="Enter Color Name" name='color'
                        value="<?php echo $updateData->color; ?>" required>

                    <br />
                    <label>Pick Color</label>

                    <div class="input-group my-colorpicker2">
                        <input type="text" name="code" value="<?php echo $updateData->code; ?>" class="form-control">

                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-square"
                                    style="color: <?php echo $updateData->code; ?>"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <?php if ($action == 'update') { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-info btn-md px-4" name="update">Update</button>
            </div>
            <?php } else { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-info btn-md px-4" name="submit">Add</button>
            </div>
            <?php } ?>
        </form>
    </div>
</div>