<!-- column -->
<div class="col-lg-6">
    <!-- general form elements -->
    <?php $this->load->view('dashboard/sections/error') ?>
    <?php echo validation_errors(); ?>

            <?php if($this->session->flashdata('error')) : ?>
                <div class="alert alert-dismissible" style="background: red;">
                <?= $this->session->flashdata('error'); ?>
                </div>
                <?php elseif($this->session->flashdata('success')) : ?>
                    <div class="alert alert-success alert-dismissible">
                <?= $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
    
    <div class="card card-primary">
        <div class="card-header">           
            <h3 class="card-title">AboutPage Form</h3>
        </div>
        <!-- form start -->
        
        <form action="<?php echo base_url('admin/about/' . $action . '/'); ?><?php echo $updateData->id ?>"
            method="POST" name="addForm" enctype="multipart/form-data">
            <?php $alignData = ['Left'=>'Left', 'Right'=>'Right']; ?>
            <div class="card-body">
                <div class="form-group">
                    <label for="inputTitle">Title</label>
                    <input type="text" class="form-control" placeholder="Enter Title" name='title'
                        value="<?php echo $updateData->title; ?>" required>
                    
                    <label for="inputTitle">Content</label>                    
                        <textarea class="textarea" name='content' required>
                            <?php echo $updateData->content; ?>
                        </textarea>
                        <div class="row">
                            <div class="col-5">
                                <label for="inputTitle">Position</label>                                
                                <select class="form-control" name="position" required> 
                                    <?php for ($position = 1; $position < 50; $position++) : ?>
                                    <option value="<?php echo $position; ?>"><?php echo $position; ?>
                                    </option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-7">
                             <label for="SliderImage">Image</label><br/>
                                <input type="file" name='image' class="" value="<?php echo $updateData->image; ?>" required> 
                                 <p>[ Images should be size with <b>600*600]</b></p> 
                                <!-- <label for="inputTitle">Alignment</label>
                                <select name="alignment" class="form-control" required>
                                    <?php if ($updateData->alignment) : ?>
                                    <option selected="selected"><?php echo $updateData->alignment; ?></option>
                                    <?php else : ?>
                                    <option disabled="disabled" selected="selected">Select Alignment</option>
                                    <?php endif; ?>
                                    <?php foreach ($alignData as $align) : ?>
                                    <option value="<?php echo $align; ?>"><?php echo $align; ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select> -->
                            </div>       
                        </div>
         
                </div>
            </div>
            <!-- /.card-body -->
            <?php if ($action == 'update') { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="update">Update</button>
            </div>
            <?php } else { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="submit">Add</button>
            </div>
            <?php } ?>
        </form>
    </div>
    
</div>