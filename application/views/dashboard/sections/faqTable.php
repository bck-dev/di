<div class="col-lg-6">
    <div class="card p-3">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Questions</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allData as $dataRow) : ?>
                <tr>
                    <td><?php echo $dataRow->category; ?></td>                   
                    <td><?php echo $dataRow->question; ?></td>                    
                    <td>
                        <a class="btn btn-xs btn-warning"
                            href="<?php echo base_url('admin/faq/loadUpdate/'); ?><?php echo $dataRow->id ?>">Edit</a>
                        <a class="btn btn-xs btn-danger"
                           onclick="confirm('Are you sure want to delete --<?php echo $dataRow->question; ?> --Question?')" href="<?php echo base_url('admin/faq/delete/'); ?><?php echo $dataRow->id ?>">Delete</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>