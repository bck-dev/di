<!-- column -->
<div class="col-lg-6">
    <!-- general form elements -->
    <?php $this->load->view('dashboard/sections/error') ?>

    <div class="card card-danger">
        <div class="card-header">
            <h3 class="card-title">Material Form</h3>
        </div>
        <!-- form start -->
        <form action="<?php echo base_url('admin/material/' . $action . '/'); ?><?php echo $updateData->id ?>"
            method="POST" name="addForm">
            <div class="card-body">
                <div class="form-group">
                    <label for="inputTitle">Material</label>
                    <input type="text" class="form-control" placeholder="Enter Material" name='material'
                        value="<?php echo $updateData->material; ?>" required>
                </div>
            </div>
            <!-- /.card-body -->
            <?php if ($action == 'update') { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-danger btn-md px-4" name="update">Update</button>
            </div>
            <?php } else { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-danger btn-md px-4" name="submit">Add</button>
            </div>
            <?php } ?>
        </form>
    </div>
</div>