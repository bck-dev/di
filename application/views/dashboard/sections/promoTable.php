<div class="col-7">
    <div class="card p-3">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Promocode</th>
                    <th>Offer Type</th>
                    <th>Percentage</th>
                    <!--<th>Start Date</th>
                    <th>End Date</th>-->
                    <th>Actions</th>
                </tr>
            </thead>
            
            <tbody>
                <?php foreach ($allData as $dataRow) : ?>
                <tr>
                    <td><?php echo $dataRow->promo_code; ?></td>
                    <td><?php echo $dataRow->offertype; ?></td>
                    <td><?php echo $dataRow->percentage; ?>%</td>
                    <!--<td><?php echo $dataRow->start_date; ?></td>
                    <td><?php echo $dataRow->end_date; ?></td>-->
                    <td>
                        <a class="btn btn-xs btn-warning"
                            href="<?php echo base_url('admin/promocode/loadUpdate/'); ?><?php echo $dataRow->id ?>">Edit</a>
                        <a class="btn btn-xs btn-danger"
                           onclick="confirm('Are you sure want to delete --<?php echo $dataRow->promo_code; ?> --Question?')" href="<?php echo base_url('admin/promocode/delete/'); ?><?php echo $dataRow->id ?>">Delete</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        
        </table>
    </div>
</div>