<div class="col-6">
    <div class="card p-3">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Size</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allData as $dataRow) : ?>
                <tr>
                    <td><?php echo $dataRow->size; ?></td>
                    <td>
                        <a class="btn btn-xs btn-warning"
                            href="<?php echo base_url('admin/size/loadUpdate/'); ?><?php echo $dataRow->id ?>">Edit</a>
                        <a class="btn btn-xs btn-danger"
                            href="<?php echo base_url('admin/size/delete/'); ?><?php echo $dataRow->id ?>">Delete</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>