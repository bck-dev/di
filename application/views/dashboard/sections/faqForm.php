<!-- column -->
<div class="col-lg-6">
    <!-- general form elements -->
    <?php $this->load->view('dashboard/sections/error') ?>
    <?php echo validation_errors(); ?>

            <?php if($this->session->flashdata('error')) : ?>
                <div class="alert alert-dismissible" style="background: red;">
                <?= $this->session->flashdata('error'); ?>
                </div>
                <?php elseif($this->session->flashdata('success')) : ?>
                    <div class="alert alert-success alert-dismissible">
                <?= $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
     
    <div class="card card-primary">
        <div class="card-header">           
            <h3 class="card-title">Faq Form</h3>
        </div>
        <!-- form start -->
        
        <form action="<?php echo base_url('admin/faq/' . $action . '/'); ?><?php echo $updateData->id ?>"
            method="POST" name="addForm">
            <?php $categoryData = ['Orders & Shipping'=>'Orders & Shipping', 'Return & Exchanges'=>'Return & Exchanges']; ?>
            <div class="card-body">
                <div class="form-group">
                    <label>Category</label>
                    <select name="category" class="form-control" required>
                        <?php if ($updateData->category) : ?>
                        <option selected="selected"><?php echo $updateData->category; ?></option>
                        <?php else : ?>
                        <option disabled="disabled" selected="selected">Select Category</option>
                        <?php endif; ?>
                        <?php foreach ($categoryData as $cat) : ?>
                        <option value="<?php echo $cat; ?>"><?php echo $cat; ?>
                        </option>
                        <?php endforeach; ?>
                    </select>                    

                    <label for="inputTitle">Question</label>
                    <input type="text" class="form-control" placeholder="Enter Question" name='question'
                        value="<?php echo $updateData->question; ?>" required>

                    <label for="inputTitle">Answer</label>                    
                        <textarea class="textarea" name='answer' required>
                            <?php echo $updateData->answer; ?>
                        </textarea>
                </div>
            </div>
            <!-- /.card-body -->
            <?php if ($action == 'update') { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="update">Update</button>
            </div>
            <?php } else { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="submit">Add</button>
            </div>
            <?php } ?>
        </form>
    </div>
    
</div>