<!-- column -->
<div class="col-lg-5">
    <!-- general form elements -->
    <?php $this->load->view('dashboard/sections/error') ?>
    <?php echo validation_errors(); ?>

            <?php if($this->session->flashdata('error')) : ?>
                <div class="alert alert-dismissible" style="background: red;">
                <?= $this->session->flashdata('error'); ?>
                </div>
                <?php elseif($this->session->flashdata('success')) : ?>
                    <div class="alert alert-success alert-dismissible">
                <?= $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
      
    <div class="card card-primary">
        <div class="card-header">           
            <h3 class="card-title">Promo Form</h3>
        </div>
        <!-- form start -->
        
        <form action="<?php echo base_url('admin/promocode/' . $action . '/'); ?><?php echo $updateData->id ?>"
            method="POST" name="addForm">
            <?php $offData = ['Coupan'=>'Coupan', 'Discount'=>'Discount']; ?>
            <div class="card-body">
                <div class="form-group">

                    <label for="inputTitle">PromoCode</label>
                    <input type="text" class="form-control" placeholder="Enter Title" name='promo_code'
                        value="DI<?php echo $updateData->promo_code; ?>" required>

                    <label>OfferType</label>
                    <select name="offertype" class="form-control" required>
                        <?php if ($updateData->offertype) : ?>
                        <option selected="selected"><?php echo $updateData->offertype; ?></option>
                        <?php else : ?>
                        <option disabled="disabled" selected="selected">Select offertype</option>
                        <?php endif; ?>
                        <?php foreach ($offData as $offd) : ?>
                        <option value="<?php echo $offd; ?>"><?php echo $offd; ?>
                        </option>
                        <?php endforeach; ?>
                    </select>                    

                    <label for="inputTitle">Percentage</label>
                    <input type="text" class="form-control" placeholder="Enter Percentage" name='percentage'
                        value="<?php echo $updateData->percentage; ?>" required>

                    <div class="row">
                        <div class="col-6">
                            <label for="inputTitle">StartDate</label>                    
                            <div class="input-group">
                                <input type="text" id="datepicker1" name="start_date" value="<?php echo $updateData->start_date; ?>" class="form-control datepicker1">

                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-calendar"
                                            style="<?php echo $updateData->start_date; ?>"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                        <label for="inputTitle">EndDate</label>                    
                            <div class="input-group ">
                                <input type="text" id="datepicker2" name="end_date" value="<?php echo $updateData->end_date; ?>" class="form-control datepicker">

                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-calendar"
                                            style="<?php echo $updateData->end_date; ?>"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
            <!-- /.card-body -->
            <?php if ($action == 'update') { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="update">Update</button>
            </div>
            <?php } else { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="submit">Add</button>
            </div>
            <?php } ?>
        </form>
    </div>
    
</div>


<script src="<?php echo base_url('assets/adminLte/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script>
        $('#datepicker1').datepicker({
            format: 'yyyy/mm/dd',
            autoclose: true,
            todayHighlight: true
        });
</script>
<script>
        $('#datepicker2').datepicker({
            format: 'yyyy/mm/dd',
            autoclose: true,
            todayHighlight: true
        });
</script>