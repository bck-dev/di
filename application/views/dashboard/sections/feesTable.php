<div class="col-lg-6">
    <div class="card p-3">
        <table id="datatable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Area</th>
                    <th>Duration</th>
                    <th>Fees</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($allData as $dataRow) : ?>
                <tr>
                    <td><?php echo $dataRow->area; ?></td>                   
                    <td><?php echo $dataRow->duration; ?> days</td>  
                    <td>LKR.<?php echo number_format($dataRow->fees,2); ?></td>                    
                    <td>
                        <a class="btn btn-xs btn-warning"
                            href="<?php echo base_url('admin/fees/loadUpdate/'); ?><?php echo $dataRow->id ?>">Edit</a>
                        <a class="btn btn-xs btn-danger"
                           onclick="confirm('Are you sure want to delete --<?php echo $dataRow->area; ?>?')" href="<?php echo base_url('admin/fees/delete/'); ?><?php echo $dataRow->id ?>">Delete</a>
                    </td>
                </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>