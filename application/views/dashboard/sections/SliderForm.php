<!-- column -->
<div class="col-lg-12">
    <!-- general form elements -->
    <?php $this->load->view('dashboard/sections/error') ?>
    <?php echo validation_errors(); ?>

    <?php if($this->session->flashdata('error')) : ?>
    <div class="alert alert-dismissible" style="background: red;">
        <?= $this->session->flashdata('error'); ?>
    </div>
    <?php elseif($this->session->flashdata('success')) : ?>
    <div class="alert alert-success alert-dismissible">
        <?= $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Slider Form</h3>
        </div>
        <!-- form start -->

        <form action="<?php echo base_url('admin/slider/' . $action . '/'); ?><?php echo $updateData->id ?>"
            method="POST" name="addForm" enctype="multipart/form-data">

            <div class="card-body">
                <div class="form-group">

                    <label for="inputTitle">Title</label>
                    <input type="text" class="form-control" placeholder="Enter Title" name='title'
                        value="<?php echo $updateData->title; ?>" required>

                    <label for="inputTitle">Subtitle</label>
                    <input type="text" class="form-control" placeholder="Enter Subtitle" name='sub_title'
                        value="<?php echo $updateData->sub_title; ?>" required>

                    <div class="row">
                        <div class="col-6">
                            <label for="inputTitle">Button text</label>
                            <input type="text" class="form-control" placeholder="Enter Button text" name='buttn_text'
                                value="<?php echo $updateData->buttn_text; ?>" required>
                        </div>
                        <div class="col-6">
                            <label for="inputTitle">Button link</label>
                            <input type="text" class="form-control" placeholder="Enter Button link" name='buttn_link'
                                value="<?php echo $updateData->buttn_link; ?>" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <label for="SliderImage">Main Banner Image</label><br />
                            <input type="file" name='image' class="" value="<?php echo $updateData->image; ?>" required>
                            <p>[Size <b>1920*660]</b></p>
                        </div>
                        <div class="col-md-3">
                            <label for="SliderImage">Slider Image-1</label><br />
                            <input type="file" name='image1' class="" value="<?php echo $updateData->image1; ?>"
                                required>
                            <p>[Size <b>280*280]</b></p>
                        </div>

                        <div class="col-md-3">
                            <label for="SliderImage">Slider Image-2</label><br />
                            <input type="file" name='image2' class="" value="<?php echo $updateData->image2; ?>"
                                required>
                            <p>[Size<b>540*540]</b></p>
                        </div>

                    </div>

                </div>
            </div>
            <!-- /.card-body -->
            <?php if ($action == 'update') { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="update">Update</button>
            </div>
            <?php } else { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-md px-4" name="submit">Add</button>
            </div>
            <?php } ?>
        </form>
    </div>

</div>