<div class="col-12">
    <div class="card p-3">
        <div class="row">
            <?php $this->load->view('dashboard/sections/productBasicDetail') ?>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <div class="row">
                    <?php $this->load->view('dashboard/sections/productImages') ?>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="row">
                    <?php $this->load->view('dashboard/sections/productStockDetail') ?>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="row">
                    <?php $this->load->view('dashboard/sections/productAdditionalDetail') ?>
                </div>
            </div>
        </div>
    </div>
</div>