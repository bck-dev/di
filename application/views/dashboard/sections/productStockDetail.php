<div class="w-100 mt-4 px-2">
    <div class="bg-warning px-2 py-1 color-palette">
        <b>Product Stock Detail</b>
        <a class="btn btn-xs btn-danger float-right white"
            href="<?php echo base_url('admin/inventory/stock/') . $product->id ?>">Edit</a>
    </div>

    <div class="row mt-3">
        <div class="col-lg-2"></div>
        <div class="col-lg-2"></div>
        <div class="col-lg-3"></div>
        <div class="col-lg-3">
            <div class="bg-dark disabled p-1 color-palette text-center small-font">
                <span><b>Price</b></span>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="bg-dark disabled p-1 color-palette text-center small-font">
                <span><b>Stock</b></span>
            </div>
        </div>
    </div>
    <?php $i = 1;
    $total = 0;
    foreach ($productOptions as $opt) : ?>
    <div class="row mt-3">
        <div class="col-lg-2">
            Option <b>#<?php echo $i ?></b>
        </div>
        <div class="col-lg-2">
            <div class="bg-dark disabled p-1 color-palette text-center small-font">
                <span><?php echo $opt->size; ?></span>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="bg-dark p-1 color-palette text-center small-font"
                style="color: #fff; background-color: <?php echo $opt->code; ?> !important">
                <span><?php echo $opt->color; ?></span>
            </div>
        </div>
        <div class="col-lg-3 text-right">
            <span>Rs. <?php echo $opt->price; ?></span>
        </div>
        <div class="col-lg-2 text-right">
            <span><?php echo $opt->stock; ?></span>
        </div>
    </div>
    <?php $i++;
        $total = $total + $opt->stock;
    endforeach; ?>
    <div class="row mt-3">
        <div class="col-lg-2"></div>
        <div class="col-lg-3"></div>
        <div class="col-lg-3"></div>
        <div class="col-lg-2"></div>
        <div class="col-lg-2 text-right">

            <div class="bg-dark disabled p-1 color-palette text-right">
                <span><b><?php echo number_format($total, 2); ?></b></span>
            </div>
        </div>
    </div>
</div>