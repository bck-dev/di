<!-- column -->
<div class="col-lg-12">
    <!-- general form elements -->
    <?php $this->load->view('dashboard/sections/error') ?>

    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Product Form</h3>
        </div>
        <!-- form start -->

        <form action="<?php echo base_url('admin/inventory/' . $action . '/');  ?><?php echo $updateData->id ?>"
            method="POST" name="addForm" enctype="multipart/form-data">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-lg-12">
                        <label for="productName">Product Name</label>
                        <input type="text" class="form-control" id="productName" placeholder="Enter Product Name"
                            name='productName' value="<?php echo $updateData->productName; ?>" required>
                    </div>

                    <div class="form-group col-lg-4">
                        <label for="type">Type</label>
                        <select name="type" id="type" class="form-control" required>
                            <?php if ($action == "update") : ?>
                            <option value="<?php echo $updateData->type; ?>"><?php echo $updateData->type; ?></option>
                            <?php else : ?>
                            <option value="" disabled="disabled" selected="selected">Type</option>
                            <?php endif; ?>
                            <option>Men</option>
                            <option>Women</option>
                            <option>Kids</option>
                        </select>
                    </div>

                    <div class="form-group col-lg-4">
                        <label for="category">Category</label>
                        <select id="category" name="category" class="form-control">
                            <?php if ($action == "update") : ?>
                            <option value="<?php echo $x->categoryName; ?>"><?php echo $x->categoryName; ?></option>
                            <?php else : ?>
                            <option value="" disabled="disabled" selected="selected">Category</option>
                            <?php endif; ?>
                            <?php foreach ($dropDown as $d) : ?>
                            <option value="<?php echo $d['realValue']; ?>"><?php echo $d['displayValue']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-lg-4">
                        <label for="material">Material</label>
                        <select name="material" id="material" class="form-control" required>
                            <?php if ($action == "update") : ?>
                            <option value="<?php echo $updateData->material; ?>"><?php echo $updateData->material; ?></option>
                            <?php else : ?>
                            <option value="" disabled="disabled" selected="selected">Material</option>
                            <?php endif; ?>
                            <?php foreach ($materials as $m) : ?>
                            <option><?php echo $m->material; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-lg-4">
                        <label>Size</label>
                        <select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger"
                            multiple="multiple" data-placeholder=" " name="size[]" style="width: 100%;">                            
                            <?php if ($action == "update") : ?>
                                <?php foreach ($ProductSizes as $sd) : ?>
                                    <option selected="selected"><?php echo $sd->size; ?></option>
                                <?php endforeach; ?>                            
                            <?php endif; ?>
                            
                            <?php foreach ($sizes as $s) : ?>
                            <option><?php echo $s->size; ?></option>
                            <?php endforeach; ?>
                            
                        </select>
                    </div>

                    <div class="form-group col-lg-4">
                        <label>Color</label>
                        <select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger"
                            multiple="multiple" data-placeholder="
                           " name="color[]" style="width: 100%;">                            
                            <?php if ($action == "update") : ?>
                                <?php foreach ($productColors as $cd) : ?>
                                    <option selected="selected"><?php echo $cd->color; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php foreach ($colors as $c) : ?>
                            <option><?php echo $c->color; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-lg-4">
                        <label>Label</label>
                        <select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger"
                            multiple="multiple" data-placeholder="
                           " name="label[]" style="width: 100%;"> 
                           <?php if ($action == "update") : ?>
                                <?php foreach ($labelsData as $ld) : ?>
                                    <option selected="selected"><?php echo $ld->label; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>                          

                            <?php foreach ($labels as $l) : ?>
                            <option><?php echo $l->label; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-lg-12">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id="description" placeholder="Enter Description"
                            name='description' value="<?php echo $updateData->description; ?>" required>
                    </div>
                </div>
            </div>
            <?php if ($action == 'update') { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-warning btn-md px-4" name="update">Update</button>
            </div>
            <?php } else { ?>
            <div class="card-footer">
                <button type="submit" class="btn btn-warning btn-md px-4" name="submit">Add</button>
            </div>
            <?php } ?>
        </form>
    </div>
</div>