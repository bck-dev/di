<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
        </ul>


    </nav>
    <!-- /.navbar -->


    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a style="color: white" class="brand-link">

            <span class="brand-text font-weight-light">E-Commerce System</span>
        </a>

        <div class="sidebar">

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="<?php echo base_url() ?>" class="nav-link" target="_blank">
                            <i class="nav-icon fas fa-globe"></i>
                            <p>Visit Website</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('admin') ?>" class="nav-link <?php if ($this->uri->segment(2)) {
                                                                                    } else {
                                                                                        echo "active";
                                                                                    } ?>">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview <?php if ($this->uri->segment(2) == "inventory") {
                                                            echo "menu-open";
                                                        } ?>">
                        <a href="<?php echo base_url('inventory') ?>" class="nav-link <?php if ($this->uri->segment(2) == "inventory") {
                                                                                            echo "active";
                                                                                        } ?>">
                            <i class="nav-icon fas fa-cubes"></i>
                            <p>
                                Inventory Management
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview ml-4">
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/inventory/new') ?>" class="nav-link">
                                    <i class="fas fa-plus nav-icon"></i>
                                    <p>Add Product</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/inventory/discount') ?>" class="nav-link">
                                    <i class="fas fa-percent nav-icon"></i>
                                    <p>Discount</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/inventory') ?>" class="nav-link">
                                    <i class="fas fa-clipboard-list nav-icon"></i>
                                    <p>List Product</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview <?php if ($this->uri->segment(2) == "size" || $this->uri->segment(2) == "detailTitle" || $this->uri->segment(2) == "category" || $this->uri->segment(2) == "color" || $this->uri->segment(2) == "label" || $this->uri->segment(2) == "material") {
                                                            echo "menu-open";
                                                        } ?>">
                        <a href="<?php echo base_url('inventory') ?>" class="nav-link <?php if ($this->uri->segment(2) == "category" || $this->uri->segment(2) == "size" || $this->uri->segment(2) == "detailTitle" || $this->uri->segment(2) == "color" || $this->uri->segment(2) == "label" || $this->uri->segment(2) == "material") {
                                                                                            echo "active";
                                                                                        } ?>">
                            <i class="nav-icon fas fa-cog"></i>
                            <p>
                                Product Settings
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview ml-4">
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/category') ?>" class="nav-link">
                                    <i class="fas fa-list nav-icon"></i>
                                    <p>Category</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/color') ?>" class="nav-link">
                                    <i class="fas fa-palette nav-icon"></i>
                                    <p>Color</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/detailTitle') ?>" class="nav-link">
                                    <i class="fas fa-heading nav-icon"></i>
                                    <p>Detail Title</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/label') ?>" class="nav-link">
                                    <i class="fas fa-tag nav-icon"></i>
                                    <p>Label</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/material') ?>" class="nav-link">
                                    <i class="far fa-dot-circle nav-icon"></i>
                                    <p>Material</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/size') ?>" class="nav-link">
                                    <i class="fas fa-ruler nav-icon"></i>
                                    <p>Size</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview <?php if ($this->uri->segment(2) == "user") {
                                                            echo "menu-open";
                                                        } ?>">
                        <a href="<?php echo base_url('inventory') ?>" class="nav-link <?php if ($this->uri->segment(2) == "user") {
                                                                                            echo "active";
                                                                                        } ?>">
                            <i class="nav-icon fas fa-cogs"></i>
                            <p>
                                System Settings
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview ml-4">
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/user') ?>" class="nav-link">
                                    <i class="fas fa-users-cog nav-icon"></i>
                                    <p>User Settings</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/slider') ?>" class="nav-link">                                   
                                    <i class="fas fa-image nav-icon"></i>
                                    <p>Slider Image</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('admin/featureImage') ?>" class="nav-link">
                                    <i class="fas fa-fire-alt nav-icon"></i>                                   
                                    <p>Feature Image</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('admin/faq') ?>" class="nav-link">
                            <i class="fa fa-question-circle nav-icon"></i>
                            <p>FAQ</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?php echo base_url('admin/fees') ?>" class="nav-link">
                            <i class="fas fa-shipping-fast nav-icon"></i>                         
                            <p>Delivery Fees</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?php echo base_url('admin/promocode') ?>" class="nav-link">
                        <i class="fab fa-adversal nav-icon"></i>
                            <p>Promo Code</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?php echo base_url('admin/voucher') ?>" class="nav-link">
                        <i class="fas fa-gift nav-icon"></i>
                            <p>Voucher</p>
                        </a>
                    </li>
                     <li class="nav-item">
                        <a href="<?php echo base_url('admin/about') ?>" class="nav-link">
                        <i class="fas fa-info-circle nav-icon"></i>
                            <p>About Page</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('logout') ?>" class="nav-link">
                            <i class="fas fa-sign-out-alt nav-icon"></i>
                            <p>Logout</p>
                        </a>
                    </li>

            </nav>
            <!-- /.sidebar-menu -->
        </div>
    </aside>

    <div class="content-wrapper p-3">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">