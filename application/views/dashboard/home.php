<?php $this->load->view('dashboard/common/header.php') ?>
<?php $this->load->view('dashboard/common/sidebar.php') ?>

<div class="w-100 p-0 m-0">
    <div class="row">
        <div class="col-lg-12 mb-4">
            <h1 class="m-0 text-dark">Dashboard</h1>
        </div>
    </div>
    <!-- Info boxes -->
    <div class="row">
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
                <span class="info-box-icon bg-purple elevation-1"><i class="fas fa-cubes"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total Orders</span>
                    <span class="info-box-number">
                    <?php echo $totalCount; ?>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-satellite-dish"></i></span>
                <!-- <i class="fas fa-thumbs-up"></i> -->
                <div class="info-box-content">
                    <span class="info-box-text">Pending Orders</span>
                    <span class="info-box-number"><?php echo $pendingCount; ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix hidden-md-up"></div>

        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-spinner"></i></span>
                <!-- <i class="fas fa-shopping-cart"></i> -->

                <div class="info-box-content">
                    <span class="info-box-text">On Progress</span>
                    <span class="info-box-number"><?php echo $progressCount; ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-running"></i></span>
                <!-- <i class="fas fa-users"></i> -->
                <div class="info-box-content">
                    <span class="info-box-text">On Route </span>
                    <span class="info-box-number"><?php echo $routeCount; ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->


    <div class="row">
        <div class="col-lg-9 pb-3">
            <!-- TABLE: LATEST ORDERS -->
            <div class="card">
                <div class="card-header border-transparent">
                    <h3 class="card-title">Latest Orders</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                                <tr>
                                    <th>Order No</th>
                                    <th>Customer Info</th>
                                    <th>Items</th>
                                    <th>Status</th>
                                    <th style="text-align:right">Total Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($orders as $order) : ?>
                                <tr>
                                    <td><a
                                            href="<?php echo base_url('admin/order/view/') ?><?php echo $order->id ?>"><?php echo $order->order_no; ?></a>
                                    </td>

                                    <td>
                                        <?php echo $order->first_name; ?><br />
                                        <?php echo $order->telephone; ?><br />
                                    </td>

                                    <td>
                                        <?php foreach ($order->items as $item) : ?>
                                        <?php echo $item->productName; ?><br />
                                        <?php endforeach; ?>
                                    </td>

                                    <td>
                                        <?php if ($order->order_status == '0') { ?>
                                        <span class="badge badge-success">Order Received</span>
                                        <?php } else if ($order->order_status == '1') { ?>
                                        <span class="badge badge-info">On Process</span>
                                        <?php } else if ($order->order_status == '2') { ?>
                                        <span class="badge badge-warning">On Route</span>
                                        <?php } else if ($order->order_status == '3') { ?>
                                        <span class="badge badge-danger">Delivered</span>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <div class="sparkbar text-right" data-color="#00c0ef" data-height="20">LKR.
                                            <?php echo number_format($order->grand_total,2); ?></div>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <hr />
                        <div class="button-wrap" style="text-align:center">
                            <a class="more-product text-uppercase" href="<?php echo base_url('admin/order') ?>">View All
                                Orders
                                <i class="lastudioicon-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">

                </div>
                <!-- /.card-footer -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->

        <div class="col-lg-3 pb-3">

            <div class="info-box mb-3 ">
                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-truck"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Delivered</span>
                    <span class="info-box-number"><?php echo $deliverCount; ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>

            <div class="info-box mb-3 bg-pink">
                <span class="info-box-icon"><i class="far fa-heart"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Unique Customers</span>
                    <span class="info-box-number"><?php echo $customerCount; ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
           
            <!-- Info Boxes Style 2 -->
            <div class="info-box mb-3 bg-orange">
                <span class="info-box-icon"><i class="fas fa-calendar-week"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Weekly Sales</span>
                    <span class="info-box-number">50,200</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box mb-3 bg-orange">
                <span class="info-box-icon"><i class="fas fa-calendar-week"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Monthly Sales</span>
                    <span class="info-box-number">50,200</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <div class="info-box mb-3 bg-purple">
                <span class="info-box-icon"><i class="fas fa-cloud-download-alt"></i></span>
                <?php foreach ($totalSales as $totalSales) : ?>
                <div class="info-box-content">
                    <span class="info-box-text">Total Sales</span>
                    <span class="info-box-number">LKR.<?php echo number_format($totalSales,2); ?></span>
                </div>
                <?php endforeach; ?>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->

            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>
<?php $this->load->view('dashboard/common/footer.php') ?>