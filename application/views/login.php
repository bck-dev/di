<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/topBar'); ?>
<?php $this->load->view('components/common/middleBar'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/common/miniCart'); ?>
<?php $this->load->view('components/common/breadcrumb'); ?>

<!-- Begin Main Content Area -->
<main class="main-content pb-100">
    <div class="main-account_area">
        <div class="container-fluid custom-space">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="login-area">
                        <ul class="hassub-item">
                            <li class="has-sub login-wrap">
                                <a href="javascript:void(0)">
                                    <i class="lastudioicon-a-check"></i>
                                    Login
                                </a>
                                <ul class="hassub-body show">
                                    <li>
                                        <form class="login-form" action="<?php echo base_url('checkLogin') ?>"
                                            method="POST">
                                            <div class="form-field pb-25">
                                                <label class="input-label" for="email">Email address *</label>
                                                <input id="email" class="input-field" type="email" name="email"
                                                    required>
                                            </div>
                                            <div class="form-field pb-30">
                                                <label class="input-label" for="password">Billing email</label>
                                                <input id="password" class="input-field" type="password" name="password"
                                                    required>
                                            </div>
                                            <div class="button-wrap position-center pt-30">
                                                <button type="submit" value="submit"
                                                    class="skudmart-btn primary-btn primary-hover fullwidth-btn">Log
                                                    in</button>
                                            </div>
                                            <a class="lost-pass pt-15" href="<?php echo base_url('forgot'); ?>">Lost
                                                your password?</a>
                                        </form>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- Main Content Area End Here -->



<?php $this->load->view('components/common/footer'); ?>
<?php $this->load->view('components/common/templateJs'); ?>
<?php $this->load->view('components/common/customJs'); ?>