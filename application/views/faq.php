<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/topBar'); ?>
<?php $this->load->view('components/common/middleBar'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/common/miniCart'); ?>
<?php $this->load->view('components/common/breadcrumb'); ?>


<main class="main-content pt-95 pb-100">
            <div class="container custom-space-2">          
                <div class="row col-lg-12"><?php foreach ($allData as $dataRow) : ?>    
                    <div class="col-lg-6">
                        <div class="frequently-area pb-95">
                            <h2 class="heading mb-0"><?php echo $dataRow->category; ?></h2>                           
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="frequently-item">
                                                <ul>
                                                    <li class="has-sub active">
                                                        <a href="javascript:void(0)"><b><?php echo $dataRow->question; ?></b>
                                                            <i class="lastudioicon lastudioicon-e-add"></i>
                                                        </a>
                                                        <ul class="frequently-body">
                                                            <li>
                                                            <?php echo $dataRow->answer; ?>
                                                            </li>
                                                        </ul>
                                                    </li>                                                 
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                
            </div>
        </main>



<?php $this->load->view('components/common/footer'); ?>
<?php $this->load->view('components/common/templateJs'); ?>
<?php $this->load->view('components/common/customJs'); ?>