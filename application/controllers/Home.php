<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class Home extends CI_Controller
{

  public function index()
  {
    $new = $this->product->newProducts('10');
    $men = $this->product->newProductsByType('Men');
    $women = $this->product->newProductsByType('Women');
    $kids = $this->product->newProductsByType('Kids');
    $sliderData = $this->common->getAllData('slider');
    $featureData = $this->common->getAllData('feature_image','3');
    
    $data = array(
      'newProducts' => $new,
      'menProducts' => $men,
      'womenProducts' => $women,
      'kidsProducts' => $kids,
      'sliderData' =>$sliderData,
      'featureData' => $featureData,
      'pageName' => "Home"
    );

    $this->common->template('home', $data);
  }
}