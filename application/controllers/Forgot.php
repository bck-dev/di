<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Forgot extends CI_Controller {

	public function index()
	{
    $this->common->template('forgot/enterEmail');
  }

  public function sendForgotMail(){

    if($_POST['email']){
      $email = $this->input->post('email');
      $user=$this->common->getByFeild('user', 'email', $email);

      //Load email library
      $this->load->library('email');

      //SMTP & mail configuration
      $config = array(
          'protocol'  => 'smtp',
          'smtp_host' => 'ssl://smtp.googlemail.com',
          'smtp_port' => 465,
          'smtp_user' => 'bckl.dev@gmail.com',
          'smtp_pass' => 'Bcklimited',
          'mailtype'  => 'html',
          'charset'   => 'utf-8'
      );
      $this->email->initialize($config);
      $this->email->set_mailtype("html");
      $this->email->set_newline("\r\n");

      //Email content
      $htmlContent = '<h1>Reset Password</h1>';
      $htmlContent .= '<p>Click the link below</p>';
      $htmlContent .= '<p><a href="'.base_url().'resetPassword/'.$user[0]->id.'/'.md5($user[0]->email).'">Reset</a></p>';
      
      $this->email->from('bckl.dev@gmail.com','Password Reset Email');
      $this->email->subject('Reset Password');
      $this->email->message($htmlContent);

      $this->email->to($email);
      $this->email->send();
      
      redirect('/checkMail');
    }else{
      echo "failed";
      redirect('forgot');
    }
  }

  public function checkMail(){
		$this->common->template('forgot/checkMail');
	}

  public function resetPassword($userId, $hash)
	{
    $data['user']=$this->common->getById('user', $userId);
    if(md5($data['user']->email)==$hash){
      $this->common->template('forgot/changePassword', $data);
    }
    else{
      redirect('forgot');
    }
  }

  public function updateNewPassword(){
    $id = $this->input->post('id');
    $hashPassword = password_hash($this->input->post('password'), PASSWORD_BCRYPT );
    $data['password'] = $hashPassword;
    $this->common->update('user', $id, $data);
    
    echo json_encode ("done");
  }

  public function resetSuccess(){
		$this->load->view('forgot/resetSuccess');
	}
  
}