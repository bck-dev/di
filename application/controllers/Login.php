<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{

    $data = array ('pageName' => "Login");
    $this->common->template('login', $data);
  }

  public function checkLogin()
	{
    $email = $this->input->post('email');
    $password = $this->input->post('password');
   
    $result = $this->user->userLogin($email, $password);

    if($result!=FALSE){
      $user=$this->common->getByFeild('user', 'email', $email);
      $sessionArray = array('userId' => $user[0]->id,
                            'userType' => $user[0]->userType,
                            'loggedIn' => TRUE );

      $this->session->set_userdata($sessionArray);
      
      if($user[0]->userType=="admin"){
        redirect ( 'admin' );
      }
      else{
        if($this->session->userdata('pendingCopyId')){
          redirect('sell/finish/'.$this->session->userdata('pendingCopyId'));
        }else{
          redirect ( 'profile' );
        }
      }
    }
    else{
      $data= array('message'=>'Incorrect email or password!' );
      
      $this->common->template('login', $data);
    }
  }

  public function logout(){
    $this->session->sess_destroy ();	
		redirect ( 'login' );
  }
}