<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class About extends CI_Controller
{

    public function index()
    {
        $allData = $this->common->getAllData('about');
       
        $data = [
            'pageName' => "About",
            'allData' => $allData,            
        ];

        $this->load->view('about', $data);
    }

}