<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class Faq extends CI_Controller
{

    public function index()
    {
        $allData = $this->common->getAllData('faqs');
        //$deleteCount = $this->data->onlyTrashed()->count();
        //$dropDownData = $this->faqModel->categoryDropdown();

        $data = [
            'pageName' => "Faq",
            'allData' => $allData,
            
        ];

        $this->load->view('faq', $data);
    }

}