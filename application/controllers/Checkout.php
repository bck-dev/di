<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class Checkout extends CI_Controller
{

    public function index()
    {
        if($this->session->userdata('tempCartId')){
             
            $deliveryData = $this->common->getAllData('delivery_fees');
            $cartData = $this->product->cart($this->session->userdata('tempCartId'));
            $cartTotal = $this->product->cartTotal($this->session->userdata('tempCartId'));

            $data = [
                'pageName' => "Checkout",
                'deliveryData' => $deliveryData,
                'cartData' => $cartData,
                'cartTotal' => $cartTotal
            ];

            $this->common->template('checkout', $data);
        }
        else{            
            redirect ( '/' );
        }

        
    }

    public function add()
    {      
        $order_no = $this->order->getOrderNo(); 
        $data = array(
            'first_name' => $_POST['first_name'],
            'last_name' => $_POST['last_name'],
            'email' => $_POST['email'],
            'telephone' => $_POST['telephone'],
            'address1' => $_POST['address1'],
            'address2' => $_POST['address2'],
            'city' => $_POST['city'],
            'delivery_fees' => $_POST['df'],
            'grand_total' => $_POST['grand_total'],
            'cart_total' => $_POST['cart_total'],
            'billing_address' => $_POST['billing_address'],
            'gift_email' => $_POST['gift_email'],
            'note' => $_POST['note'],
            'order_no' => $order_no,
            'order_time' => date('Y-m-d H:i:s'),            
            'reference' => $this->session->userdata('reference'),
            'deduction_amount' => $this->session->userdata('deduction_amount'),
        );

        $this->db->insert('orders', $data);

        $cart = $this->product->cart($this->session->userdata('tempCartId'));
        
        foreach ($cart as $item){
           
            $dumpCartData = array(
                'order_no' => $order_no,
                'productId' => $item->productId,
                'productOptionId' => $item->productOptionId,
                'price' => $item->price,
                'quantity' => $item->quantity,
                'discount' => $item->discount,
                'total' => $item->total,            
            );
            $this->db->insert('order_has_item', $dumpCartData);

            if ($item->productOptionId>0) {
                $productStockData=$this->common->getById('product_option', $item->productOptionId);
                $newStock = $productStockData->stock - $item->quantity;
                $newStockData = array('stock'=>$newStock);

                $this->common->update('product_option', $item->productOptionId, $newStockData);
            }
            else{
                for($i=1; $i<=$item->quantity; $i++){
                    $voucherInfo=$this->common->getById('voucher', $item->productId);
                    $validTill = date('Y-m-d', strtotime("+".$voucherInfo->valid_period."day"));
                    $voucherData = array(
                        'voucherNo' => $this->order->getVoucherNo(),
                        'amount' => $item->price,
                        'validTill' => $validTill
                    );

                    $this->db->insert('active_voucher', $voucherData);

                    $this->order->sendVoucherMail($voucherData['voucherNo'], $_POST['gift_email'], $_POST['email'], $_POST['first_name']);
                }                

            }
        }

        if($this->session->userdata('deduction_amount')>0){
            $this->common->deleteByFeild('active_voucher', 'voucherNo', $this->session->userdata('reference'));
        }

        $this->common->deleteByFeild('temp_cart_item', 'tempCartId', $this->session->userdata('tempCartId'));
        $this->session->sess_destroy ();

        $this->order->sendOrderMail($data['order_no']);
        
        redirect('thankyou/'.$data['order_no']);
    }
    public function thankyou($order_no){
        
        $orderData = $this->common->getByFeildSingle('orders', 'order_no', $order_no);
        $orderItems = $this->order->orderItems($order_no);

        $data = [
            'pageName' => "Thank You",
            'orderData' => $orderData,
            'orderItems' => $orderItems
        ];

        $this->common->template('thankyou', $data);
    }
    
    public function getArea(){
   
        $city = $this->input->get('city');
        $data = $this->product->getArea($city);

        echo json_encode($data->fees);
      }

}