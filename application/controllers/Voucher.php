<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class Voucher extends CI_Controller
{

    public function index()
    {
        $allData = $this->common->getAllData('voucher');

        $data = [
            'pageName' => "Voucher",
            'vouchers' => $allData,
            
        ];

        $this->common->template('voucher', $data);
    }

}