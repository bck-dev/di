<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class Cart extends CI_Controller
{

    public function index()
    {
        if($this->session->userdata('tempCartId')){
             
            $allData = $this->product->cart($this->session->userdata('tempCartId'));
            $cartTotal = $this->product->cartTotal($this->session->userdata('tempCartId'));
            
            $data = [
                'pageName' => "Cart",
                'allData' => $allData, 
                'cartTotal' => $cartTotal
            ]; 
                
            $this->common->template('cart', $data);
        }
        else{            
            redirect ( '/' );
        }

        
    }

    public function add(){
        if($this->session->userdata('tempCartId')){
            $tempCartId=$this->session->userdata('tempCartId');
        }
        else{
            
            $tempCartId=$this->product->generateTempCartId();
            $sessionArray = array('tempCartId' => $tempCartId );

            $this->session->set_userdata($sessionArray);
        }


        $productData = $this->common->getById('product_option', $_POST['productOption']);

        $data = [
            'tempCartId' => $tempCartId,
            'productId' => $productData->productId,
            'productOptionId' => $_POST['productOption'],
            'quantity' => $_POST['qty'],
            'price' => $productData->price,
            'discount' => $productData->discount,
            'total' => $productData->price * ((100 - $productData->discount) /100) * ($_POST['qty'])
        ];

        $this->db->insert('temp_cart_item', $data);
        
        redirect ( 'cart' );
    }

    public function addVoucher(){
        if($this->session->userdata('tempCartId')){
            $tempCartId=$this->session->userdata('tempCartId');
        }
        else{
            
            $tempCartId=$this->product->generateTempCartId();
            $sessionArray = array('tempCartId' => $tempCartId );

            $this->session->set_userdata($sessionArray);
        }


        $data = [
            'tempCartId' => $tempCartId,
            'productId' => $_POST['voucherId'],
            'productOptionId' => 0,
            'quantity' => $_POST['qty'],
            'price' => $_POST['amount'],
            'total' => $_POST['amount'] * $_POST['qty']
        ];

        $this->db->insert('temp_cart_item', $data);
        
        redirect ( 'cart' );
    }

    public function delete($id)
    {

        $this->common->delete('temp_cart_item', $id);

        redirect ( 'cart' );
    }
   
   public function updateCart(){

    $qty = $this->input->get('qty');
    $id = $this->input->get('id');

    $cartItemInfo = $this->common->getById('temp_cart_item', $id);

    $total = $qty*(($cartItemInfo->price/100)*(100-$cartItemInfo->discount));
    
    $data = array(
        'quantity' => $qty,
        'total' => $total
    );

    $this->common->update('temp_cart_item', $id, $data);
    $cartTotal = $this->product->cartTotal($this->session->userdata('tempCartId'));
    $output['amount']=$total;
    $output['cartTotal']=$cartTotal;
    echo json_encode($output);
   }

   public function redeem(){

    $reference = $this->input->get('reference');
    $validData = $this->order->validateDeduction($reference);

    $output['amount']=$validData->amount;

    if($validData->amount > 0 ){
        $sessionArray = array('deduction_amount' => $validData->amount,
                            'reference' => $reference
                        );

        $this->session->set_userdata($sessionArray);
        $output['msg']="Voucher Activated.";
    }else
    {
        $sessionArray = array('deduction_amount' => 0,
                            'reference' => ''
                        );

        $this->session->set_userdata($sessionArray);
        $output['msg']="Invalid or Expired Voucher.";  
    }

    echo json_encode($output);
   }
    

}