<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class Register extends CI_Controller {

	public function index()
	{ 
		$this->common->template('register/register');
	}

	public function email(){
		$output=[];
		if($this->common->getCount('user', 'email', $_POST['email'])>0){
			$output['cls']="error";
			$output['msg']="E-mail Already Exist!";
		}
		else{
			$output['cls']="success";
			$output['msg']="E-mail Available!";
		}
		echo json_encode ($output);
  	}

  	public function save(){
    	$hashPassword = password_hash($this->input->post('password'), PASSWORD_BCRYPT );
    
		$data = array(
			'email' => $this->input->post('email'),
			'password' => $hashPassword,
			'userType' => "user",
			'status' => 1
		);

		$this->db->insert('user', $data);

		$profileData = array(
			'telephone' => $this->input->post('contact'),
			'userId' => $this->db->insert_id()
		);

		$this->db->insert('profile', $profileData);

		echo json_encode ("done");
	}

	public function regMsg(){
		$this->common->template('register/regimsg');
	}
}