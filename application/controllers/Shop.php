<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class Shop extends CI_Controller
{

  public function index()
  { 
    $products = $this->product->filter();
    $categoryData = $this->categoryModel->allCategories();
    $colorData = $this->common->getAllData('color');
    $sizeData = $this->common->getAllData('size');
    $labelData = $this->common->getAllData('label');
    $min = $this->product->minPrice();
    $max = $this->product->maxPrice();

    $data = array(
      'products' => $products,      
      'categoryData' => $categoryData,
      'min' => $min,
      'max' => $max,
      'colorData' => $colorData,
      'sizeData' => $sizeData,
      'labelData' => $labelData,
      'count' => count($products)

    );
    $data['totalProducts'] = $this->product->filterResultCount();   

    // echo "<pre>";
    // var_dump($data['pagination']);
    // echo "</pre>";
    $this->common->template('shop', $data);
    //$this->load->view('shop', $data);
  }

  public function search()
  {
    $products = $this->product->filter('','','','','','','',$_GET['search']);
    $categoryData = $this->categoryModel->allCategories();
    $colorData = $this->common->getAllData('color');
    $sizeData = $this->common->getAllData('size');
    $labelData = $this->common->getAllData('label');
    $min = $this->product->minPrice();
    $max = $this->product->maxPrice();

    $data = array(
      'products' => $products,
      'categoryData' => $categoryData,
      'min' => $min,
      'max' => $max,
      'colorData' => $colorData,
      'sizeData' => $sizeData,
      'labelData' => $labelData,
      'count' => count($products),
      'search' => $_GET['search']
    );
    $data['totalProducts'] = $this->product->filterResultCount('','','','','','',$_GET['search']);

    $this->common->template('shop', $data);
  } 

  public function type($type)
  {
    $products = $this->product->filter($type);
    $categoryData = $this->categoryModel->allCategories();
    $colorData = $this->common->getAllData('color');
    $sizeData = $this->common->getAllData('size');
    $labelData = $this->common->getAllData('label');
    $min = $this->product->minPrice();
    $max = $this->product->maxPrice();

    $data = array(
      'products' => $products,
      'categoryData' => $categoryData,
      'min' => $min,
      'max' => $max,
      'colorData' => $colorData,
      'sizeData' => $sizeData,
      'labelData' => $labelData,
      'count' => count($products)

    );
    $data['totalProducts'] = $this->product->filterResultCount($type);

    $this->common->template('shop', $data);
  }

  public function category($type, $category)
  {
    $products = $this->product->filter($type, $category);
    $categoryData = $this->categoryModel->allCategories();
    $colorData = $this->common->getAllData('color');
    $sizeData = $this->common->getAllData('size');
    $labelData = $this->common->getAllData('label');
    $min = $this->product->minPrice();
    $max = $this->product->maxPrice();

    $data = array(
      'products' => $products,
      'categoryData' => $categoryData,
      'min' => $min,
      'max' => $max,
      'colorData' => $colorData,
      'sizeData' => $sizeData,
      'labelData' => $labelData,
      'count' => count($products)

    );
    $data['totalProducts'] = $this->product->filterResultCount($type, $category);

    $this->common->template('shop', $data);
  } 

  public function view($productId)
  {
    $product = $this->product->productFullDetail($productId);

    $data = array(
      'product' => $product,
    );
    
    $this->common->template('product', $data);
  }

  public function filterProducts()
  {
    $type = $this->input->get('type');
    $category = $this->input->get('category');
    $size = $this->input->get('size');
    $color = $this->input->get('color');
    $min = $this->input->get('min');
    $max = $this->input->get('max');

    if ($this->input->get('pageNo')) {
      $pageNo = $this->input->get('pageNo');
    } else {
      $pageNo = 1;
    }

    $data['totalProducts'] = $this->product->filterResultCount($type, $category, $color, $size, $min, $max);

    $data['products'] = $this->product->filter($type, $category, $color, $size, $min, $max, $pageNo);
    
    $pagination =$this->common->pagination($data['totalProducts'], $pageNo);
   
    $data['pagination'] = $pagination;  

    echo json_encode($data);
  }

  public function getColors(){
   
    $size = $this->input->get('size');
    $productId = $this->input->get('productId');
    $data = $this->product->getColors($productId, $size );   
      
    echo json_encode($data);
  }

  public function getPrice(){
   
    $size = $this->input->get('size');
    $color = $this->input->get('color');
    $productId = $this->input->get('productId');
    $data = $this->product->getPrice($productId, $size, $color);   
      
    echo json_encode($data);
  }
}