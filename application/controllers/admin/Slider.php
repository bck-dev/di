<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Slider extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('slider');
        
        $data = [
            'pageName' => "Slider",
            'action'  => 'add',
            'allData' => $allData,
            
        ];

        $this->load->view('dashboard/slider', $data);
    }

    public function add()
    {    
       // if (!empty($_FILES['image1']['image2']['image3'])) {   
        $image = $this->common->upload('image'); 
        $image1 = $this->common->upload('image1'); 
        $image2 = $this->common->upload('image2');
            
        $data = array(
            'title' => $_POST['title'],
            'sub_title' => $_POST['sub_title'],
            'buttn_text' => $_POST['buttn_text'],
            'buttn_link' => $_POST['buttn_link'],
            'image' => $image,
            'image1' => $image1,
            'image2' => $image2
        );
        $this->db->insert('slider', $data); //}   
       
        $allData = $this->common->getAllData('slider');

        $pageData = [
            'pageName' => "Slider",
            'action'  => 'add',
            'allData' => $allData,            
        ];
//var_dump($data);
        $this->load->view('dashboard/slider', $pageData);
    }

    public function delete($id)
    {       
        $this->common->delete('slider', $id);
        $allData = $this->common->getAllData('slider');
        $this->session->set_flashdata('success', 'Your slider has been deleted successfully');        
        
        $pageData = [
            'pageName' => "Slider",
            'allData' => $allData,
            'action'  => 'add',
        ];

        $this->load->view('dashboard/slider', $pageData);
    }

    public function loadUpdate($id)
    {
        $updateData = $this->common->getById('slider', $id);
        $allData = $this->common->getAllData('slider');
        
        $pageData = [
            'pageName' => "Slider",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
        ];

        $this->load->view('dashboard/slider', $pageData);
    }

    public function update($id)
    {
        //if (!empty($_FILES['image1'])) {
        $image = $this->common->upload('image');   
        $image1 = $this->common->upload('image1');
        $image2 = $this->common->upload('image2');
        $data = array(
            'title' => $_POST['title'],
            'sub_title' => $_POST['sub_title'],
            'buttn_text' => $_POST['buttn_text'],
            'buttn_link' => $_POST['buttn_link'],
            'image' => $image,
            'image1' => $image1,
            'image2' => $image2,
        );

        $this->common->update('slider', $id, $data);
    //}
    
        $allData = $this->common->getAllData('slider');
        
        $pageData = [
            'pageName' => "Slider",
            'allData' => $allData,
            'action'  => 'update',
            
        ];

        $this->load->view('dashboard/slider', $pageData);
    }

    
}