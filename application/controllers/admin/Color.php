<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Color extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('color');

        $data = [
            'pageName' => "Color",
            'action'  => 'add',
            'allData' => $allData
        ];

        $this->load->view('dashboard/color', $data);
    }

    public function add()
    {

        $data = array(
            'color' => $_POST['color'],
            'code' => $_POST['code']
        );

        $this->db->insert('color', $data);

        $allData = $this->common->getAllData('color');

        $pageData = [
            'pageName' => "Color",
            'action'  => 'add',
            'allData' => $allData,
        ];

        $this->load->view('dashboard/color', $pageData);
    }

    public function delete($id)
    {

        $this->common->delete('color', $id);

        $allData = $this->common->getAllData('color');

        $pageData = [
            'pageName' => "Color",
            'allData' => $allData,
            'action'  => 'add'
        ];

        $this->load->view('dashboard/color', $pageData);
    }

    public function loadUpdate($id)
    {

        $updateData = $this->common->getById('color', $id);

        $allData = $this->common->getAllData('color');

        $pageData = [
            'pageName' => "Color",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
        ];

        $this->load->view('dashboard/color', $pageData);
    }

    public function update($id)
    {

        $data = array(
            'color' => $_POST['color'],
            'code' => $_POST['code']
        );

        $this->common->update('color', $id, $data);
        $allData = $this->common->getAllData('color');

        $pageData = [
            'pageName' => "Color",
            'allData' => $allData,
            'action'  => 'add'
        ];

        $this->load->view('dashboard/color', $pageData);
    }
}