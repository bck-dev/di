<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Size extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('size');

        $data = [
            'pageName' => "Size",
            'action'  => 'add',
            'allData' => $allData
        ];

        $this->load->view('dashboard/size', $data);
    }

    public function add()
    {

        $data = array(
            'size' => $_POST['size']
        );

        $this->db->insert('size', $data);

        $allData = $this->common->getAllData('size');

        $pageData = [
            'pageName' => "Size",
            'action'  => 'add',
            'allData' => $allData,
        ];

        $this->load->view('dashboard/size', $pageData);
    }

    public function delete($id)
    {

        $this->common->delete('size', $id);

        $allData = $this->common->getAllData('size');

        $pageData = [
            'pageName' => "Size",
            'allData' => $allData,
            'action'  => 'add'
        ];

        $this->load->view('dashboard/size', $pageData);
    }

    public function loadUpdate($id)
    {

        $updateData = $this->common->getById('size', $id);

        $allData = $this->common->getAllData('size');

        $pageData = [
            'pageName' => "Size",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
        ];

        $this->load->view('dashboard/size', $pageData);
    }

    public function update($id)
    {

        $data = array(
            'size' => $_POST['size']
        );

        $this->common->update('size', $id, $data);
        $allData = $this->common->getAllData('size');

        $pageData = [
            'pageName' => "Size",
            'allData' => $allData,
            'action'  => 'add'
        ];

        $this->load->view('dashboard/size', $pageData);
    }
}