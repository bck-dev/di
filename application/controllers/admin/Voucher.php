<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Voucher extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('voucher');
        
        $data = [
            'pageName' => "Voucher",
            'action'  => 'add',
            'allData' => $allData,
            
        ];

        $this->load->view('dashboard/voucher', $data);
    }

    public function add()
    {    
        if (!empty($_FILES['image'])) {   
        $voucher_images = $this->common->upload('image');        
            
        $data = array(
            'name' => $_POST['name'],
            'amount' => $_POST['amount'],
            'valid_period' => $_POST['valid_period'],
            'description' => $_POST['description'],
            'image' => $voucher_images
        );
        $this->db->insert('voucher', $data); }   
       
        $allData = $this->common->getAllData('voucher');

        $pageData = [
            'pageName' => "Voucher",
            'action'  => 'add',
            'allData' => $allData,            
        ];

        $this->load->view('dashboard/voucher', $pageData);
    }

    public function delete($id)
    {       
        $this->common->delete('voucher', $id);
        $allData = $this->common->getAllData('voucher');
        $this->session->set_flashdata('success', 'Your Voucher has been deleted successfully');        
        
        $pageData = [
            'pageName' => "Voucher",
            'allData' => $allData,
            'action'  => 'add',
        ];

        $this->load->view('dashboard/voucher', $pageData);
    }

    public function loadUpdate($id)
    {
        $updateData = $this->common->getById('voucher', $id);
        $allData = $this->common->getAllData('voucher');
        
        $pageData = [
            'pageName' => "Voucher",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
        ];

        $this->load->view('dashboard/voucher', $pageData);
    }

    public function update($id)
    {
        if (!empty($_FILES['image'])) {   
        $voucher_images = $this->common->upload('image');
        $data = array(
            'name' => $_POST['name'],
            'amount' => $_POST['amount'],
            'valid_period' => $_POST['valid_period'],
            'description' => $_POST['description'],
            'image' => $voucher_images
        );

        $this->common->update('voucher', $id, $data);}
    
        $allData = $this->common->getAllData('voucher');
        
        $pageData = [
            'pageName' => "Voucher",
            'allData' => $allData,
            'action'  => 'update',
            
        ];

        $this->load->view('dashboard/voucher', $pageData);
    }

    
}