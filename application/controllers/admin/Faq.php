<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Faq extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('faqs');
        
        $data = [
            'pageName' => "Faq",
            'action'  => 'add',
            'allData' => $allData,
            
        ];

        $this->load->view('dashboard/faq', $data);
    }

    public function add()
    {       
        $data = array(
            'category' => $_POST['category'],
            'question' => $_POST['question'],
            'answer' => $_POST['answer']
        );

        $this->db->insert('faqs', $data);
        //var_dump($data);

        $allData = $this->common->getAllData('faqs');

        $pageData = [
            'pageName' => "Faq",
            'action'  => 'add',
            'allData' => $allData,            
        ];

        $this->load->view('dashboard/faq', $pageData);
    }

    public function delete($id)
    {       
        $this->common->delete('faqs', $id);
        $allData = $this->common->getAllData('faqs');
        $this->session->set_flashdata('success', 'Your Question has been deleted successfully');        
        
        $pageData = [
            'pageName' => "Faq",
            'allData' => $allData,
            'action'  => 'add',
        ];

        $this->load->view('dashboard/faq', $pageData);
    }

    public function loadUpdate($id)
    {
        $updateData = $this->common->getById('faqs', $id);
        $allData = $this->common->getAllData('faqs');
        
        $pageData = [
            'pageName' => "Faq",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
        ];

        $this->load->view('dashboard/faq', $pageData);
    }

    public function update($id)
    {
        $data = array(
            'category' => $_POST['category'],
            'question' => $_POST['question'],
            'answer' => $_POST['answer']
        );

        $this->common->update('faqs', $id, $data);
        $allData = $this->common->getAllData('faqs');
        
        $pageData = [
            'pageName' => "Faq",
            'allData' => $allData,
            'action'  => 'update',
            
        ];

        $this->load->view('dashboard/faq', $pageData);
    }

    
}