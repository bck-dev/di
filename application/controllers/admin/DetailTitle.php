<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DetailTitle extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('detail_title');

        $data = [
            'pageName' => "DetailTitle",
            'action'  => 'add',
            'allData' => $allData
        ];

        $this->load->view('dashboard/detailTitle', $data);
    }

    public function add()
    {

        $data = array(
            'detailTitle' => $_POST['detailTitle']
        );

        $this->db->insert('detail_title', $data);

        $allData = $this->common->getAllData('detail_title');

        $pageData = [
            'pageName' => "DetailTitle",
            'action'  => 'add',
            'allData' => $allData,
        ];

        $this->load->view('dashboard/detailTitle', $pageData);
    }

    public function delete($id)
    {

        $this->common->delete('detail_title', $id);

        $allData = $this->common->getAllData('detail_title');

        $pageData = [
            'pageName' => "DetailTitle",
            'allData' => $allData,
            'action'  => 'add'
        ];

        $this->load->view('dashboard/detailTitle', $pageData);
    }

    public function loadUpdate($id)
    {

        $updateData = $this->common->getById('detail_title', $id);

        $allData = $this->common->getAllData('detail_title');

        $pageData = [
            'pageName' => "DetailTitle",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
        ];

        $this->load->view('dashboard/detailTitle', $pageData);
    }

    public function update($id)
    {

        $data = array(
            'detailTitle' => $_POST['detailTitle']
        );

        $this->common->update('detail_title', $id, $data);
        $allData = $this->common->getAllData('detail_title');

        $pageData = [
            'pageName' => "DetailTitle",
            'allData' => $allData,
            'action'  => 'add'
        ];

        $this->load->view('dashboard/detailTitle', $pageData);
    }
}