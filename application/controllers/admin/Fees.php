<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Fees extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('delivery_fees');
        
        $data = array(
            'area' => $_POST['area'],
            'duration' => $_POST['duration'],
            'fees' => $_POST['fees'],
            'allData' => $allData,
            'action'  => 'add',
        );

        $this->load->view('dashboard/deliveryFees', $data);
    }

    public function add()
    {       
        $data = array(
            'area' => $_POST['area'],
            'duration' => $_POST['duration'],
            'fees' => $_POST['fees']
        );

        $this->db->insert('delivery_fees',$data);
        $allData = $this->common->getAllData('delivery_fees');
        
        $pageData = [
            'pageName' => "Delivery Fees",
            'allData' => $allData,
            'action'  => 'add',
            
        ];

        $this->load->view('dashboard/deliveryFees', $pageData);
    }

    public function delete($id)
    {       
        $this->common->delete('delivery_fees', $id);
        $allData = $this->common->getAllData('delivery_fees');
        $this->session->set_flashdata('success', 'Your Question has been deleted successfully');        
        
        $pageData = [
            'pageName' => "Delivery Fees",
            'allData' => $allData,
            'action'  => 'add',
        ];

        $this->load->view('dashboard/deliveryFees', $pageData);
    }

    public function loadUpdate($id)
    {
        $updateData = $this->common->getById('delivery_fees', $id);
        $allData = $this->common->getAllData('delivery_fees');
        
        $pageData = [
            'pageName' => "Delivery Fees",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
        ];

        $this->load->view('dashboard/deliveryFees', $pageData);
    }

    public function update($id)
    {
        $data = array(
            'area' => $_POST['area'],
            'duration' => $_POST['duration'],
            'fees' => $_POST['fees']
        );

        $this->common->update('delivery_fees', $id, $data);
        $allData = $this->common->getAllData('delivery_fees');
        
        $pageData = [
            'pageName' => "Delivery Fees",
            'allData' => $allData,
            'action'  => 'update',
            
        ];

        $this->load->view('dashboard/deliveryFees', $pageData);
    }

    
}