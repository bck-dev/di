<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class OrderReport extends BaseController {

  public function __construct()
  {
      parent::__construct();
      $this->isLoggedIn($this->session->userdata('loggedIn'));      
      $this->isAdmin($this->session->userdata('userType'));   
  }

  public function index()
  {
    $orders = $this->common->getAllData('orders');
    foreach($orders as $order){
      $order->items = $this->order->orderItems($order->order_no);
    }

    $data = [
      'orders' => $orders,      
    ];
    
    $this->load->view('dashboard/order', $data);

  }

  public function view($order_no)
  {
    $orderData = $this->common->getByFeildSingle('orders', 'order_no', $order_no);
    $orderItems = $this->order->orderItems($order_no);

    $data = [
        'orderData' => $orderData,
        'orderItems' => $orderItems
    ];
    
    $this->load->view('dashboard/singleOrder', $data);

  }

  public function changeStatus($order_no, $status)
  {
    if($status<3){
      
      $data = array (
        'order_status' => $status+1,
      );

      $condition = array (
        'order_no' => $order_no,
      );

      $this->common->updateMultipleCondition('orders',$condition, $data);
    }
    
    redirect('admin/order/view/'.$order_no);

  }

  

} 

?>