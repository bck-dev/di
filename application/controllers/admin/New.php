<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Category extends BaseController {

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));      
        $this->isAdmin($this->session->userdata('userType'));      
    }

    public function index()
    {
        $allData = $this->common->getAllData('category');
        $dropDownData = $this->categoryModel->categoryDropdown();

        $data=['pageName'=>"Categories",
                'action'  => 'add',
                'allData' => $allData, 
                'dropDown' => $dropDownData
                ];

        $this->load->view('dashboard/category',$data);

    }

    public function add()
    {
        if($_POST['parent']=="None"){
            $parent="";
            $level=0;
        }
        else{
            $parentCategoryInfo= $this->common->getByFeildSingle('category', 'categoryName', $_POST['parent']);
            $level=$parentCategoryInfo->level+1;
            $parent=$_POST['parent'];
        }


        $data = array
        (
            'categoryName' => $_POST['categoryName'],
            'parent' => $parent,
            'level' => $level
        );  

        $this->db->insert('category', $data);

        $allData = $this->common->getAllData('category');

        $pageData=['pageName'=>"Categories",
                    'action'  => 'add',
                    'allData' => $allData, 
        ];

        $this->load->view('dashboard/category',$pageData);

    }

    public function delete($id)
    {

        $this->common->delete('category', $id);
    
        $allData = $this->common->getAllData('category');
    
        $pageData=['pageName'=>"Categories",
                'allData' => $allData,
                'action'  => 'add'
            ];
            
        $this->load->view('dashboard/category', $pageData); 
    
    }
    
    public function loadUpdate($id){
    
        $updateData = $this->common->getById('category',$id);
    
        $allData = $this->common->getAllData('category');
    
        $pageData=['pageName'=>"Categories",
                    'allData' => $allData,
                    'updateData'  => $updateData,
                    'action'  => 'update',
                ];
    
        $this->load->view('dashboard/category', $pageData);
    
    }
    
    public function update($id){
        
        $data = array
        (
            'categoryName' => $_POST['categoryName'],
            'parent' => $_POST['parent'],
            //'subCategory' => $_POST['subCategory']
        ); 
    
        $this->common->update('category', $id, $data);
        $allData = $this->common->getAllData('category');
    
        $pageData=['pageName'=>"Categories",
            'allData' => $allData,
            'action'  => 'add'
        ];
    
        $this->load->view('dashboard/category', $pageData);
    
    }


} 

?>