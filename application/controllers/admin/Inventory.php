<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Inventory extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $products = $this->product->allProducts();

        $data = [
            'pageName' => "Inventory",
            'action'  => 'add',
            'products' => $products
        ];

        $this->load->view('dashboard/inventory/list', $data);
    }

    public function new()
    {

        $dropDownData = $this->categoryModel->categoryDropdown();
        $sizes = $this->common->getAllData('size');
        $colors = $this->common->getAllData('color');
        $materials = $this->common->getAllData('material');
        $labels = $this->common->getAllData('label');

        $data = [
            'pageName' => "Product",
            'action'  => 'add',
            'dropDown' => $dropDownData,
            'sizes' => $sizes,
            'colors' => $colors,
            'materials' => $materials,
            'labels' => $labels
        ];

        $this->load->view('dashboard/inventory/new', $data);
    }

    public function add()
    {
        $categoryInfo = $this->common->getByFeildSingle('category', 'categoryName', $_POST['category']);

        $data = array(
            'categoryId' => $categoryInfo->id,
            'productName' => $_POST['productName'],
            'material' => $_POST['material'],
            'description' => $_POST['description'],
            'type' => $_POST['type']
        );

        $this->db->insert('product', $data);

        $productId = $this->db->insert_id();

        foreach ($_POST['size'] as $size) {
            $sizeInfo = $this->common->getByFeildSingle('size', 'size', $size);

            $sizeData = array(
                'productId' => $productId,
                'sizeId' => $sizeInfo->id
            );

            $this->db->insert('product_has_size', $sizeData);

            foreach ($_POST['color'] as $color) {
                $colorInfo = $this->common->getByFeildSingle('color', 'color', $color);

                $productOptionData = array(
                    'productId' => $productId,
                    'colorId' => $colorInfo->id,
                    'sizeId' => $sizeInfo->id
                );

                $this->db->insert('product_option', $productOptionData);
            }
        }

        foreach ($_POST['color'] as $color) {
            $colorInfo = $this->common->getByFeildSingle('color', 'color', $color);

            $colorData = array(
                'productId' => $productId,
                'colorId' => $colorInfo->id
            );

            $this->db->insert('product_has_color', $colorData);
        }

        foreach ($_POST['label'] as $label) {
            $LabelInfo = $this->common->getByFeildSingle('label', 'label', $label);

            $labelData = array(
                'productId' => $productId,
                'labelId' => $LabelInfo->id
            );

            $this->db->insert('product_has_label', $labelData);
        }

        redirect('admin/inventory/stock/' . $productId);
    }

    public function loadUpdateBasic($productId)
    {
        $dropDownData = $this->categoryModel->categoryDropdown();
        $sizes = $this->common->getAllData('size');
        $colors = $this->common->getAllData('color');        
        $materials = $this->common->getAllData('material');
        $labels = $this->common->getAllData('label');        
        $updateData = $this->common->getById('product', $productId);
        $x = $this->common->getById('category', $updateData->categoryId);
        $productColors = $this->product->productColors($productId);
        $ProductSizes = $this->product->ProductSizes($productId);        
        $labelsData = $this->product->productLabels($productId);

        $data = [
            'pageName' => "Product",
            'action'  => "update",
            'dropDown' => $dropDownData,           
            'x' => $x, 
            'sizes' => $sizes,
            'colors' => $colors,
            'materials' => $materials,
            'labels' => $labels,
            'updateData' => $updateData,
            'ProductSizes'=> $ProductSizes,
            'productColors' =>$productColors,
            'labelsData' =>$labelsData
        ];
        //var_dump($data);
        $this->load->view('dashboard/inventory/new', $data);
    }

    public function updateBasic($productId)
    {
        $categoryInfo = $this->common->getByFeildSingle('category', 'categoryName', $_POST['category']);

        $data = array(
            'categoryId' => $categoryInfo->id,
            'productName' => $_POST['productName'],
            'material' => $_POST['material'],
            'description' => $_POST['description'],
            'type' => $_POST['type']
        );

        $this->common->update('product', $productId, $data);

        $this->common->deleteByFeild('product_has_size', 'productId', $productId);
        $this->common->deleteByFeild('product_has_color', 'productId', $productId);
        $this->common->deleteByFeild('product_has_label', 'productId', $productId);
        $this->common->temporaryDumbing('product_option', 'temp_old_product_option', 'productId', $productId);
        $this->common->deleteByFeild('product_option', 'productId', $productId);

        foreach ($_POST['color'] as $color) {
            $colorInfo = $this->common->getByFeildSingle('color', 'color', $color);

            $colorData = array(
                'productId' => $productId,
                'colorId' => $colorInfo->id
            );

            $this->db->insert('product_has_color', $colorData);
        }

        foreach ($_POST['size'] as $size) {
            $sizeInfo = $this->common->getByFeildSingle('size', 'size', $size);

            $sizeData = array(
                'productId' => $productId,
                'sizeId' => $sizeInfo->id
            );

            $this->db->insert('product_has_size', $sizeData);

            foreach ($_POST['color'] as $color) {
                $colorInfo = $this->common->getByFeildSingle('color', 'color', $color);

                $productOptionData = array(
                    'productId' => $productId,
                    'colorId' => $colorInfo->id,
                    'sizeId' => $sizeInfo->id
                );

                $this->db->insert('product_option', $productOptionData);
            }
        }

        $productOptions = $this->common->getByFeild('product_option', 'productId', $productId);

        foreach ($productOptions as $opt) {
            $tempData = $this->product->getTempProductOption($productId, $opt->colorId, $opt->sizeId);

            if ($tempData) {
                $newData['price'] = $tempData->price;
                $newData['stock'] = $tempData->stock;
                $newData['discount'] = $tempData->discount;
                $newData['stockUpdatedDate'] = $tempData->stockUpdatedDate;

                $conditions = array(
                    'productId' => $productId,
                    'colorId' => $opt->colorId,
                    'sizeId' => $opt->sizeId
                );

                $this->common->updateMultipleCondition('product_option', $conditions, $newData);
            }
        }

        $this->common->deleteByFeild('temp_old_product_option', 'productId', $productId);

        if ($_POST['label']) {
            foreach ($_POST['label'] as $label) {
                $labelInfo = $this->common->getByFeildSingle('label', 'label', $label);

                $labelData = array(
                    'productId' => $productId,
                    'labelId' => $labelInfo->id
                );

                $this->db->insert('product_has_label', $labelData);
            }
        }

        redirect('admin/inventory/stock/' . $productId);
    }


    public function stock($productId)
    {
        $sizes = $this->product->productSizes($productId);
        $colors = $this->product->productColors($productId);
        $labels = $this->product->productLabels($productId);
        $product = $this->product->productBasicDetail($productId);
        $productOptions = $this->product->productOptions($productId);

        $data = [
            'pageName' => "Stock",
            'sizes' => $sizes,
            'colors' => $colors,
            'labels' => $labels,
            'product' => $product,
            'productOptions' => $productOptions
        ];

        $this->load->view('dashboard/inventory/stock', $data);
    }


    public function view($productId)
    {
        $sizes = $this->product->productSizes($productId);
        $colors = $this->product->productColors($productId);
        $labels = $this->product->productLabels($productId);
        $product = $this->product->productBasicDetail($productId);
        $productOptions = $this->product->productOptions($productId);
        $productImages = $this->product->productImages($productId);
        $productDetails = $this->product->productDetails($productId);

        $data = [
            'pageName' => "Stock",
            'sizes' => $sizes,
            'colors' => $colors,
            'product' => $product,
            'labels' => $labels,
            'productOptions' => $productOptions,
            'productImages' => $productImages,
            'productDetails' => $productDetails
        ];

        $this->load->view('dashboard/inventory/view', $data);
    }

    public function updateStock($productId)
    {
        $i = 1;
        while ($i <= $_POST['count']) {
            $priceInputName = 'price' . $i;
            $stockInputName = 'stock' . $i;
            $optionInputName = 'optId' . $i;
            $stockUpdatedDate = date('Y-m-d');

            $optionData = array(
                'price' => $_POST[$priceInputName],
                'stock' => $_POST[$stockInputName],
                'stockUpdatedDate' => $stockUpdatedDate,
            );

            $this->common->update('product_option', $_POST[$optionInputName], $optionData);

            $i++;
        }

        $product = $this->product->productBasicDetail($productId);

        if ($product->coverImage) {
            redirect('admin/inventory/view/' . $productId);
        } else {
            redirect('admin/inventory/image/' . $productId);
        }
    }

    public function image($productId)
    {
        $sizes = $this->product->productSizes($productId);
        $colors = $this->product->productColors($productId);
        $labels = $this->product->productLabels($productId);
        $product = $this->product->productBasicDetail($productId);
        $productImages = $this->product->productImages($productId);

        $data = [
            'pageName' => "Images",
            'sizes' => $sizes,
            'colors' => $colors,
            'labels' => $labels,
            'product' => $product,
            'productImages' => $productImages
        ];

        $this->load->view('dashboard/inventory/image', $data);
    }

    public function updateImage($productId)
    {
        $product = $this->product->productBasicDetail($productId);

        if (!empty($_FILES['coverImage']['name'])) {
            $coverImage = $this->common->upload('coverImage');

            $coverData = array(
                'coverImage' => $coverImage,
            );

            $this->common->update('product', $productId, $coverData);
        }

        $colors = $this->product->productColors($productId);

        foreach ($colors as $color) {
            $filename = 'color' . $color->colorId;
            echo $filename;
            if (!empty($_FILES[$filename]['name'])) {
                $colorImage = $this->common->upload($filename);

                $colorImageData = array(
                    'path' => $colorImage,
                    'productId' => $productId,
                    'colorId' => $color->colorId
                );

                $this->db->insert('product_has_image', $colorImageData);
            }
        }

        if ($product->coverImage) {
            redirect('admin/inventory/view/' . $productId);
        } else {
            redirect('admin/inventory/detail/' . $productId);
        }
    }

    public function detail($productId)
    {
        $sizes = $this->product->productSizes($productId);
        $colors = $this->product->productColors($productId);
        $labels = $this->product->productLabels($productId);
        $product = $this->product->productBasicDetail($productId);
        $productDetails = $this->product->productDetails($productId);
        $detailTitles = $this->common->getAllData('detail_title');


        $data = [
            'pageName' => "Images",
            'sizes' => $sizes,
            'colors' => $colors,
            'product' => $product,
            'labels' => $labels,
            'productDetails' => $productDetails,
            'detailTitles' => $detailTitles,
            'action' => "add"
        ];

        $this->load->view('dashboard/inventory/detail', $data);
    }

    public function addDetail($productId)
    {
        $detailData = array(
            'productId' => $productId,
            'title' => $_POST['title'],
            'content' => $_POST['content']
        );

        $this->db->insert('product_has_detail', $detailData);

        redirect('admin/inventory/detail/' . $productId);
    }

    public function loadUpdateDetail($productId, $id)
    {
        $sizes = $this->product->productSizes($productId);
        $colors = $this->product->productColors($productId);
        $product = $this->product->productBasicDetail($productId);
        $productDetails = $this->product->productDetails($productId);
        $detailTitles = $this->common->getAllData('detail_title');
        $updateData = $this->common->getById('product_has_detail', $id);


        $data = [
            'pageName' => "Images",
            'sizes' => $sizes,
            'colors' => $colors,
            'product' => $product,
            'productDetails' => $productDetails,
            'detailTitles' => $detailTitles,
            'action' => "update",
            'updateData' => $updateData
        ];

        $this->load->view('dashboard/inventory/detail', $data);
    }

    public function updateDetail($productId, $id)
    {
        $detailData = array(
            'title' => $_POST['title'],
            'content' => $_POST['content']
        );

        $this->common->update('product_has_detail', $id, $detailData);

        redirect('admin/inventory/detail/' . $productId);
    }

    public function deleteDetail($productId, $id)
    {
        $this->common->delete('product_has_detail', $id);

        redirect('admin/inventory/detail/' . $productId);
    }

    public function deleteImage()
    {
        $this->common->delete('product_has_image', $_POST['id']);

        echo json_encode("Successfully Deleted");
    }

    public function delete($id)
    {
        $this->common->delete('product', $id);
        $this->common->deleteByFeild('product_option', 'productId', $id);
        $this->common->deleteByFeild('product_has_image', 'productId', $id);
        $this->common->deleteByFeild('product_has_size', 'productId', $id);
        $this->common->deleteByFeild('product_has_color', 'productId', $id);
        $this->common->deleteByFeild('product_has_detail', 'productId', $id);

        redirect('admin/inventory');
    }

    public function discount()
    {
        $products = $this->product->productOrderCategory();
        $discountedproducts = $this->product->discountedProducts();

        $data = array(
            'products' => $products,
            'discountedproducts' => $discountedproducts
        );

        $this->load->view('dashboard/inventory/discount', $data);
    }

    public function addDiscount()
    {
        $data = array(
            'discount' => $_POST['percentage']
        );

        foreach ($_POST['products'] as $product) {
            $this->common->update('product', $product, $data);
        }

        redirect('admin/inventory/discount');
    }

    public function removeDiscount($productId)
    {
        $data = array(
            'discount' => '0'
        );

        $this->common->update('product', $productId, $data);

        redirect('admin/inventory/discount');
    }

    /*public function loadUpdate($id)
    {

        // $updateData = $this->common->getById('label', $id);

        // $allData = $this->common->getAllData('label');

        // $pageData = [
        //     'pageName' => "Label",
        //     'allData' => $allData,
        //     'updateData'  => $updateData,
        //     'action'  => 'update',
        // ];

        // $this->load->view('dashboard/label', $pageData);
    }

    public function update($id)
    {

        // $data = array(
        //     'label' => $_POST['label']
        // );

        // $this->common->update('label', $id, $data);
        // $allData = $this->common->getAllData('label');

        // $pageData = [
        //     'pageName' => "Label",
        //     'allData' => $allData,
        //     'action'  => 'add'
        // ];

        // $this->load->view('dashboard/label', $pageData);
    }*/
}