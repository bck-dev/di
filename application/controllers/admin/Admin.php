<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Admin extends BaseController {

  public function __construct()
  {
      parent::__construct();
      $this->isLoggedIn($this->session->userdata('loggedIn'));      
      $this->isAdmin($this->session->userdata('userType'));   
  }

  public function index()
  {
    $orders = $this->common->getDataWithLimit('orders', 10);
    foreach($orders as $order){
      $order->items = $this->order->orderItems($order->order_no);
    }
    $pendingCount = $this->common->getCount('orders','order_status', 0);
    $progressCount = $this->common->getCount('orders','order_status', 1);
    $routeCount = $this->common->getCount('orders','order_status', 2);    
    $deliverCount = $this->common->getCount('orders','order_status', 3);
    $customerCount = $this->order->getOrderCount();
    $totalCount = $this->order->totlaOrderCount();
    $totalSales = $this->order->totalSales();
    $data = [
      'orders' => $orders, 
      'pendingCount' => $pendingCount,
      'progressCount' => $progressCount,
      'routeCount' => $routeCount,
      'deliverCount' => $deliverCount,
      'totalCount' => $totalCount,
      'customerCount' => $customerCount,
      'totalSales' => $totalSales
    ];
    
    $this->load->view('dashboard/home', $data);

  }

} 

?>