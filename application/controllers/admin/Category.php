<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Category extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->categoryModel->allCategories();
        $dropDownData = $this->categoryModel->categoryDropdown();

        $data = [
            'pageName' => "Categories",
            'action'  => 'add',
            'allData' => $allData,
            'dropDown' => $dropDownData
        ];

        $this->load->view('dashboard/category', $data);
    }

    public function add()
    {

        if ($_POST['parent'] == "None") {
            $parent = "";
            $level = 0;
        } else {
            $parentCategoryInfo = $this->common->getById('category', $_POST['parent']);
            $level = $parentCategoryInfo->level + 1;
            $parent = $_POST['parent'];
        }


        $data = array(
            'categoryName' => $_POST['categoryName'],
            'parent' => $parent,
            'level' => $level
        );

        $this->db->insert('category', $data);

        $allData = $this->categoryModel->allCategories();
        $dropDownData = $this->categoryModel->categoryDropdown();

        $pageData = [
            'pageName' => "Categories",
            'action'  => 'add',
            'allData' => $allData,
            'dropDown' => $dropDownData
        ];

        $this->load->view('dashboard/category', $pageData);
    }

    public function delete($id)
    {
        if ($this->common->getCount('category', 'parent', $id) > 0) {

            $this->session->set_flashdata('error', 'Unable to delete the current data');
            // redirect('admin/category');

        } elseif ($this->common->getCount('product', 'categoryId', $id) > 0) {

            $this->session->set_flashdata('error', 'Unable to delete the current data');
        } else {

            $this->common->delete('category', $id);
            $this->session->set_flashdata('success', 'Your data has been deleted successfully');
        }

        $allData = $this->categoryModel->allCategories();
        $dropDownData = $this->categoryModel->categoryDropdown();

        $pageData = [
            'pageName' => "Categories",
            'allData' => $allData,
            'action'  => 'add',
            'dropDown' => $dropDownData,

        ];

        $this->load->view('dashboard/category', $pageData);
    }

    public function loadUpdate($id)
    {

        $updateData = $this->common->getById('category', $id);
        $a = $this->common->getById('category', $updateData->parent);
        //$x = $a->categoryName;
        //var_dump($a->categoryName);
        //$upD = $this->common->getById('category', $updateData->parent);

        $allData = $this->categoryModel->allCategories();
        $dropDownData = $this->categoryModel->categoryDropdown();

        $pageData = [
            'pageName' => "Categories",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
            'dropDown' => $dropDownData
        ];

        $this->load->view('dashboard/category', $pageData);
    }

    public function update($id)
    {
        $categoryInfo = $this->common->getById('category', $id);

        if ($_POST['parent'] == "None") {
            $parent = "";
            $level = 0;
        } else {
            $parent = $_POST['parent'];
            $parentCategoryInfo = $this->common->getById('category', $_POST['parent']);
            $level = $parentCategoryInfo->level + 1;
        }

        $data = array(
            'categoryName' => $_POST['categoryName'],
            'parent' => $parent,
            'level' => $level
        );

        $this->common->update('category', $id, $data);

        if ($_POST['parent'] != $categoryInfo->parent) {
            $this->categoryModel->categoryLevelUpdate($id, $level);
        }

        $allData = $this->categoryModel->allCategories();
        $dropDownData = $this->categoryModel->categoryDropdown();

        $pageData = [
            'pageName' => "Categories",
            'allData' => $allData,
            'action'  => 'add',
            'dropDown' => $dropDownData
        ];

        $this->load->view('dashboard/category', $pageData);
    }

    /*public function check_categoryName($categoryName){
        
        $this->form_validation->set_message('categoryName_exists', 'That catgory is taken. Please choose a different one');
        if($this->categoryModel->check_categoryName($categoryName)){
            return true;
        } else {
            return false;
        }
    }*/
}