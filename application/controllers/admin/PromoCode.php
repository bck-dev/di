<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PromoCode extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('promo_code');
        
        $data = [
            'pageName' => "PromoCode",
            'action'  => 'add',
            'allData' => $allData,            
        ];

        $this->load->view('dashboard/promocode', $data);
    }

    public function add()
    {       
        $data = array(
            'offertype' => $_POST['offertype'],
            'percentage' => $_POST['percentage'],
            'start_date'=> $_POST['start_date'],
            'end_date'=> $_POST['end_date'],            
            'promo_code'=> $_POST['promo_code']
        );

        $this->db->insert('promo_code', $data);
        
        $allData = $this->common->getAllData('promo_code');
        
        $pageData = [
            'pageName' => "PromoCode",
            'action'  => 'add',
            'allData' => $allData,            
        ];

        $this->load->view('dashboard/promocode', $pageData);
    }

    public function delete($id)
    {       

        $this->common->delete('promo_code', $id);
        $this->session->set_flashdata('success', 'Your Promo Code has been deleted successfully');        

        $allData = $this->common->getAllData('faqs');
        
        $pageData = [
            'pageName' => "Promocode",
            'allData' => $allData,
            'action'  => 'add',   
        ];

        $this->load->view('dashboard/promocode', $pageData);
    }

    public function loadUpdate($id)
    {

        $updateData = $this->common->getById('promo_code', $id);       

        $allData = $this->common->getAllData('promo_code');
        
        $pageData = [
            'pageName' => "PromoCode",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',            
        ];

        $this->load->view('dashboard/promocode', $pageData);
    }

    public function update($id)
    {
        $categoryInfo = $this->common->getById('promo_code', $id);

        $data = array(
            'offertype' => $_POST['offertype'],
            'percentage' => $_POST['percentage'],
            'start_date'=> $_POST['start_date'],
            'end_date'=> $_POST['end_date'],            
            'promo_code'=> $_POST['promo_code']
        );

        $this->common->update('promo_code', $id, $data);        

        $allData = $this->common->getAllData('promo_code');
        
        $pageData = [
            'pageName' => "PromoCode",
            'allData' => $allData,
            'action'  => 'update',            
        ];

        $this->load->view('dashboard/promocode', $pageData);
    }
    
}