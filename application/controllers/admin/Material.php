<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Material extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('material');

        $data = [
            'pageName' => "Material",
            'action'  => 'add',
            'allData' => $allData
        ];

        $this->load->view('dashboard/material', $data);
    }

    public function add()
    {

        $data = array(
            'material' => $_POST['material']
        );

        $this->db->insert('material', $data);

        $allData = $this->common->getAllData('material');

        $pageData = [
            'pageName' => "Material",
            'action'  => 'add',
            'allData' => $allData,
        ];

        $this->load->view('dashboard/material', $pageData);
    }

    public function delete($id)
    {

        $this->common->delete('material', $id);

        $allData = $this->common->getAllData('material');

        $pageData = [
            'pageName' => "Material",
            'allData' => $allData,
            'action'  => 'add'
        ];

        $this->load->view('dashboard/material', $pageData);
    }

    public function loadUpdate($id)
    {

        $updateData = $this->common->getById('material', $id);

        $allData = $this->common->getAllData('material');

        $pageData = [
            'pageName' => "Material",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
        ];

        $this->load->view('dashboard/material', $pageData);
    }

    public function update($id)
    {

        $data = array(
            'material' => $_POST['material']
        );

        $this->common->update('material', $id, $data);
        $allData = $this->common->getAllData('material');

        $pageData = [
            'pageName' => "Material",
            'allData' => $allData,
            'action'  => 'add'
        ];

        $this->load->view('dashboard/material', $pageData);
    }
}