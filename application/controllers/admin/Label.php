<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Label extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('label');

        $data = [
            'pageName' => "Label",
            'action'  => 'add',
            'allData' => $allData
        ];

        $this->load->view('dashboard/label', $data);
    }

    public function add()
    {

        $data = array(
            'label' => $_POST['label']
        );

        $this->db->insert('label', $data);

        $allData = $this->common->getAllData('label');

        $pageData = [
            'pageName' => "Label",
            'action'  => 'add',
            'allData' => $allData,
        ];

        $this->load->view('dashboard/label', $pageData);
    }

    public function delete($id)
    {

        $this->common->delete('label', $id);

        $allData = $this->common->getAllData('label');

        $pageData = [
            'pageName' => "Label",
            'allData' => $allData,
            'action'  => 'add'
        ];

        $this->load->view('dashboard/label', $pageData);
    }

    public function loadUpdate($id)
    {

        $updateData = $this->common->getById('label', $id);

        $allData = $this->common->getAllData('label');

        $pageData = [
            'pageName' => "Label",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
        ];

        $this->load->view('dashboard/label', $pageData);
    }

    public function update($id)
    {

        $data = array(
            'label' => $_POST['label']
        );

        $this->common->update('label', $id, $data);
        $allData = $this->common->getAllData('label');

        $pageData = [
            'pageName' => "Label",
            'allData' => $allData,
            'action'  => 'add'
        ];

        $this->load->view('dashboard/label', $pageData);
    }
}