<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class About extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('about');
        
        $data = [
            'pageName' => "AboutPage",
            'action'  => 'add',
            'allData' => $allData,
            
        ];

        $this->load->view('dashboard/about', $data);
    }

    public function add()
    {    
        $image = $this->common->upload('image'); 
            
        $data = array(
            'title' => $_POST['title'],
            'position' => $_POST['position'],
            // 'alignment' => $_POST['alignment'],
            'content' => $_POST['content'],
            'image' => $image,            
        );
        $this->db->insert('about', $data);   
       
        $allData = $this->common->getAllData('about');

        $pageData = [
            'pageName' => "AboutPage",
            'action'  => 'add',
            'allData' => $allData,            
        ];
//var_dump($data);
        $this->load->view('dashboard/about', $pageData);
    }

    public function delete($id)
    {       
        $this->common->delete('about', $id);
        $allData = $this->common->getAllData('about');
        $this->session->set_flashdata('success', 'Your aboutPage has been deleted successfully');        
        
        $pageData = [
            'pageName' => "AboutPage",
            'allData' => $allData,
            'action'  => 'add',
        ];

        $this->load->view('dashboard/about', $pageData);
    }

    public function loadUpdate($id)
    {
        $updateData = $this->common->getById('about', $id);
        $allData = $this->common->getAllData('about');
        
        $pageData = [
            'pageName' => "AboutPage",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
        ];
        //var_dump($pageData);
        $this->load->view('dashboard/about', $pageData);
    }

    public function update($id)
    {
        $image = $this->common->upload('image');
       
        $data = array(
            'title' => $_POST['title'],
            'position' => $_POST['position'],
            //'alignment' => $_POST['alignment'],
            'content' => $_POST['content'],
            'image' => $image,            
        );

        $this->common->update('about', $id, $data);
       
        $allData = $this->common->getAllData('about');
        
        $pageData = [
            'pageName' => "AboutPage",
            'allData' => $allData,
            'action'  => 'update',
            
        ];

        $this->load->view('dashboard/about', $pageData);
    }

    
}