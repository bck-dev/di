<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FeatureImage extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn($this->session->userdata('loggedIn'));
        $this->isAdmin($this->session->userdata('userType'));
    }

    public function index()
    {
        $allData = $this->common->getAllData('feature_image');
        
        $data = [
            'pageName' => "FeatureImage",
            'action'  => 'add',
            'allData' => $allData,
            
        ];

        $this->load->view('dashboard/featureImage', $data);
    }

    public function add()
    {    
       // if (!empty($_FILES['image1']['image2']['image3'])) {   
        $image = $this->common->upload('image'); 
            
        $data = array(
            'title' => $_POST['title'],
            'buttn_text' => $_POST['buttn_text'],
            'buttn_link' => $_POST['buttn_link'],
            'image' => $image,
            
        );
        $this->db->insert('feature_image', $data); //}   
       
        $allData = $this->common->getAllData('feature_image');

        $pageData = [
            'pageName' => "FeatureImage",
            'action'  => 'add',
            'allData' => $allData,            
        ];
//var_dump($data);
        $this->load->view('dashboard/featureImage', $pageData);
    }

    public function delete($id)
    {       
        $this->common->delete('feature_image', $id);
        $allData = $this->common->getAllData('feature_image');
        $this->session->set_flashdata('success', 'Your feature_image has been deleted successfully');        
        
        $pageData = [
            'pageName' => "FeatureImage",
            'allData' => $allData,
            'action'  => 'add',
        ];

        $this->load->view('dashboard/featureImage', $pageData);
    }

    public function loadUpdate($id)
    {
        $updateData = $this->common->getById('feature_image', $id);
        $allData = $this->common->getAllData('feature_image');
        
        $pageData = [
            'pageName' => "FeatureImage",
            'allData' => $allData,
            'updateData'  => $updateData,
            'action'  => 'update',
        ];
        //var_dump($pageData);
        $this->load->view('dashboard/featureImage', $pageData);
    }

    public function update($id)
    {
        //if (!empty($_FILES['image'])) {   
        $image = $this->common->upload('image');
       
        $data = array(
            'title' => $_POST['title'],
            'buttn_text' => $_POST['buttn_text'],
            'buttn_link' => $_POST['buttn_link'],
            'image' => $image,
            
        );

        $this->common->update('feature_image', $id, $data);
    //}
    
        $allData = $this->common->getAllData('feature_image');
        
        $pageData = [
            'pageName' => "FeatureImage",
            'allData' => $allData,
            'action'  => 'update',
            
        ];

        $this->load->view('dashboard/featureImage', $pageData);
    }

    
}