<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Model
{
    function allProducts()
    {
        $this->db->select('product.*, category.categoryName');
        $this->db->from('product');
        $this->db->join('category', "product.categoryId = category.id");
        return $this->db->get()->result();
    }

    public function getValidProductCount()
    {
        $this->db->select('id');
        $this->db->from('product');
        $this->db->where('coverImage!=', "");
        return $this->db->count_all_results();
    }

    public function getTempProductOption($productId, $colorId, $sizeId)
    {
        $this->db->select('*');
        $this->db->from('temp_old_product_option');
        $this->db->where('productId', $productId);
        $this->db->where('colorId', $colorId);
        $this->db->where('sizeId', $sizeId);
        return $this->db->get()->row();
    }

    function newProducts($count = null)
    {
        $this->db->select('DISTINCT(product.id)');
        $this->db->from('product');
        $this->db->where('coverImage!=', "");
        $this->db->join('product_option', 'product_option.productId = product.id');
        $this->db->where('product_option.price>', 0);
        
        $this->db->order_by('id', "DESC");
        if ($count) {
            $this->db->limit($count);
        }
        $products = $this->db->get()->result();
        $newProducts = [];
        $i=0;
        foreach ($products as $product) {
            $newProducts[$i] = $this->productBasicDetail($product->id);
            $newProducts[$i]->lowestPrice = $this->lowestPrice($product->id)->price;
            $newProducts[$i]->stock = $this->totalStock($product->id)->stock;
            $i++;
        }

        return $newProducts;
    }

    function newProductsByType($type)
    {
        $this->db->select('DISTINCT(product.id)');
        $this->db->from('product');
        $this->db->where('product.type', $type);
        $this->db->join('category', "product.categoryId = category.id");
        $this->db->join('product_option', 'product_option.productId = product.id');
        $this->db->order_by('id', "DESC");
        $this->db->limit(10);
        $products = $this->db->get()->result();
        $newProducts = [];
        $i=0;
        foreach ($products as $product) {
            $newProducts[$i] = $this->productBasicDetail($product->id);
            $newProducts[$i]->lowestPrice = $this->lowestPrice($product->id)->price;
            $newProducts[$i]->stock = $this->totalStock($product->id)->stock;
            $i++;
        }

        return $newProducts;
    }

    function cart($tempCartId){
        $this->db->select('*');
        $this->db->from('temp_cart_item');
        $this->db->where('tempCartId', $tempCartId);
        $cartItems = $this->db->get()->result();
        
        foreach($cartItems as $item){
            if($item->productOptionId==0){
                $voucherInfo=$this->common->getById('voucher', $item->productId);

                $item->productName = $voucherInfo->name;
                $item->coverImage = $voucherInfo->image;
            }
            else{
                $productInfo=$this->common->getById('product', $item->productId);
                $productOptionInfo=$this->common->getById('product_option', $item->productOptionId);
                $colorInfo=$this->common->getById('color', $productOptionInfo->colorId);

                $item->productName = $productInfo->productName;
                $item->coverImage = $productInfo->coverImage;
                $item->color= $colorInfo->color;
                $item->code= $colorInfo->code;
                $item->size= $this->common->getById('size', $productOptionInfo->sizeId)->size;
            }
        }

        return $cartItems;
    }

    function cartTotal($tempCartId)
    {
        $this->db->select_sum('total');
        $this->db->from('temp_cart_item');
        $this->db->where('tempCartId', $tempCartId);
        return $this->db->get()->row()->total;
    }

    function generateTempCartId(){
        $this->db->select_max('tempCartId');
        $this->db->from('temp_cart_item');
        
        $maxRow = $this->db->get()->row();
        
        return $maxRow->tempCartId+1;
    }

    function productFullDetail($productId)
    {
        $this->db->select('product.*, category.categoryName');
        $this->db->from('product');
        $this->db->where('product.id', $productId);
        $this->db->join('category', "product.categoryId = category.id");
        $this->db->order_by('id', "DESC");
        $product = $this->db->get()->row();

        $product->lowestPrice = $this->lowestPrice($product->id)->price;
        $product->stock = $this->totalStock($product->id)->stock;
        $product->options = $this->productOptions($product->id);
        $product->colors = $this->productColors($product->id);
        $product->sizes = $this->productSizes($product->id);
        $product->labels = $this->productLabels($product->id);
        $product->details = $this->productDetails($product->id);
        $product->images = $this->productImages($product->id);


        return $product;
    }

    

    function minPrice()
    {
        $this->db->select_min('price');
        $this->db->from('product_option');
        $this->db->where('stock>', 0);
        $min = $this->db->get()->row()->price;
        return $min;
    }

    function maxPrice()
    {
        $this->db->select_max('price');
        $this->db->from('product_option');
        $this->db->where('stock>', 0);
        $max = $this->db->get()->row()->price;
        return $max;
    }

    function lowestPrice($productId)
    {
        $this->db->select_min('price');
        $this->db->from('product_option');
        $this->db->where('productId', $productId);
        return $this->db->get()->row();
    }

    function totalStock($productId)
    {
        $this->db->select_sum('stock');
        $this->db->from('product_option');
        $this->db->where('productId', $productId);
        return $this->db->get()->row();
    }

    function productBasicDetail($productId)
    {
        $this->db->select('product.*, category.categoryName');
        $this->db->from('product');
        $this->db->where('product.id', $productId);
        $this->db->join('category', "product.categoryId = category.id");
        return $this->db->get()->row();
    }

    function productOptions($productId)
    {
        $this->db->select('product_option.*, color.color, color.code, size.size');
        $this->db->from('product_option');
        $this->db->where('productId', $productId);
        $this->db->join('color', "product_option.colorId = color.id");
        $this->db->join('size', "product_option.sizeId = size.id");
        return $this->db->get()->result();
    }

    function productImages($productId)
    {
        $this->db->select('product_has_image.*, color.color, color.code');
        $this->db->from('product_has_image');
        $this->db->where('productId', $productId);
        $this->db->join('color', "product_has_image.colorId = color.id");
        return $this->db->get()->result();
    }

    function productDetails($productId)
    {
        $this->db->select('*');
        $this->db->from('product_has_detail');
        $this->db->where('productId', $productId);
        return $this->db->get()->result();
    }

    function productColors($productId)
    {
        $this->db->select('product_has_color.colorId, color.color, color.code');
        $this->db->from('product_has_color');
        $this->db->where('productId', $productId);
        $this->db->join('color', "product_has_color.colorId = color.id");
        return $this->db->get()->result();
    }

    function productLabels($productId)
    {
        $this->db->select('product_has_label.labelId, label.label');
        $this->db->from('product_has_label');
        $this->db->where('productId', $productId);
        $this->db->join('label', "product_has_label.labelId = label.id");
        return $this->db->get()->result();
    }

    function ProductSizes($productId)
    {
        $this->db->select('product_has_size.sizeId, size.size');
        $this->db->from('product_has_size');
        $this->db->where('productId', $productId);
        $this->db->join('size', "product_has_size.sizeId = size.id");
        return $this->db->get()->result();
    }

    function productOrderCategory()
    {
        $this->db->select('product.*, category.categoryName');
        $this->db->from('product');
        $this->db->where('coverImage!=', "");
        $this->db->join('category', "product.categoryId = category.id");
        $this->db->order_by('productName', "ASC");
        $this->db->order_by('categoryId', "DESC");
        $products = $this->db->get()->result();

        foreach ($products as $product) {
            $product->lowestPrice = $this->lowestPrice($product->id)->price;
            $product->stock = $this->totalStock($product->id)->stock;
        }

        return $products;
    }

    function discountedProducts()
    {
        $this->db->select('product.*, category.categoryName');
        $this->db->from('product');
        $this->db->where('discount>', 0);
        $this->db->join('category', "product.categoryId = category.id");
        $this->db->order_by('productName', "ASC");
        $this->db->order_by('categoryId', "DESC");
        $products = $this->db->get()->result();

        foreach ($products as $product) {
            $product->lowestPrice = $this->lowestPrice($product->id)->price;
        }

        return $products;
    }


    public function filterResultCount($type=null, $category = null, $color = null, $size = null, $min = null, $max = null, $keyword=null)
    {
        $this->db->select('DISTINCT(product.id)');
        $this->db->from('product');
        $this->db->where('coverImage!=', "");
        if ($keyword) {
            $this->db->like('productName', $keyword);
            $this->db->or_like('description', $keyword);
        }
        if ($category) {
            $this->db->where('categoryId', $category);
        }

        if ($type) {
            $this->db->where('type', $type);
        }

        if ($color) {
            $this->db->join('product_has_color', 'product_has_color.productId = product.id');
            $this->db->where('product_has_color.colorId', $color);
        }

        if ($size) {
            $this->db->join('product_has_size', 'product_has_size.productId = product.id');
            $this->db->where('product_has_size.sizeId', $size);
        }

        if ($min) {
            $this->db->join('product_option', 'product_option.productId = product.id');
            $this->db->where('product_option.price>=', $min);
            $this->db->where('product_option.price<=', $max);
        }

        $this->db->join('category', "product.categoryId = category.id");

        $newProducts = $this->db->get()->result();
        $i = 0;
        foreach ($newProducts as $product) {
            if($this->lowestPrice($product->id)->price){
                $i++;
            }
        }

        return $i;
    }

    public function filter($type=null, $category = null, $color = null, $size = null, $min = null, $max = null, $pageNo = null, $keyword= null)
    {
        $limit = 12;
        if ($pageNo) {
            $from = $limit * ($pageNo - 1);
        }

        $this->db->select('DISTINCT(product.id)');
        $this->db->from('product');
        $this->db->where('coverImage!=', "");
        if ($keyword) {
            $this->db->like('productName', $keyword);
            $this->db->or_like('description', $keyword);
        }
        if ($category) {
            $this->db->where('categoryId', $category);
        }
        if ($type) {
            $this->db->where('type', $type);
        }

        if ($color) {
            $this->db->join('product_has_color', 'product_has_color.productId = product.id');
            $this->db->where('product_has_color.colorId', $color);
        }

        if ($size) {
            $this->db->join('product_has_size', 'product_has_size.productId = product.id');
            $this->db->where('product_has_size.sizeId', $size);
        }

        
        if ($min) {
            $this->db->join('product_option', 'product_option.productId = product.id');
            $this->db->where('product_option.price>=', $min);
            $this->db->where('product_option.price<=', $max);
        }else{
            $this->db->join('product_option', 'product_option.productId = product.id');
            $this->db->where('product_option.price>', 0);
        }

        $this->db->join('category', "product.categoryId = category.id");
        $this->db->order_by('id', "DESC");

        if ($from) {
            $this->db->limit($limit, $from);
        } else {
            $this->db->limit($limit);
        }

        $newProducts = $this->db->get()->result();
        $output = array();
        $i = 0;
        foreach ($newProducts as $product) {
            $output[$i] = $this->productBasicDetail($product->id);
            $output[$i]->lowestPrice = $this->lowestPrice($product->id)->price;
            $output[$i]->stock = $this->totalStock($product->id)->stock;
            $i++;
        }

        return $output;
    }
    public function getColors($productId, $size)
    {
        $this->db->select('product_option.*, color.code');
        $this->db->from('product_option');
        $this ->db->where('productId', $productId);
        $this->db->where('sizeId', $size);
        $this->db->where('stock>', 0);
        $this->db->join('color', 'product_option.colorId = color.id');
           
        $output = $this->db->get()->result();
        return $output;
        // echo $this->db->last_query();
    }

    public function getPrice($productId, $size, $color)
    {
        $this->db->select('*');
        $this->db->from('product_option');
        $this ->db->where('productId', $productId);
        $this->db->where('sizeId', $size);
        $this->db->where('stock>', 0);
            $this->db->where('colorId', $color);
           
        $output = $this->db->get()->result();
        return $output;
        // echo $this->db->last_query();
    }

    public function getArea($city)
    {
        $this->db->select('delivery_fees.*');
        $this->db->from('delivery_fees');
        $this ->db->where('area', $city);
                  
        $output = $this->db->get()->row();
        //$areas = $output->fees;
        return $output;
        // echo $this->db->last_query();
    }
    
}