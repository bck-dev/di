<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Common extends CI_Model
{
    public function getAllData($table)
    {
        $this->db->from($table);
        $this->db->order_by('id', 'DESC');
        return $this->db->get()->result();
    }

    public function getDataWithLimit($table, $limit)
    {
        $this->db->from($table);
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit);
        return $this->db->get()->result();
    }

    public function getById($table, $id)
    {
        $this->db->from($table);
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function getByFeildSingle($table, $feild, $value)
    {
        $this->db->from($table);
        $this->db->where($feild, $value);
        return $this->db->get()->row();
    }

    public function getByFeild($table, $feild, $value)
    {
        $this->db->from($table);
        $this->db->where($feild, $value);
        return $this->db->get()->result();
    }

    public function delete($table, $id)
    {
        $this->db->where('id', $id);
        $this->db->delete($table);
    }

    public function deleteByFeild($table, $feild, $value)
    {
        $this->db->where($feild, $value);
        $this->db->delete($table);
    }

    public function update($table, $id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($table, $data);
    }

    public function updateMultipleCondition($table, $conditions, $data)
    {
        $conditionCount = count($conditions);
        $keys = array_keys($conditions);
        $values = array_values($conditions);

        $i = 0;
        while ($i < $conditionCount) {
            $this->db->where($keys[$i], $values[$i]);
            $i++;
        }

        $this->db->update($table, $data);
    }

    public function getCount($table, $feild, $value)
    {
        $this->db->select('id');
        $this->db->from($table);
        $this->db->where($feild, $value);
        return $this->db->count_all_results();
    }

    function upload($inputname)
    {
        $date = date('d-m-Y');
        $path = './uploads/' . $date;
        $db_path = 'uploads/' . $date . '/';

        if (!is_dir('uploads/' . $date)) {
            mkdir('./uploads/' . $date, 0777, TRUE);
        }

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['encrypt_name'] = TRUE;

        $this->upload->initialize($config);
        $this->upload->do_upload($inputname);

        $data_upload_files = $this->upload->data();

        if ($data_upload_files['file_name']) {
            return $db_path . $data_upload_files['file_name'];
        } else {
            return false;
        }
    }

    public function temporaryDumbing($table, $dumbingTable, $feild, $value)
    {
        $this->db->from($table);
        $this->db->where($feild, $value);
        $results = $this->db->get()->result();

        foreach ($results as $result) {
            $this->db->insert($dumbingTable, $result);
        }
    }

    // public function updateCart($curr_quantity,$id){

    //     $quantity = trim($curr_quantity['txt_quantity']);
        
    //         $value=array('quantity'=>$quantity);
    //         $this->db->where('id',$id);
    //         $this->db->update('temp_cart_item',$value);
    // }   


    public function template($page, $data = Null)
    {
        $allData = $this->product->cart($this->session->userdata('tempCartId'));
        $countCart = $this->common->getCount('temp_cart_item','tempCartId', $this->session->userdata('tempCartId'));
        $cartTotal = $this->product->cartTotal($this->session->userdata('tempCartId'));
            
        $data['categories'] = $this->categoryModel->allCategories();
        if (isset($data['products'])) {
            
            $count = $data['totalProducts'];
            $pagination = $this->common->pagination($count, 1);
        }

        if (isset($pagination)) {
            $data['pagination'] = $pagination;
        }

        $data['allData'] = $allData;
        $data['countCart'] = $countCart;
        $data['cartTotal'] = $cartTotal;

        $this->load->view($page, $data);
    }

    public function pagination($count, $current)
    {
        $limit = 12;

        if ($count != 0) {
            $pageCount = number_format(ceil($count / $limit));
            $paginationButtons = [];

            $max = $pageCount > $current + 2 ? $current + 2 : $pageCount;
            $x = $current - 2 > 0 ? $current - 2 : $x = 1;
            $start = (($current - 1)*$limit) +1;
            if(($current * $limit) < $count){
                $end =  $current * $limit;
            }else{
                $end = $count;
                }

            if ($current == 1) {
                $i = 0;
            } else {
                $paginationButtons[0]['value'] = "<<";
                $paginationButtons[0]['id'] = $current - 1;
                $i = 1;
            }

            if ($current - 2 >= 2) {
                $paginationButtons[$i]['value'] = "1";
                $paginationButtons[$i]['id'] = 1;
                $paginationButtons[$i]['status'] = "first";
                $i++;
            }

            while ($x <= $max) {
                if ($x >= $current - 2) {
                    $paginationButtons[$i]['value'] = $x;
                    $paginationButtons[$i]['id'] = $x;
                    if ($x == $current) {
                        $paginationButtons[$i]['status'] = "current";
                    }
                    $i++;
                }
                $x++;
            }

            if ($current + 2 < $pageCount) {
                $paginationButtons[$i]['value'] = "" . $pageCount;
                $paginationButtons[$i]['id'] = $pageCount;
                $paginationButtons[$i]['status'] = "last";
                $i++;
            }

            if ($pageCount != $current) {
                $paginationButtons[$i + 1]['value'] = ">>";
                $paginationButtons[$i + 1]['id'] = $current + 1;
            }

            $paginationData = array(
                'count' => $count,
                'start' => $start,
                'end' => $end,               
                'buttons' => $paginationButtons
            );

            return $paginationData;
        }
    }
}