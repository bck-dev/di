<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CategoryModel extends CI_Model
{
    function categoryDropdown($categoryName = "", $level = 0)
    {
        $this->db->from('category');
        $this->db->where('parent', $categoryName);

        $results = $this->db->get()->result();

        $x = 0;
        $levelMark = "";
        while ($x < $level) {
            $levelMark = $levelMark . "--";
            $x++;
        }

        $i = 0;
        foreach ($results as $r) {
            $output[$i] = array(
                'id' => $r->id,
                'realValue' => $r->categoryName,
                'displayValue' => $levelMark . $r->categoryName
            );

            $i++;

            if ($this->common->getCount('category', 'parent', $r->id) > 0) {
                $subCats = $this->categoryDropdown($r->id, $r->level + 1);

                foreach ($subCats as $cat) {
                    $output[$i] = $cat;
                    $i++;
                }
            }
        }

        return $output;
    }
    public function allCategories()
    {
        $this->db->select("*");
        $this->db->from('category');

        $categories = $this->db->get()->result();
        foreach ($categories as $cat) {

            $parentInfo = $this->common->getById('category', $cat->parent);
            $cat->parentName = $parentInfo->categoryName;
        }

        return $categories;
    }
    public function check_categoryName($categoryName)
    {

        $q = $this->db->get_where('category', array('categoryName' => $categoryName));
        if (empty($q->row_array())) {
            return true;
        } else {
            return false;
        }
    }

    function categoryLevelUpdate($categoryId, $level)
    {
        $this->db->from('category');
        $this->db->where('parent', $categoryId);
        $results = $this->db->get()->result();

        foreach ($results as $r) {
            $data = array(
                'level' => $level + 1,
            );

            $this->common->update('category', $r->id, $data);

            if ($this->common->getCount('category', 'parent', $r->id) > 0) {
                $this->categoryLevelUpdate($r->id, $level + 1);
            }
        }
    }

   /* function upDat($parent =""){

        //$updateData = $this->common->getById('category', $id);

        $this->db->select("*");
        $this->db->from('category');       
        $this->db->where('categoryName', $parent);
        $res = $this->db->get()->result();
        
        
        //$a->parentName = $b->categoryName;    

        return $res;
    }*/
}