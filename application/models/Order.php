<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Model
{
    function getOrderNo(){
        $this->db->select_max('order_no');
        $this->db->from('order_has_item');
        
        $maxRow = $this->db->get()->row();
        
        return $maxRow->order_no+1;
    }

    function getVoucherNo(){
        $this->db->select_max('voucherNo');
        $this->db->from('active_voucher');
        
        $maxRow = $this->db->get()->row();
        
        return $maxRow->voucherNo+1;
    }

    function orderItems($order_no){
        $this->db->select('*');
        $this->db->from('order_has_item');
        $this->db->where('order_no', $order_no);
        $cartItems = $this->db->get()->result();
        
        foreach($cartItems as $item){
            if($item->productOptionId==0){
                $voucherInfo=$this->common->getById('voucher', $item->productId);

                $item->productName = $voucherInfo->name;
                $item->coverImage = $voucherInfo->image;
            }
            else{
                $productInfo=$this->common->getById('product', $item->productId);
                $productOptionInfo=$this->common->getById('product_option', $item->productOptionId);
                $colorInfo=$this->common->getById('color', $productOptionInfo->colorId);

                $item->productName = $productInfo->productName;
                $item->coverImage = $productInfo->coverImage;
                $item->color= $colorInfo->color;
                $item->code= $colorInfo->code;
                $item->size= $this->common->getById('size', $productOptionInfo->sizeId)->size;
            }
        }

        return $cartItems;
    }

    function sendVoucherMail($voucherNo, $giftEmail, $email, $firstName){
        
    }

    function sendOrderMail($order_no){
        
    }

    function validateDeduction($reference){
        if(is_numeric($reference)){
            $this->db->select('*');
            $this->db->from('active_voucher');
            $this->db->where('voucherNo', $reference);
            $this->db->where('validTill >=', date('Y-m-d'));

            return $this->db->get()->row();

        }
        else{
            return false;
        }
        
    }

    function orderProduct(){
    $this->db->select('orders.*,product.*,order_has_item.*');
    $this->db->from('orders');
   // $this->db->where('orders.order_no',$order_no);
    $this->db->join('order_has_item', "orders.order_no = order_has_item.order_no");
    $this->db->join('product', "product.id = order_has_item.productId");
    $this->db->order_by('orders.id', "DESC");
    //$this->db->limit(10);        
    $orderProduct = $this->db->get()->result();  

        return $orderProduct;
    }

    function orderName($order_no)
    {
        $this->db->select('order_has_item.*, product.*,orders.*');
        $this->db->from('order_has_item');
        $this->db->where('order_has_item.order_no', $order_no);
        $this->db->join('product', "product.id = order_has_item.productId");
        $this->db->join('orders', 'order_has_item.order_no = orders.order_no');
        $this->db->order_by('order_has_item.id', "DESC");
        return $this->db->get()->result();
    }

    public function getOrderCount()
    {
        $this->db->select('id');
        $this->db->from('orders');
        $this->db->where('order_no!=', "");
        return $this->db->count_all_results();
    }
    public function totlaOrderCount()
    {
        $this->db->select('id');
        $this->db->from('order_has_item');
        $this->db->where('order_no!=', "");
        return $this->db->count_all_results();
    }
    function totalSales()
    {
        $this->db->select_sum('grand_total');
        $this->db->from('orders');
        $this->db->where('order_no!=', "");
        return $this->db->get()->row();
    }

    function get_monthly_totals($theYear = '', $theMonth = '')
    {
        $date = date('d-m-Y');
        $sql = "SELECT DATEPART(Year, Date) [TheYear], DATEPART(Month, Date) [TheMonth], DATENAME(Month, Date) [TheMonthName], SUM(Amount) [TotalAmount]
            FROM orders
            WHERE Date IS NOT NULL ";

        if ($theYear != '') {
            $sql = $sql . " AND DATEPART(Year, Date) = '$theYear'";
        }

        if ($theMonth != '') {
            $sql = $sql . " AND DATEPART(Month, Date) = '$theMonth'";
        }

        $sql = $sql . " GROUP BY DATEPART(Year, Date), DATEPART(Month, Date), DATENAME(Month, Date) ORDER BY [TheYear], [TheMonth], [TheMonthName]";

        $query = $this->db->query($sql);

        return $query->result();
    }

    
}