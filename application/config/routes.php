<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['home'] = 'home';
$route['login'] = 'login';
$route['checkLogin'] = 'login/checkLogin';
$route['register'] = 'register';
$route['registerUser'] = 'register/save';
$route['regMsg'] = 'register/regMsg';
$route['logout'] = 'login/logout';
$route['forgot'] = 'forgot';
$route['sendForgotMail'] = 'forgot/sendForgotMail';
$route['checkMail'] = 'forgot/checkMail';
$route['resetPassword/(:num)/(:any)'] = 'forgot/resetPassword/$1/$2';
$route['updateNewPassword'] = 'forgot/updateNewPassword';
$route['resetSuccess'] = 'forgot/resetSuccess';
$route['faq'] = 'faq';
$route['voucher'] = 'voucher';

//cart
$route['cart'] = 'cart';
$route['updateCart'] = 'cart/updateCart';
$route['redeem'] = 'cart/redeem';
$route['cart/view/(:num)'] = 'cart/view/$1';
$route['cart/delete/(:num)'] = 'cart/delete/$1';
$route['cart/loadUpdate/(:num)'] = 'cart/loadUpdate/$1';

//shop 
$route['shop'] = 'shop';
$route['search'] = 'shop/search';
$route['shop/type/(:any)'] = 'shop/type/$1';
$route['shop/type/(:any)/category/(:num)'] = 'shop/category/$1/$2';
$route['shop/shop/(:num)'] = 'shop/shop/$1';
$route['shop/view/(:num)'] = 'shop/view/$1';
$route['filterProducts'] = 'shop/filterProducts';
$route['getColors'] = 'shop/getColors';
$route['getPrice'] = 'shop/getPrice';
$route['shop/pagination'] = 'shop/pagination';
$route['shop/paginatedData'] = 'shop/filterProducts';

// checkout order
$route['checkout'] = 'checkout';
$route['checkout/add'] = 'checkout/add';
$route['getArea'] = 'checkout/getArea';
$route['thankyou/(:num)'] = 'checkout/thankyou/$1';

//about
$route['about'] = 'about';

// admin route
$route['admin'] = 'admin/admin';

// category
$route['admin/category'] = 'admin/category';
$route['admin/category/add'] = 'admin/category/add';
$route['admin/category/delete/(:num)'] = 'admin/category/delete/$1';
$route['admin/category/loadUpdate/(:num)'] = 'admin/category/loadUpdate/$1';

// order
$route['admin/order'] = 'admin/orderReport';
$route['admin/order/view/(:num)'] = 'admin/orderReport/view/$1';
$route['admin/order/changeStatus/(:num)/(:num)'] = 'admin/orderReport/changeStatus/$1/$2';

//delivery fees
$route['admin/fees'] = 'admin/fees';
$route['admin/fees/add'] = 'admin/fees/add';
$route['admin/fees/delete/(:num)'] = 'admin/fees/delete/$1';
$route['admin/fees/loadUpdate/(:num)'] = 'admin/fees/loadUpdate/$1';

//sliders
$route['admin/slider'] = 'admin/slider';
$route['admin/slider/add'] = 'admin/slider/add';
$route['admin/slider/delete/(:num)'] = 'admin/slider/delete/$1';
$route['admin/slider/loadUpdate/(:num)'] = 'admin/slider/loadUpdate/$1';

//About
$route['admin/about'] = 'admin/about';
$route['admin/about/add'] = 'admin/about/add';
$route['admin/about/delete/(:num)'] = 'admin/about/delete/$1';
$route['admin/about/loadUpdate/(:num)'] = 'admin/about/loadUpdate/$1';

//Feature Image
$route['admin/featureImage'] = 'admin/featureImage';
$route['admin/featureImage/add'] = 'admin/featureImage/add';
$route['admin/featureImage/delete/(:num)'] = 'admin/featureImage/delete/$1';
$route['admin/featureImage/loadUpdate/(:num)'] = 'admin/featureImage/loadUpdate/$1';

// size
$route['admin/size'] = 'admin/size';
$route['admin/size/add'] = 'admin/size/add';
$route['admin/size/delete/(:num)'] = 'admin/size/delete/$1';
$route['admin/size/loadUpdate/(:num)'] = 'admin/size/loadUpdate/$1';

// detailTitle
$route['admin/detailTitle'] = 'admin/detailTitle';
$route['admin/detailTitle/add'] = 'admin/detailTitle/add';
$route['admin/detailTitle/delete/(:num)'] = 'admin/detailTitle/delete/$1';
$route['admin/detailTitle/loadUpdate/(:num)'] = 'admin/detailTitle/loadUpdate/$1';

// color
$route['admin/color'] = 'admin/color';
$route['admin/color/add'] = 'admin/color/add';
$route['admin/color/delete/(:num)'] = 'admin/color/delete/$1';
$route['admin/color/loadUpdate/(:num)'] = 'admin/color/loadUpdate/$1';

// label
$route['admin/label'] = 'admin/label';
$route['admin/label/add'] = 'admin/label/add';
$route['admin/label/delete/(:num)'] = 'admin/label/delete/$1';
$route['admin/label/loadUpdate/(:num)'] = 'admin/label/loadUpdate/$1';

// material
$route['admin/material'] = 'admin/material';
$route['admin/material/add'] = 'admin/material/add';
$route['admin/material/delete/(:num)'] = 'admin/material/delete/$1';
$route['admin/material/loadUpdate/(:num)'] = 'admin/material/loadUpdate/$1';

// FAQ
$route['admin/faq'] = 'admin/faq';
$route['admin/faq/add'] = 'admin/faq/add';
$route['admin/faq/delete/(:num)'] = 'admin/faq/delete/$1';
$route['admin/faq/loadUpdate/(:num)'] = 'admin/faq/loadUpdate/$1';

// PromoCode
$route['admin/promocode'] = 'admin/promocode';
$route['admin/promocode/add'] = 'admin/promocode/add';
$route['admin/promocode/delete/(:num)'] = 'admin/promocode/delete/$1';
$route['admin/promocode/loadUpdate/(:num)'] = 'admin/promocode/loadUpdate/$1';

// Voucher
$route['admin/voucher'] = 'admin/voucher';
$route['admin/voucher/add'] = 'admin/voucher/add';
$route['admin/voucher/delete/(:num)'] = 'admin/voucher/delete/$1';
$route['admin/voucher/loadUpdate/(:num)'] = 'admin/voucher/loadUpdate/$1';

// inventory
$route['admin/inventory'] = 'admin/inventory';
$route['admin/inventory/new/(:num)'] = 'admin/inventory/new/$1';
$route['admin/inventory/add'] = 'admin/inventory/add';
$route['admin/inventory/loadUpdate/(:num)'] = 'admin/inventory/loadUpdateBasic/$1';
$route['admin/inventory/update/(:num)'] = 'admin/inventory/updateBasic/$1';
$route['admin/inventory/stock/(:num)'] = 'admin/inventory/stock/$1';
$route['admin/inventory/stock/update/(:num)'] = 'admin/inventory/updateStock/$1';
$route['admin/inventory/view/(:num)'] = 'admin/inventory/view/$1';
$route['admin/inventory/image/(:num)'] = 'admin/inventory/image/$1';
$route['admin/inventory/image/update/(:num)'] = 'admin/inventory/updateImage/$1';
$route['admin/inventory/image/delete'] = 'admin/inventory/deleteImage';
$route['admin/inventory/detail/(:num)'] = 'admin/inventory/detail/$1';
$route['admin/inventory/detail/add/(:num)'] = 'admin/inventory/addDetail/$1';
$route['admin/inventory/detail/loadUpdate/(:num)/(:num)'] = 'admin/inventory/loadUpdateDetail/$1/$2';
$route['admin/inventory/detail/update/(:num)/(:num)'] = 'admin/inventory/updateDetail/$1/$2';
$route['admin/inventory/detail/delete/(:num)/(:num)'] = 'admin/inventory/deleteDetail/$1/$2';
$route['admin/inventory/delete/(:num)'] = 'admin/inventory/delete/$1';
//$route['admin/inventory/loadUpdate/(:num)'] = 'admin/inventory/loadUpdate/$1';
$route['admin/inventory/discount'] = 'admin/inventory/discount';
$route['admin/inventory/discount/add'] = 'admin/inventory/addDiscount';
$route['admin/inventory/discount/remove/(:num)'] = 'admin/inventory/removeDiscount/$1';